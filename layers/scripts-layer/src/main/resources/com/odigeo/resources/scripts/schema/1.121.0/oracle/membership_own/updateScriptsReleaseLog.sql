UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.121.0' where ID ='OPRIME-1318/01_insert_into_membership_config.sql';
UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.121.0' where ID ='rollback/R_01_insert_into_membership_config.sql';
UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.121.0' where ID ='UNL-6400/00_create_table_ge_membership_auto_renewal_tracking.sql';
UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.121.0' where ID ='UNL-6400/01_create_index_ge_auto_renewal_tracking.sql';
UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.121.0' where ID ='rollback/R00_create_index_ge_auto_renewal_tracking.sql';
UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.121.0' where ID ='rollback/R01_create_table_ge_membership_auto_renewal_tracking.sql';
INSERT INTO GE_RELEASE_LOG (ID,EXECUTION_DATE,MODULE) values('1.121.0',systimestamp,'membership');
