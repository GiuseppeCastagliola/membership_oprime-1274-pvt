package com.odigeo.product.membership.service;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.google.inject.Inject;
import com.odigeo.commons.monitoring.metrics.MetricsBuilder;
import com.odigeo.commons.monitoring.metrics.MetricsNames;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.membership.Membership;
import com.odigeo.membership.member.MemberService;
import com.odigeo.membership.member.MembershipProductService;
import com.odigeo.membership.member.MembershipValidationService;
import com.odigeo.membership.member.UpdateMembershipService;
import com.odigeo.product.response.exception.MembershipProductServiceException;
import com.odigeo.product.response.exception.MembershipProductServiceExceptionType;
import com.odigeo.product.v2.exception.ProductException;
import com.odigeo.product.v2.model.Product;
import com.odigeo.product.v2.model.enums.ProductType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProductServiceV2New extends ProductServiceV2 {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductServiceV2New.class);
    private static final String GET_PRODUCT = "getProduct";
    private static final String SAVE_PRODUCT = "saveProduct";

    private final UpdateMembershipService updateMembershipService;

    @Inject
    public ProductServiceV2New(MemberService memberService,
                               MembershipProductService membershipProductService,
                               MembershipValidationService membershipValidationService,
                               UpdateMembershipService updateMembershipService) {
        super(membershipValidationService, memberService, membershipProductService);
        this.updateMembershipService = updateMembershipService;
    }

    @Override
    public Product getProduct(String productId) throws ProductException {
        logAndValidateProductId(GET_PRODUCT, productId, productId);

        try {
            return getProductFromDatabase(productId);
        } catch (MissingElementException | DataAccessException e) {
            throw new MembershipProductServiceException(MembershipProductServiceExceptionType.NOT_FOUND, productId, ProductType.MEMBERSHIP_RENEWAL, e.getMessage(), e);
        }
    }

    @Override
    protected boolean commitMembership(Membership membership) throws DataAccessException {
        return updateMembershipService.activatePendingToCollect(membership);
    }

    @Override
    public void saveProduct(String productId, String productProviderId, boolean isPartial) throws ProductException {
        logAndValidateProductId(SAVE_PRODUCT, productId, productProviderId);
        if (!isPartial) {
            saveCompleteOrder(productId, productProviderId);
        }
    }

    private void saveCompleteOrder(String productId, String productProviderId) throws ProductException {
        defaultLoggerMessage("saveCompleteOrder", productId, productProviderId);
        try {
            final Membership membership = getMemberService().getMembershipById(Long.parseLong(productId));
            validateProductInContract(membership);
            MetricsUtils.incrementCounter(MetricsBuilder.buildMetric(MetricsNames.RENEWAL_ACTIVATIONS_NUMBER), MetricsNames.METRICS_REGISTRY_NAME);
        } catch (MissingElementException e) {
            LOGGER.error(MESSAGE_LOG_DEFAULT_EXCEPTION, e.getMessage(), e);
            throw new MembershipProductServiceException(MembershipProductServiceExceptionType.NOT_FOUND, productId, ProductType.MEMBERSHIP_RENEWAL, e.getMessage(), e);
        } catch (DataAccessException e) {
            LOGGER.error(MESSAGE_LOG_DEFAULT_EXCEPTION, e.getMessage(), e);
            throw new MembershipProductServiceException(MembershipProductServiceExceptionType.INTERNAL_SERVER_ERROR, productId, ProductType.MEMBERSHIP_RENEWAL, e.getMessage(), e);
        }
    }
}
