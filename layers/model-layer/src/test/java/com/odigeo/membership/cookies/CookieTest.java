package com.odigeo.membership.cookies;

import bean.test.BeanTest;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.time.Month;

public class CookieTest extends BeanTest<Cookie> {

    private static final String NAME = "cookieTest";
    private static final String VALUE = "18";
    private static final String NAME_AND_VALUE = NAME + VALUE;
    private static final String DOMAIN = "http//:wwww.membership.edreamsodigeo";
    private static final boolean IS_SECURE = false;

    @Override
    protected Cookie getBean() {
        return assembleCookie();
    }

    private Cookie assembleCookie() {
        Cookie cookie = new Cookie();
        cookie.setExpirationDate(LocalDateTime.of(2018, Month.FEBRUARY, 8, 0, 0, 0, 0));
        cookie.setName(NAME);
        cookie.setValue(VALUE);
        cookie.setNameAndValue(NAME_AND_VALUE);
        cookie.setDomain(DOMAIN);
        cookie.setSecure(IS_SECURE);
        return cookie;
    }

    @Test
    public void parametrizedCookiesEqualsVerifierTest() {
        EqualsVerifier.forClass(Cookie.class)
                .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }
}