package com.odigeo.membership.activation.validation.util;

import com.odigeo.test.utils.EnumTest;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static org.testng.Assert.assertTrue;

public class ShoppingCartCollectionStateTest {

    private static final List<String> EXPECTED_VALUES = Arrays.asList("COLLECTED",
            "NOT_COLLECTED", "PARTIALLY_COLLECTED", "UNKNOWN");

    @Test
    public void testShoppingCartCollectionStateEnumIsConsistent() {
        assertTrue(EXPECTED_VALUES.equals(Arrays.asList(EnumTest
                .getNames(ShoppingCartCollectionState.class))));
    }
}
