package com.odigeo.membership.auth;

import bean.test.BeanTest;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.testng.annotations.Test;

import java.util.HashMap;

public class ParametrizedPermissionTest extends BeanTest<ParametrizedPermission> {
    @Test
    public void parametrizedPermissionEqualsVerifierTest() {
        EqualsVerifier.forClass(ParametrizedPermission.class)
                .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }

    @Override
    protected ParametrizedPermission getBean() {
        return assembleParametrizedPermission();
    }

    private ParametrizedPermission assembleParametrizedPermission() {
        ParametrizedPermission parametrizedPermission = new ParametrizedPermission();
        parametrizedPermission.setParams(new HashMap<>());
        return parametrizedPermission;
    }
}