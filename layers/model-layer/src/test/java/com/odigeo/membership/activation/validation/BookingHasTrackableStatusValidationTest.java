package com.odigeo.membership.activation.validation;

import com.odigeo.bookingapi.v12.responses.BookingDetail;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class BookingHasTrackableStatusValidationTest {

    BookingDetail bookingDetail;

    @BeforeTest
    public void setup() {
        bookingDetail = new BookingDetail();
    }

    @Test
    public void testNullStatusValidation() {
        BookingHasTrackableStatusValidation validation = new BookingHasTrackableStatusValidation(bookingDetail);

        assertFalse(validation.validate());
    }

    @Test
    public void testContractStatusValidation() {
        bookingDetail.setBookingStatus("CONTRACT");
        BookingHasTrackableStatusValidation validation = new BookingHasTrackableStatusValidation(bookingDetail);

        assertTrue(validation.validate());
    }

    @Test
    public void testDidNotBuyStatusValidation() {
        bookingDetail.setBookingStatus("INIT");
        BookingHasTrackableStatusValidation validation = new BookingHasTrackableStatusValidation(bookingDetail);

        assertFalse(validation.validate());
    }
}
