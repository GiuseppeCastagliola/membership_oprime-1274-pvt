package com.odigeo.membership.activation.validation;

import com.odigeo.bookingapi.v12.responses.BookingDetail;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;
import static org.testng.Assert.assertFalse;

public class BookingHasContractStatusValidationTest {

    private BookingDetail bookingDetail;
    private BookingHasContractStatusValidation bookingHasContractStatusValidation;

    private static final String FOO = "FOO";
    private static final String CONTRACT = "CONTRACT";

    @BeforeMethod
    public void init() {
        bookingDetail = new BookingDetail();
        bookingHasContractStatusValidation = new BookingHasContractStatusValidation(bookingDetail);
    }

    @Test
    public void testValidationNull() {
        assertFalse(bookingHasContractStatusValidation.validate());
    }

    @Test
    public void testValidationBookingHasNotContract() {
        bookingDetail.setBookingStatus(FOO);
        assertFalse(bookingHasContractStatusValidation.validate());
    }

    @Test
    public void testValidationBookingHasContract() {
        bookingDetail.setBookingStatus(CONTRACT);
        assertTrue(bookingHasContractStatusValidation.validate());
    }
}
