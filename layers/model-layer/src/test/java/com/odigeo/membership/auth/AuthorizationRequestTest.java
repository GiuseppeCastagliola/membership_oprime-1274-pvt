package com.odigeo.membership.auth;

import bean.test.BeanTest;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.testng.annotations.Test;

public class AuthorizationRequestTest extends BeanTest<AuthorizationRequest> {
    @Test
    public void authorizationRequestEqualsVerifierTest() {
        EqualsVerifier.forClass(AuthorizationRequest.class)
                .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }

    @Override
    protected AuthorizationRequest getBean() {
        return assembleAuthorizationRequest();
    }

    private AuthorizationRequest assembleAuthorizationRequest() {
        AuthorizationRequest authorizationRequest = new AuthorizationRequest();
        authorizationRequest.setToken("token");
        return authorizationRequest;
    }
}