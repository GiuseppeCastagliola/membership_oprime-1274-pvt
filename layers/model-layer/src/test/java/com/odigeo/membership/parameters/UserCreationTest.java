package com.odigeo.membership.parameters;

import bean.test.BeanTest;


public class UserCreationTest extends BeanTest<UserCreation> {

    private static final String EMAIL = "oprime@mail.com";
    private static final String LOCALE = "es_ES";
    private static final Integer TRAFFIC_INTERFACE_ID = 1;
    private static final String WEBSITE = "ES";

    @Override
    protected UserCreation getBean() {
        return new UserCreation.Builder()
                .withEmail(EMAIL)
                .withLocale(LOCALE)
                .withTrafficInterfaceId(TRAFFIC_INTERFACE_ID)
                .withWebsite(WEBSITE)
                .build();
    }
}
