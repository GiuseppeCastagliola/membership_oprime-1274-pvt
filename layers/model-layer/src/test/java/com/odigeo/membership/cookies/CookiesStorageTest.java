package com.odigeo.membership.cookies;

import bean.test.BeanTest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class CookiesStorageTest extends BeanTest<CookiesStorage> {

    private static final String NAME = "cookieTest";

    @Override
    protected CookiesStorage getBean() {
        return new CookiesStorage();
    }

    @Test
    public void addCookiesAndClearTest() {
        CookiesStorage cookiesStorage = new CookiesStorage();
        Cookie cookie = new Cookie();
        cookie.setName(NAME);
        cookiesStorage.addCookie(cookie);
        assertEquals(cookiesStorage.getCookies().size(), 1);
        cookiesStorage.clear();
        assertTrue(cookiesStorage.getCookies().isEmpty());
    }
}