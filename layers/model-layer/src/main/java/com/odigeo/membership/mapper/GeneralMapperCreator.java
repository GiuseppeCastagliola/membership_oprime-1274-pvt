package com.odigeo.membership.mapper;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import ma.glasnost.orika.metadata.Type;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class GeneralMapperCreator implements MapperCreator {

    public MapperFactory getMapperFactory() {
        return new DefaultMapperFactory.Builder().build();
    }

    public MapperFacade getMapper() {
        MapperFactory mapperFactory = getMapperFactory();
        mapperFactory.getConverterFactory().registerConverter(new LocalDateConverter());
        return mapperFactory.getMapperFacade();
    }

    static class LocalDateConverter extends BidirectionalConverter<String, LocalDateTime> {

        @Override
        public LocalDateTime convertTo(String s, Type<LocalDateTime> type, MappingContext mappingContext) {
            return LocalDateTime.parse(s);
        }

        @Override
        public String convertFrom(LocalDateTime localDateTime, Type<String> type, MappingContext mappingContext) {
            return localDateTime.format(DateTimeFormatter.ISO_DATE_TIME);
        }
    }
}
