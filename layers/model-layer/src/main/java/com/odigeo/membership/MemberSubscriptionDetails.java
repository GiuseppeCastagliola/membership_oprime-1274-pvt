package com.odigeo.membership;

import com.odigeo.bookingapi.v12.responses.CreditCard;
import com.odigeo.bookingapi.v12.responses.Money;

import java.io.Serializable;
import java.util.List;

public class MemberSubscriptionDetails implements Serializable {

    private Long membershipId;
    private Money totalSavings;
    private List<PrimeBookingInformation> primeBookingsInfo;
    private CreditCard subscriptionPaymentMethod;
    private String subscriptionPaymentMethodType;

    public Long getMembershipId() {
        return membershipId;
    }

    public MemberSubscriptionDetails setMembershipId(Long membershipId) {
        this.membershipId = membershipId;
        return this;
    }

    public Money getTotalSavings() {
        return totalSavings;
    }

    public MemberSubscriptionDetails setTotalSavings(Money totalSavings) {
        this.totalSavings = totalSavings;
        return this;
    }

    public List<PrimeBookingInformation> getPrimeBookingsInfo() {
        return primeBookingsInfo;
    }

    public MemberSubscriptionDetails setPrimeBookingsInfo(List<PrimeBookingInformation> primeBookingsInfo) {
        this.primeBookingsInfo = primeBookingsInfo;
        return this;
    }

    public CreditCard getSubscriptionPaymentMethod() {
        return subscriptionPaymentMethod;
    }

    public MemberSubscriptionDetails setSubscriptionPaymentMethod(CreditCard subscriptionPaymentMethod) {
        this.subscriptionPaymentMethod = subscriptionPaymentMethod;
        return this;
    }

    public String getSubscriptionPaymentMethodType() {
        return subscriptionPaymentMethodType;
    }

    public MemberSubscriptionDetails setSubscriptionPaymentMethodType(String subscriptionPaymentMethodType) {
        this.subscriptionPaymentMethodType = subscriptionPaymentMethodType;
        return this;
    }
}
