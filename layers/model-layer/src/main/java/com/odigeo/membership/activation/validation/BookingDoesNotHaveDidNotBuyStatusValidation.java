package com.odigeo.membership.activation.validation;

import com.odigeo.bookingapi.v12.responses.BookingDetail;

/**
 * Created by roc.arajol on 27-Sep-17.
 */
public class BookingDoesNotHaveDidNotBuyStatusValidation extends AbstractValidation<BookingDetail> {

    public BookingDoesNotHaveDidNotBuyStatusValidation(BookingDetail o) {
        super(o);
    }

    @Override
    public boolean validate() {
        return !"DIDNOTBUY".equals(o.getBookingStatus());
    }
}
