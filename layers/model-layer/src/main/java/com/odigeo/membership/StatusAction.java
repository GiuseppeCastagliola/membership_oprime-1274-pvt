package com.odigeo.membership;

import java.util.Arrays;

public enum StatusAction {
    CREATION, ACTIVATION, EXPIRATION, DEACTIVATION, RENOVATION, PB_IOS_CREATION, PB_ANDROID_CREATION, PB_PHONE_CREATION, PB_EMAIL_CREATION, INTERNAL_CREATION, ONLINE_RENEWAL, DISCARD, UNKNOWN, PB_LANDING_PAGE;

    public static boolean contains(String statusAction) {
        return Arrays.stream(StatusAction.values()).anyMatch(c -> c.name().equals(statusAction));
    }
}
