package com.odigeo.membership.activation.validation.util;

public enum ShoppingCartCollectionState {
    COLLECTED, NOT_COLLECTED, PARTIALLY_COLLECTED, UNKNOWN
}
