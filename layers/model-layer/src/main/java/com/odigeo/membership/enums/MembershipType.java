package com.odigeo.membership.enums;

public enum MembershipType {
    BASIC,
    BASIC_FREE,
    BUSINESS
}
