package com.odigeo.membership.parameters;

import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.enums.SourceType;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.time.LocalDate;

public class MembershipCreationBuilder {

    private static final int MONTHS_DURATION_DEFAULT = 12;

    private Long userId;
    private String name;
    private String lastNames;
    private String website;
    private Long memberAccountId;
    private MemberStatus memberStatus;
    private MembershipRenewal autoRenewal;
    private BigDecimal balance;
    private LocalDate activationDate;
    private LocalDate expirationDate;
    private StatusAction statusAction;
    private MembershipType membershipType;
    private String monthsDuration;
    private SourceType sourceType;
    private BigDecimal subscriptionPrice;
    private BigDecimal renewalPrice;
    private String productStatus;
    private String currencyCode;
    private String recurringId;
    private UserCreation userCreation;

    public int getMonthsDuration() {
        return StringUtils.isNotBlank(monthsDuration) && Integer.parseInt(monthsDuration) > 0 ? Integer.parseInt(monthsDuration) : MONTHS_DURATION_DEFAULT;
    }

    public Long getUserId() {
        return userId;
    }

    public MembershipCreationBuilder withUserId(Long userId) {
        this.userId = userId;
        return this;
    }

    public String getName() {
        return name;
    }

    public MembershipCreationBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public String getLastNames() {
        return lastNames;
    }

    public MembershipCreationBuilder withLastNames(String lastNames) {
        this.lastNames = lastNames;
        return this;
    }

    public String getWebsite() {
        return website;
    }

    public MembershipCreationBuilder withWebsite(String website) {
        this.website = website;
        return this;
    }

    public Long getMemberAccountId() {
        return memberAccountId;
    }

    public MembershipCreationBuilder withMemberAccountId(Long memberAccountId) {
        this.memberAccountId = memberAccountId;
        return this;
    }

    public MemberStatus getMemberStatus() {
        return memberStatus;
    }

    public MembershipCreationBuilder withMemberStatus(MemberStatus memberStatus) {
        this.memberStatus = memberStatus;
        return this;
    }

    public MembershipRenewal getAutoRenewal() {
        return autoRenewal;
    }

    public MembershipCreationBuilder withAutoRenewal(MembershipRenewal autoRenewal) {
        this.autoRenewal = autoRenewal;
        return this;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public MembershipCreationBuilder withBalance(BigDecimal balance) {
        this.balance = balance;
        return this;
    }

    public LocalDate getActivationDate() {
        return activationDate;
    }

    public MembershipCreationBuilder withActivationDate(LocalDate activationDate) {
        this.activationDate = activationDate;
        return this;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public MembershipCreationBuilder withExpirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
        return this;
    }

    public StatusAction getStatusAction() {
        return statusAction;
    }

    public MembershipCreationBuilder withStatusAction(StatusAction statusAction) {
        this.statusAction = statusAction;
        return this;
    }

    public MembershipType getMembershipType() {
        return membershipType;
    }

    public MembershipCreationBuilder withMembershipType(MembershipType membershipType) {
        this.membershipType = membershipType;
        return this;
    }

    public MembershipCreationBuilder withMonthsDuration(String monthsDuration) {
        this.monthsDuration = monthsDuration;
        return this;
    }

    public SourceType getSourceType() {
        return sourceType;
    }

    public MembershipCreationBuilder withSourceType(SourceType sourceType) {
        this.sourceType = sourceType;
        return this;
    }

    public BigDecimal getSubscriptionPrice() {
        return subscriptionPrice;
    }

    public MembershipCreationBuilder withSubscriptionPrice(BigDecimal subscriptionPrice) {
        this.subscriptionPrice = subscriptionPrice;
        return this;
    }

    public BigDecimal getRenewalPrice() {
        return renewalPrice;
    }

    public MembershipCreationBuilder withRenewalPrice(BigDecimal renewalPrice) {
        this.renewalPrice = renewalPrice;
        return this;
    }

    public String getProductStatus() {
        return productStatus;
    }

    public MembershipCreationBuilder withProductStatus(String productStatus) {
        this.productStatus = productStatus;
        return this;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public MembershipCreationBuilder withCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
        return this;
    }

    public String getRecurringId() {
        return recurringId;
    }

    public MembershipCreationBuilder withRecurringId(String recurringId) {
        this.recurringId = recurringId;
        return this;
    }

    public UserCreation getUserCreation() {
        return userCreation;
    }

    public MembershipCreationBuilder withUserCreation(UserCreation userCreation) {
        this.userCreation = userCreation;
        return this;
    }

    public MembershipCreation build() {
        return new MembershipCreation(this);
    }
}
