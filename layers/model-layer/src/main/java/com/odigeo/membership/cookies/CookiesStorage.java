package com.odigeo.membership.cookies;

import java.io.Serializable;
import java.util.Collection;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

public class CookiesStorage implements Serializable {

    private static final String JSESSION_ID_COOKIE = "JSESSIONID";

    private static final String DAPI_COOKIES_PREFIX = "DAPI.";

    private final ConcurrentHashMap<String, Cookie> cookies;

    private final String namespace;

    private final String dapiSessionCookieName;

    private CookiesStorage(String namespace, String dapiSessionCookieName) {
        this.cookies = new ConcurrentHashMap<>();
        this.namespace = namespace;
        this.dapiSessionCookieName = dapiSessionCookieName;
    }

    CookiesStorage() {
        this(DAPI_COOKIES_PREFIX, JSESSION_ID_COOKIE);
    }

    public void addCookie(Cookie cookie) {
        this.cookies.put(cookie.getName(), cookie);
    }

    public Collection<Cookie> getCookies() {
        return this.cookies.values();
    }

    public void clear() {
        this.cookies.clear();
    }

    public String getId() {
        return Optional.ofNullable(this.cookies.get(this.namespace + this.dapiSessionCookieName)).map(Cookie::getValue).orElse(null);
    }

    synchronized Cookie getCookie(String cookieName) {
        return Optional.ofNullable(this.cookies.get(cookieName)).orElse(this.cookies.get(this.namespace + cookieName));
    }
}
