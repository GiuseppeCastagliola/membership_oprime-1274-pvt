package com.odigeo.membership.propertiesconfig;

import com.google.inject.Singleton;
import com.odigeo.membership.exception.DataNotFoundException;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Singleton
class PropertiesConfigurationStore {

    private static final String MISSING_CONFIGURATION_KEY = "Missing Configuration Key: ";
    private static final String GET_CONFIG_VALUE_BY_KEY = "SELECT VALUE FROM GE_MEMBERSHIP_CONFIG mc WHERE mc.KEY = ? ";
    private static final String UPDATE_CONFIG_VALUE_BY_KEY = "UPDATE GE_MEMBERSHIP_CONFIG SET VALUE = ? WHERE KEY = ?";

    boolean getConfigValueByKey(final DataSource dataSource, final String key) throws SQLException, DataNotFoundException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(GET_CONFIG_VALUE_BY_KEY)) {
            preparedStatement.setString(1, key);
            try (ResultSet rs = preparedStatement.executeQuery()) {
                if (rs.next()) {
                    return rs.getInt("VALUE") == 0 ? Boolean.FALSE : Boolean.TRUE;
                }
            }
        }
        throw new DataNotFoundException(MISSING_CONFIGURATION_KEY + key);
    }

    boolean updateConfigValueByKey(final DataSource dataSource, final String key, final boolean value) throws SQLException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_CONFIG_VALUE_BY_KEY)) {
            preparedStatement.setInt(1, value ? 1 : 0);
            preparedStatement.setString(2, key);
            return preparedStatement.executeUpdate() > 0;
        }
    }
}
