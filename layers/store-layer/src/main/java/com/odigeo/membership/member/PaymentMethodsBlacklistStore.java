package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.odigeo.membership.BlacklistedPaymentMethod;
import com.odigeo.membership.exception.DataAccessRollbackException;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static java.sql.Statement.SUCCESS_NO_INFO;

public class PaymentMethodsBlacklistStore {

    private static final String FETCH_BLACKLISTED_PAYMENT_METHODS = "SELECT ID, ERROR_MESSAGE, ERROR_TYPE, TIMESTAMP FROM GE_PAYMENT_METHOD_BLACKLIST WHERE MEMBERSHIP_ID = ?";
    private static final String INSERT_BLACKLIST_PAYMENT_METHODS = "INSERT INTO GE_PAYMENT_METHOD_BLACKLIST (ID, ERROR_MESSAGE, ERROR_TYPE, TIMESTAMP, MEMBERSHIP_ID) VALUES (?, ?, ?, ?, ?)";

    public List<BlacklistedPaymentMethod> fetchBlackListedPaymentMethods(DataSource dataSource, Long membershipId) throws DataAccessException {

        List<BlacklistedPaymentMethod> toReturn = new ArrayList<>();

        try (Connection connection = dataSource.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(FETCH_BLACKLISTED_PAYMENT_METHODS)) {
            preparedStatement.setLong(1, membershipId);
            try (ResultSet rs = preparedStatement.executeQuery()) {
                while (rs.next()) {
                    BlacklistedPaymentMethod blackListedPaymentMethod = new BlacklistedPaymentMethod();
                    blackListedPaymentMethod.setErrorMessage(rs.getString("ERROR_MESSAGE"));
                    blackListedPaymentMethod.setErrorType(rs.getString("ERROR_TYPE"));
                    blackListedPaymentMethod.setTimestamp(new Date(rs.getTimestamp("TIMESTAMP").getTime()));
                    blackListedPaymentMethod.setId(rs.getLong("ID"));
                    blackListedPaymentMethod.setMembershipId(membershipId);
                    toReturn.add(blackListedPaymentMethod);
                }
            }
        } catch (SQLException e) {
            throw new DataAccessException(e);
        }
        return toReturn;
    }

    public Boolean addToBlackList(DataSource dataSource, List<BlacklistedPaymentMethod> blacklistedPaymentMethods) throws DataAccessException {
        try (Connection connection = dataSource.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(INSERT_BLACKLIST_PAYMENT_METHODS)) {
            for (BlacklistedPaymentMethod blackListedPaymentMethod : blacklistedPaymentMethods) {
                preparedStatement.setLong(1, blackListedPaymentMethod.getId());
                preparedStatement.setString(2, blackListedPaymentMethod.getErrorMessage());
                preparedStatement.setString(3, blackListedPaymentMethod.getErrorType());
                preparedStatement.setTimestamp(4, new Timestamp(blackListedPaymentMethod.getTimestamp().getTime()));
                preparedStatement.setLong(5, blackListedPaymentMethod.getMembershipId());
                preparedStatement.addBatch();
            }
            for (int batchExecutionResult : preparedStatement.executeBatch()) {
                if (batchExecutionResult != SUCCESS_NO_INFO) {
                    return false;
                }
            }
        } catch (SQLException e) {
            throw new DataAccessRollbackException(e);
        }
        return true;
    }
}
