package com.odigeo.membership.member;

import com.google.inject.Singleton;
import com.odigeo.db.DbUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@Singleton
class AuditMemberAccountStore {

    private static final String INSERT_AUDIT_MEMBER_ACCOUNT_SQL = "INSERT INTO GE_AUDIT_MEMBER_ACCOUNT(ID,MEMBER_ACCOUNT_ID,USER_ID,FIRST_NAME,LAST_NAME) VALUES (?, ?, ?, ?, ?) ";
    private static final String SELECT_NEXT_AUDIT_MEMBER_ACCOUNT_ID = "SELECT SEQ_GE_AUDIT_MEMBER_ACCOUNT_ID.nextval FROM dual ";

    void insertAuditMemberAccount(final DataSource dataSource, final AuditMemberAccount auditMemberAccount) throws SQLException {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_AUDIT_MEMBER_ACCOUNT_SQL)) {
            final long auditId = DbUtils.nextSequence(connection.prepareStatement(SELECT_NEXT_AUDIT_MEMBER_ACCOUNT_ID));
            preparedStatement.setLong(1, auditId);
            preparedStatement.setLong(2, auditMemberAccount.getMemberAccountId());
            preparedStatement.setLong(3, auditMemberAccount.getUserId());
            preparedStatement.setString(4, auditMemberAccount.getName());
            preparedStatement.setString(5, auditMemberAccount.getLastName());
            preparedStatement.execute();
        }
    }

}
