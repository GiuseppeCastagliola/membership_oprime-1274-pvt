package com.odigeo.membership.propertiesconfig;

import com.edreams.base.DataAccessException;
import com.odigeo.membership.exception.DataNotFoundException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.sql.SQLException;

import static com.odigeo.membership.propertiesconfig.PropertiesConfigurationStoreKeys.SEND_IDS_TO_KAFKA;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class PropertiesConfigurationManagerTest {

    private static final String SEND_IDS_TO_KAFKA_KEY = "SEND_IDS_TO_KAFKA";
    private static final String TEST_KEY = "test key";
    private PropertiesConfigurationManager propertiesConfigurationManager;
    private PropertiesConfigurationStore propertiesConfigurationStoreMock;
    private DataSource dataSourceMock;

    @BeforeMethod
    public void setUp() {
        propertiesConfigurationStoreMock = mock(PropertiesConfigurationStore.class);
        propertiesConfigurationManager = new PropertiesConfigurationManager(propertiesConfigurationStoreMock);

        dataSourceMock = mock(DataSource.class);
    }

    @Test
    public void testConfigForSendingIdsToKafkaIsTrue() throws SQLException, DataNotFoundException {
        //Given
        given(propertiesConfigurationStoreMock.getConfigValueByKey(dataSourceMock, SEND_IDS_TO_KAFKA_KEY)).willReturn(true);
        //When
        boolean config = propertiesConfigurationManager.isConfigurableFeatureActive(dataSourceMock, SEND_IDS_TO_KAFKA);
        //Then
        assertTrue(config);
    }

    @Test
    public void testConfigForSendingIdsToKafkaIsFalse() throws SQLException, DataNotFoundException {
        //Given
        given(propertiesConfigurationStoreMock.getConfigValueByKey(dataSourceMock, SEND_IDS_TO_KAFKA_KEY)).willReturn(false);
        //When
        boolean config = propertiesConfigurationManager.isConfigurableFeatureActive(dataSourceMock, SEND_IDS_TO_KAFKA);
        //Then
        assertFalse(config);
    }

    @Test(expectedExceptions = SQLException.class, expectedExceptionsMessageRegExp = "expected SQL exception")
    public void testSqlExceptionWhenConfigForSendingIdsToKafkaThrowsSqlException() throws SQLException, DataNotFoundException {
        //Given
        given(propertiesConfigurationStoreMock.getConfigValueByKey(dataSourceMock, SEND_IDS_TO_KAFKA_KEY)).willThrow(new SQLException("expected SQL exception"));
        //When
        propertiesConfigurationManager.isConfigurableFeatureActive(dataSourceMock, SEND_IDS_TO_KAFKA);
    }

    @Test(expectedExceptions = DataNotFoundException.class, expectedExceptionsMessageRegExp = "expected data exception")
    public void testDataExceptionWhenConfigForSendingIdsToKafkaThrowsDataException() throws SQLException, DataNotFoundException {
        //Given
        given(propertiesConfigurationStoreMock.getConfigValueByKey(dataSourceMock, SEND_IDS_TO_KAFKA_KEY)).willThrow(new DataNotFoundException("expected data exception"));
        //When
        propertiesConfigurationManager.isConfigurableFeatureActive(dataSourceMock, SEND_IDS_TO_KAFKA);
    }

    @Test
    public void testUpdatePropertyValueIsUpdated() throws SQLException, DataAccessException {
        //Given
        boolean value = false;
        given(propertiesConfigurationStoreMock.updateConfigValueByKey(dataSourceMock, TEST_KEY, value)).willReturn(true);
        //When
        boolean result = propertiesConfigurationManager.updatePropertyValue(dataSourceMock, TEST_KEY, value);
        //Then
        assertTrue(result);
    }

    @Test
    public void testUpdatePropertyValueIsNotUpdated() throws SQLException, DataAccessException {
        //Given
        boolean value = false;
        given(propertiesConfigurationStoreMock.updateConfigValueByKey(dataSourceMock, TEST_KEY, value)).willReturn(false);
        //When
        boolean result = propertiesConfigurationManager.updatePropertyValue(dataSourceMock, TEST_KEY, value);
        //Then
        assertFalse(result);
    }

    @Test(expectedExceptions = DataAccessException.class, expectedExceptionsMessageRegExp = "Cannot update the property test key with the value false.")
    public void testUpdatePropertyValueThrowsDataAccessExceptionWhenSqlException() throws SQLException, DataAccessException {
        //Given
        boolean value = false;
        given(propertiesConfigurationStoreMock.updateConfigValueByKey(dataSourceMock, TEST_KEY, value)).willThrow(new SQLException("expected exception"));
        //When
        propertiesConfigurationManager.updatePropertyValue(dataSourceMock, TEST_KEY, value);
    }
}