package com.odigeo.membership.member;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.ProductStatus;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.exception.DataNotFoundException;
import com.odigeo.membership.parameters.search.MemberAccountSearch;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class MemberAccountStoreTest {

    private static final int OK = 1;
    private static final long TEST_ACCOUNT_ID = 1L;
    private static final String TEST_NAME = "Solid";
    private static final String TEST_LAST_NAMES = "Snake";
    private static final int USER_CREATED_CODE = 0;
    private static final String WEBSITE = "ES";
    private static final long USER_ID = 123L;
    public static final long MEMBERSHIP_ID = 4321L;
    private static final MembershipType MEMBERSHIP_TYPE = MembershipType.BASIC;
    private static final LocalDateTime TIMESTAMP = LocalDateTime.now();
    private static final Membership MEMBERSHIP = new MembershipBuilder().setId(MEMBERSHIP_ID).setWebsite(WEBSITE).setStatus(MemberStatus.ACTIVATED)
            .setMembershipRenewal(MembershipRenewal.ENABLED).setActivationDate(LocalDateTime.now())
            .setExpirationDate(LocalDateTime.now().plusMonths(12)).setMemberAccountId(TEST_ACCOUNT_ID).setProductStatus(ProductStatus.CONTRACT).build();
    private static final MemberAccount MEMBER_ACCOUNT = new MemberAccount(TEST_ACCOUNT_ID, USER_ID, TEST_NAME, TEST_LAST_NAMES)
            .setTimestamp(TIMESTAMP)
            .setMemberships(Collections.singletonList(MEMBERSHIP));
    private MemberAccountStore store;

    @Mock
    private ResultSet resultSet;
    @Mock
    private PreparedStatement preparedStatement;
    @Mock
    private Connection connection;
    @Mock
    private DataSource dataSource;

    @BeforeMethod
    public void before() throws Exception {
        MockitoAnnotations.initMocks(this);
        mockDatabase();
        ConfigurationEngine.init();
        store = ConfigurationEngine.getInstance(MemberAccountStore.class);
    }

    @Test
    public void testFetchMemberAccountByMemberAccountId() throws Exception {
        testFetchSimpleClientFromDatabase();
        List<MemberAccount> actualMemberAccount = store.getMemberAccountAndMembershipsActivated(dataSource, TEST_ACCOUNT_ID);
        assertEqualsMemberAccount(actualMemberAccount.get(0), MEMBER_ACCOUNT);
    }

    @Test
    public void testGetMemberAccountById() throws Exception {
        testFetchSimpleClientFromDatabase();
        MemberAccount actualMemberAccount = store.getMemberAccountById(dataSource, TEST_ACCOUNT_ID);
        assertEqualsMemberAccount(actualMemberAccount, MEMBER_ACCOUNT);
    }

    @Test(expectedExceptions = DataNotFoundException.class)
    public void testGetMemberAccountByIdNotGettingResults() throws Exception {
        resultSetWithNoResults();
        store.getMemberAccountById(dataSource, TEST_ACCOUNT_ID);
    }

    @Test
    public void testSearchAccounts() throws SQLException {
        testFetchSimpleClientFromDatabase();
        MemberAccountSearch memberAccountSearch = new MemberAccountSearch.Builder().userId(USER_ID).build();
        List<MemberAccount> memberAccounts = store.searchMemberAccounts(dataSource, memberAccountSearch);
        MemberAccount memberAccountResult = memberAccounts.get(0);
        assertEqualsMemberAccount(memberAccountResult, MEMBER_ACCOUNT);
        assertTrue(memberAccountResult.getMemberships().isEmpty());
    }

    @Test
    public void testSearchAccountsNoResult() throws SQLException {
        resultSetWithNoResults();
        MemberAccountSearch memberAccountSearch = new MemberAccountSearch.Builder().userId(USER_ID).build();
        List<MemberAccount> memberAccounts = store.searchMemberAccounts(dataSource, memberAccountSearch);
        assertTrue(memberAccounts.isEmpty());
    }

    @Test
    public void testSearchAccountsWithMemberships() throws SQLException {
        testFetchSimpleClientFromDatabase();
        MemberAccountSearch memberAccountSearch = new MemberAccountSearch.Builder().userId(USER_ID).withMembership(true).build();
        List<MemberAccount> memberAccounts = store.searchMemberAccounts(dataSource, memberAccountSearch);
        MemberAccount memberAccountResult = memberAccounts.get(0);
        assertEqualsMemberAccount(memberAccountResult, MEMBER_ACCOUNT);
        assertFalse(memberAccountResult.getMemberships().isEmpty());
        assertEquals(memberAccountResult.getMemberships().get(0).getId().longValue(), MEMBERSHIP_ID);
    }

    private void assertEqualsMemberAccount(final MemberAccount account, final MemberAccount expectedAccount) {
        assertEquals(account, expectedAccount);
        assertEquals(account.getId(), expectedAccount.getId());
        assertEquals(account.getUserId(), expectedAccount.getUserId());
        assertEquals(account.getName(), expectedAccount.getName());
        assertEquals(account.getLastNames(), expectedAccount.getLastNames());
        assertEquals(account.getTimestamp(), expectedAccount.getTimestamp());
    }

    private void setMocksWithMembershipFromDatabase() throws SQLException {
        when(resultSet.getLong("USER_ID")).thenReturn(USER_ID);
        when(resultSet.getString("FIRST_NAME")).thenReturn(TEST_NAME);
        when(resultSet.getString("LAST_NAME")).thenReturn(TEST_LAST_NAMES);
        when(resultSet.getLong("ACCOUNT_ID")).thenReturn(TEST_ACCOUNT_ID);
        when(resultSet.getLong("MEMBER_ACCOUNT_ID")).thenReturn(TEST_ACCOUNT_ID);
        when(resultSet.getLong("ID")).thenReturn(MEMBERSHIP_ID);
        when(resultSet.getString("WEBSITE")).thenReturn(WEBSITE);
        when(resultSet.getString("STATUS")).thenReturn(MemberStatus.ACTIVATED.toString());
        when(resultSet.getString("AUTO_RENEWAL")).thenReturn(MembershipRenewal.ENABLED.toString());
        when(resultSet.getTimestamp("EXPIRATION_DATE")).thenReturn(new Timestamp(1L));
        when(resultSet.getString("MEMBERSHIP_TYPE")).thenReturn(MEMBERSHIP_TYPE.toString());
        when(resultSet.getTimestamp("TIMESTAMP")).thenReturn(Timestamp.valueOf(TIMESTAMP));
    }

    private void testFetchSimpleClientFromDatabase() throws SQLException {
        resultSetWithOneResult();
        setMocksWithMembershipFromDatabase();
    }

    private void resultSetWithOneResult() throws SQLException {
        when(resultSet.next()).thenReturn(true).thenReturn(false);
    }

    private void resultSetWithNoResults() throws SQLException {
        when(resultSet.next()).thenReturn(false);
    }

    private void mockDatabase() throws SQLException {
        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);
    }

    @Test(expectedExceptions = SQLException.class)
    public void testCreateMemberAccountThrowsExceptionIfNoNextValGiven() throws Exception {
        setUpValidConnectionAndPreparedStatement();
        store.createMemberAccount(dataSource, TEST_ACCOUNT_ID, TEST_NAME, TEST_LAST_NAMES);
    }

    @Test
    public void testCreateMemberAccount() throws Exception {
        when(resultSet.next()).thenReturn(true).thenReturn(false);
        long result = store.createMemberAccount(dataSource, TEST_ACCOUNT_ID, TEST_NAME,
                TEST_LAST_NAMES);
        assertEquals(result, USER_CREATED_CODE);
    }

    @Test
    public void testUpdateMemberAccountNames() throws Exception {
        when(preparedStatement.executeUpdate()).thenReturn(1);
        assertTrue(store.updateMemberAccountNames(dataSource, TEST_ACCOUNT_ID, TEST_NAME, TEST_LAST_NAMES));
    }

    @Test
    public void testUpdateMemberAccountUserId() throws Exception {
        when(preparedStatement.executeUpdate()).thenReturn(1);
        assertTrue(store.updateMemberAccountUserId(dataSource, TEST_ACCOUNT_ID, USER_ID));
    }

    @Test
    public void testGetMemberAccountAndMembershipsActivatedOrToRetry() throws Exception {
        resultSetWithOneResult();
        setMocksWithMembershipFromDatabase();
        List<MemberAccount> activatedOrToRetry = store.getMemberAccountAndMembershipsActivated(dataSource, USER_ID);
        assertEquals(MEMBERSHIP, activatedOrToRetry.get(0).getMemberships().get(0));
    }

    @Test
    public void testGetMemberAccountWithMembershipById() throws SQLException, DataNotFoundException {
        testFetchSimpleClientFromDatabase();
        MemberAccount memberAccountResult = store.getMemberAccountWithMembershipById(dataSource, TEST_ACCOUNT_ID);
        assertEqualsMemberAccount(memberAccountResult, MEMBER_ACCOUNT);
        assertEquals(MEMBERSHIP, memberAccountResult.getMemberships().get(0));
    }

    @Test(expectedExceptions = DataNotFoundException.class)
    public void testGetMemberAccountWithMembershipNoResults() throws SQLException, DataNotFoundException {
        resultSetWithNoResults();
        store.getMemberAccountWithMembershipById(dataSource, TEST_ACCOUNT_ID);
    }

    private void setUpValidConnectionAndPreparedStatement() throws SQLException {
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeUpdate()).thenReturn(OK);
    }
}
