package com.odigeo.membership.member;

import com.odigeo.membership.MemberAccount;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

public class AuditMemberAccountManagerTest {

    private AuditMemberAccountManager auditMemberAccountManager;
    @Mock
    private AuditMemberAccountStore auditMemberAccountStoreMock;
    @Mock
    private MemberAccountStore memberAccountStoreMock;
    @Mock
    private DataSource dataSource;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        auditMemberAccountManager = new AuditMemberAccountManager(auditMemberAccountStoreMock, memberAccountStoreMock);
    }

    @Test
    public void testAuditUpdateMemberAccountSavesAllChanges() throws Exception {
        //Given
        final Long memberAccountId = 435L;
        final Long userId = 883L;
        final String name = "account name";
        final String lastName = "last name";
        final MemberAccount updatedMemberAccount = new MemberAccount(memberAccountId, userId, name, lastName);
        given(memberAccountStoreMock.getMemberAccountById(dataSource, memberAccountId)).willReturn(updatedMemberAccount);
        //When
        auditMemberAccountManager.auditUpdatedMemberAccount(dataSource, memberAccountId);
        //Then
        verify(auditMemberAccountStoreMock).insertAuditMemberAccount(dataSource, AuditMemberAccount.builder()
                .memberAccountId(memberAccountId)
                .userId(userId)
                .name(name)
                .lastName(lastName)
                .build());
    }

    @Test
    public void testAuditNewMemberAccountSavesAllChanges() throws Exception {
        //Given
        final AuditMemberAccount auditMemberAccount = AuditMemberAccount.builder().memberAccountId(434L).build();
        //When
        auditMemberAccountManager.auditNewMemberAccount(dataSource, auditMemberAccount);
        //Then
        verify(auditMemberAccountStoreMock).insertAuditMemberAccount(dataSource, auditMemberAccount);
    }
}