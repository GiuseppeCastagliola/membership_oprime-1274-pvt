package com.odigeo.membership.tracking;

import com.odigeo.membership.AutoRenewalOperation;
import com.odigeo.membership.AutorenewalTracking;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;

public class AutorenewalTrackingStoreTest {

    private static final String REQUESTER = "requester";
    private static final String REQUESTED_METHOD = "requested_method";
    private static final String MEMBERSHIP_ID = "2205";
    @Mock
    private ResultSet resultSet;
    @Mock
    private PreparedStatement preparedStatement;
    @Mock
    private Connection connection;
    @Mock
    private DataSource dataSource;

    private AutorenewalTrackingStore autorenewalTrackingStore;

    @Captor
    private ArgumentCaptor<Timestamp> timestampCaptor;

    @BeforeMethod
    public void setUp() throws SQLException {
        MockitoAnnotations.initMocks(this);
        mockDatabase();
        autorenewalTrackingStore = new AutorenewalTrackingStore();
    }

    @Test
    public void testInsertAutorenewalTracking() throws SQLException {
        AutorenewalTracking autorenewalTracking = createAutorenewalTrackingBuilder(AutoRenewalOperation.ENABLE_AUTO_RENEW);
        autorenewalTrackingStore.insertAutorenewalTracking(dataSource, autorenewalTracking);
        verify(preparedStatement).executeUpdate();
        verify(preparedStatement, times(4)).setString(anyInt(), anyString());
        verify(preparedStatement).setTimestamp(anyInt(), timestampCaptor.capture());
        assertNotNull(timestampCaptor.getValue());
    }

    private void mockDatabase() throws SQLException {
        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);
    }

    private AutorenewalTracking createAutorenewalTrackingBuilder(AutoRenewalOperation operation) {
        AutorenewalTracking autorenewalTracking = new AutorenewalTracking();
        autorenewalTracking.setRequester(REQUESTER);
        autorenewalTracking.setRequestedMethod(REQUESTED_METHOD);
        autorenewalTracking.setAutoRenewalOperation(operation);
        autorenewalTracking.setMembershipId(MEMBERSHIP_ID);
        return autorenewalTracking;
    }
}