import React, { Component } from 'react';
import { Table, Modal, Button} from 'antd';

class Instructions extends Component {
    state = { visible: false }

    showModal = () => {
        this.setState({
            visible: true,
        });
    }

    handleOk = () => {
        this.setState({
            visible: false,
        });
    }

    handleCancel = () => {
        this.setState({
            visible: false,
        });
    }

    render() {
        const columns = [
            {
                title: 'user_id',
                dataIndex: 'user_id',
                key: 'user_id',
            },
            {
                title: 'first_name',
                dataIndex: 'first_name',
                key: 'first_name',
            },
            {
                title: 'surname',
                dataIndex: 'surname',
                key: 'surname',
            },
            {
                title: 'website',
                key: 'website',
                dataIndex: 'website',
            },
            {
                title: 'email',
                key: 'email',
                dataIndex: 'email',
            },
            {
                title: 'monthsToRenewal',
                key: 'monthsToRenewal',
                dataIndex: 'monthsToRenewal',
            },
            {
                title: 'membershipType',
                key: 'membershipType',
                dataIndex: 'membershipType',
            }
        ];

        const data = [{
            key: '1',
            user_id: '197697',
            first_name: 'Manolo',
            surname: 'Garcia',
            website: 'ES',
            email: 'foo@gmail.com',
            monthsToRenewal: 6,
            membershipType: 'BASIC',
        }, {
            key: '2',
            user_id: '197698',
            first_name: 'Fleur',
            surname: 'Bisset',
            website: 'FR',
            email: 'bar@gmail.com',
            monthsToRenewal: 3,
            membershipType: 'BUSINESS',
        }];

        return (
            <div>
                <a type="primary" onClick={this.showModal}>
                    File structure
                </a>
                <Modal
                    title="File example"
                    width={810}
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                >
                    <Table columns={columns} dataSource={data} pagination={false}/>
                    <code>
                        user_id;first_name;surname;website;email;monthsToRenewal;membershipType<br/>
                        197697;Manolo;Garcia;ES;foo@gmail.com;3;BASIC<br/>
                        197697;Fleur;Bisset;FR;bar@gmail.com;6;BUSINESS
                    </code>
                    <br/>
                    <div>
                        <b>Post-booking:</b> When the monthsToRenewal column is empty by default it's 12 months<br/>
                        <b>Internal employees:</b> email and monthsToRenewal columns not needed<br/>
                        <b>Membership type:</b> 'BASIC' or 'BUSINESS'. If not specified will be setted as 'BASIC' by default
                    </div>
                </Modal>
            </div>
        );
    }
}

export default Instructions;
