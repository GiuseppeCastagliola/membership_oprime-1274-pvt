import React, { Component } from 'react';
import Bem from 'react-bem-helper';
import { Icon, Steps } from 'antd';
import './style.scss';

const classes = new Bem('breadcrumb');
const { Step } = Steps;

class Breadcrumb extends Component {
    render() {
        return (
            <div {...classes()}>
                <Steps current={this.props.current} status={this.props.status}>
                    <Step title="Upload file" icon={<Icon type="upload" />} />
                    <Step title="Verification" icon={<Icon type="solution" />} />
                    <Step title="Done" icon={<Icon type="smile-o" />} />
                </Steps>
            </div>
        );
    }
}

export default Breadcrumb;
