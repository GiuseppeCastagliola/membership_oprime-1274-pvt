import React, {Component} from 'react';
import {Link} from "react-router-dom";
import {hot} from 'react-hot-loader'
import {Footer, Header} from 'Components';

class App extends Component {
    render() {
        return (
            <div>
                <Header text="Welcome to Membership"/>
                <Link to="/postbooking">PostBooking</Link>
                <Footer />
            </div>
        );
    }
}

export default hot(module)(App)

