package com.odigeo.membership.member.rest.utils;

import com.odigeo.membership.cookies.Cookie;
import com.odigeo.membership.cookies.CookiesContext;
import org.apache.commons.lang.StringUtils;

import javax.ws.rs.HttpMethod;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static java.lang.Math.toIntExact;

public final class ResponseBuilder {

    private static final List<String> STATES = Arrays.asList(HttpMethod.GET, HttpMethod.HEAD);

    private ResponseBuilder() {
    }

    public static Response buildResponse(final Object entity, final Request req) {
        return buildResponse(entity, req, null);
    }

    static Response buildResponse(final Object entity, final Request request, final CookiesContext cookiesContext) {
        return buildResponseBuilder(entity, request, cookiesContext).build();
    }

    private static Response.ResponseBuilder buildResponseBuilder(final Object entity, final Request request, final CookiesContext cookiesContext) {
        if (entity == null || entity.hashCode() == 0) {
            final Response.ResponseBuilder noContentBuilder = Response.noContent();
            Optional.ofNullable(cookiesContext)
                    .ifPresent(context -> context.getStorage().getCookies().forEach(cookie -> noContentBuilder.cookie(map(cookie))));
            return noContentBuilder;
        }

        int eTagValue = entity.hashCode();
        Response.ResponseBuilder rb;
        final EntityTag eTag = new EntityTag(String.valueOf(eTagValue));

        if (STATES.contains(request.getMethod())) {
            rb = Optional.ofNullable(request.evaluatePreconditions(eTag))
                    .filter(ResponseBuilder::checkStatus)
                    .map(responseBuilder -> responseBuilder.entity(StringUtils.EMPTY))
                    .orElseGet(() -> Response.ok(entity));
        } else {
            rb = Response.ok(entity);
            if (HttpMethod.POST.equals(request.getMethod())) {
                rb.status(Response.Status.CREATED);
            }
        }

        rb.tag(eTag);
        Optional.ofNullable(cookiesContext)
                .ifPresent(context -> context.getStorage().getCookies().forEach(cookie -> rb.cookie(map(cookie))));
        return rb;
    }

    private static boolean checkStatus(final Response.ResponseBuilder responseBuilder) {
        int status = responseBuilder.build().getStatus();
        return status == 412 || status == 304;
    }

    private static NewCookie map(Cookie mslCookie) {
        final LocalDateTime currentTime = LocalDateTime.now();
        final int maxAge = Optional.ofNullable(mslCookie)
                .map(Cookie::getExpirationDate)
                .map(expirationDate -> ChronoUnit.NANOS.between(expirationDate, currentTime))
                .filter(l -> l >= 0)
                .map(date -> toIntExact((date / 1000)))
                .orElse(NewCookie.DEFAULT_MAX_AGE);
        return new NewCookie(mslCookie.getName(), mslCookie.getValue(), mslCookie.getPath(),
                mslCookie.getDomain(), "", maxAge, mslCookie.isSecure());
    }
}
