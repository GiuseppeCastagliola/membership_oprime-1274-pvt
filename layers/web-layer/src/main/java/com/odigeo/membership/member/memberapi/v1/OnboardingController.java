package com.odigeo.membership.member.memberapi.v1;

import com.edreams.base.DataAccessException;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.OnboardingService;
import com.odigeo.membership.exception.MembershipServiceException;
import com.odigeo.membership.member.memberapi.v1.util.MembershipExceptionMapper;
import com.odigeo.membership.member.memberapi.v1.util.OnboardingMapper;
import com.odigeo.membership.onboarding.OnboardingEventService;
import com.odigeo.membership.request.onboarding.OnboardingEventRequest;

import static java.util.Objects.isNull;

public class OnboardingController implements OnboardingService {

    private OnboardingMapper onboardingMapper;

    @Override
    public Long saveOnboardingEvent(OnboardingEventRequest request) throws MembershipServiceException {
        try {
            return getOnboardingService().createOnboarding(getOnboardingMapper().map(request));
        } catch (DataAccessException e) {
            throw MembershipExceptionMapper.map(e);
        }
    }

    private OnboardingEventService getOnboardingService() {
        return ConfigurationEngine.getInstance(OnboardingEventService.class);
    }

    private OnboardingMapper getOnboardingMapper() {
        if (isNull(onboardingMapper)) {
            onboardingMapper = ConfigurationEngine.getInstance(OnboardingMapper.class);
        }
        return onboardingMapper;
    }
}
