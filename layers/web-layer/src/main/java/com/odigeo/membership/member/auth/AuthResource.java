package com.odigeo.membership.member.auth;


import com.odigeo.membership.auth.AuthorizationRequest;
import com.odigeo.membership.auth.LoginRequest;
import com.odigeo.membership.auth.LogoutRequest;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;


@Path("/membership/auth")
@Produces("application/json; charset=UTF-8")
public interface AuthResource {

    @POST
    @Path("login/{username}")
    Response login(@Context final Request request, @PathParam("username") final String userName,
                   final LoginRequest loginRequest);

    @POST
    @Path("logout")
    Response logout(@Context final Request request, final LogoutRequest logoutRequest);

    @POST
    @Path("permissions")
    Response getTokenPermissions(@Context final Request request, final AuthorizationRequest authorizationRequest);
}
