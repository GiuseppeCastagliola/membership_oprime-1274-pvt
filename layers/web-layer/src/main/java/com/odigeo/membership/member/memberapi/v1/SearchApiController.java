package com.odigeo.membership.member.memberapi.v1;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipSearchApi;
import com.odigeo.membership.exception.MembershipBadRequestException;
import com.odigeo.membership.mapper.GeneralMapperCreator;
import com.odigeo.membership.member.MemberAccountService;
import com.odigeo.membership.member.MemberService;
import com.odigeo.membership.member.memberapi.util.LogUtils;
import com.odigeo.membership.member.memberapi.v1.util.MembershipExceptionMapper;
import com.odigeo.membership.parameters.search.MemberAccountSearch;
import com.odigeo.membership.parameters.search.MembershipSearch;
import com.odigeo.membership.request.search.MemberAccountSearchRequest;
import com.odigeo.membership.request.search.MembershipSearchRequest;
import com.odigeo.membership.response.search.MemberAccountResponse;
import com.odigeo.membership.response.search.MembershipResponse;
import com.odigeo.membership.search.SearchService;
import ma.glasnost.orika.MapperFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import static java.util.Objects.nonNull;

public class SearchApiController implements MembershipSearchApi {
    private static final Logger LOGGER = LoggerFactory.getLogger(SearchApiController.class);
    private static final String SEARCH_BAD_REQUEST_MESSAGE = "To perform a search at least one search parameter should be set!";

    private SearchService searchService;
    private MemberAccountService memberAccountService;
    private MemberService membershipService;
    private final MapperFacade mapperFacade;
    private final Function<Membership, MembershipResponse> membershipToMembershipResponse;

    public SearchApiController() {
        this.mapperFacade = new GeneralMapperCreator().getMapper();
        this.membershipToMembershipResponse = membership -> mapperFacade.map(membership, MembershipResponse.class);
    }

    @Override
    public MemberAccountResponse getMemberAccount(Long memberAccountId, boolean withMemberships) {
        try {
            return Optional.of(getMemberAccountService().getMemberAccountById(memberAccountId, withMemberships))
                    .map(memberAccount -> mapperFacade.map(memberAccount, MemberAccountResponse.class))
                    .orElse(null);
        } catch (MissingElementException | DataAccessException e) {
            LogUtils.defaultLoggerError(LOGGER, e);
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    public MembershipResponse getMembership(Long membershipId, boolean withMemberAccount) {
        try {
            return Optional.of(getMembershipById(membershipId, withMemberAccount))
                    .map(membershipToMembershipResponse)
                    .orElse(null);
        } catch (MissingElementException | DataAccessException e) {
            LogUtils.defaultLoggerError(LOGGER, e);
            throw MembershipExceptionMapper.map(e);
        }
    }

    private Membership getMembershipById(Long membershipId, boolean withMemberAccount) throws MissingElementException, DataAccessException {
        if (withMemberAccount) {
            return getMembershipService().getMembershipByIdWithMemberAccount(membershipId);
        }
        return getMembershipService().getMembershipById(membershipId);
    }

    @Override
    public List<MemberAccountResponse> searchAccounts(MemberAccountSearchRequest searchRequest) {
        MemberAccountSearch memberAccountSearch = buildMemberAccountSearch(searchRequest, false);
        if (memberAccountSearch.isEmpty()) {
            throw new MembershipBadRequestException(SEARCH_BAD_REQUEST_MESSAGE);
        }
        try {
            return Optional.of(getSearchService().searchMemberAccounts(memberAccountSearch))
                    .map(accounts -> mapperFacade.mapAsList(accounts, MemberAccountResponse.class))
                    .orElseGet(Collections::emptyList);
        } catch (DataAccessException e) {
            LogUtils.defaultLoggerError(LOGGER, e);
            throw MembershipExceptionMapper.map(e);
        }
    }


    @Override
    public List<MembershipResponse> searchMemberships(MembershipSearchRequest searchRequest) {
        MembershipSearch membershipSearch = buildMembershipSearch(searchRequest);
        if (membershipSearch.isEmpty()) {
            throw new MembershipBadRequestException(SEARCH_BAD_REQUEST_MESSAGE);
        }
        try {
            return Optional.of(getSearchService().searchMemberships(membershipSearch))
                    .map(memberships -> mapperFacade.mapAsList(memberships, MembershipResponse.class))
                    .orElseGet(Collections::emptyList);
        } catch (DataAccessException e) {
            LogUtils.defaultLoggerError(LOGGER, e);
            throw MembershipExceptionMapper.map(e);
        }
    }

    private MembershipSearch buildMembershipSearch(MembershipSearchRequest searchRequest) {
        MembershipSearch.Builder membershipSearchBuilder = new MembershipSearch.Builder();
        if (nonNull(searchRequest.getMemberAccountSearchRequest())) {
            membershipSearchBuilder.memberAccountSearch(buildMemberAccountSearch(searchRequest.getMemberAccountSearchRequest(), true));
        }
        return membershipSearchBuilder
                .website(searchRequest.getWebsite())
                .status(searchRequest.getStatus())
                .membershipType(searchRequest.getMembershipType())
                .autoRenewal(searchRequest.getAutoRenewal())
                .currencyCode(searchRequest.getCurrencyCode())
                .fromExpirationDate(searchRequest.getFromExpirationDate())
                .toExpirationDate(searchRequest.getToExpirationDate())
                .fromActivationDate(searchRequest.getFromActivationDate())
                .toActivationDate(searchRequest.getToActivationDate())
                .fromCreationDate(searchRequest.getFromCreationDate())
                .toCreationDate(searchRequest.getToCreationDate())
                .minBalance(searchRequest.getMinBalance())
                .maxBalance(searchRequest.getMaxBalance())
                .monthsDuration(searchRequest.getMonthsDuration())
                .productStatus(searchRequest.getProductStatus())
                .sourceType(searchRequest.getSourceType())
                .totalPrice(searchRequest.getTotalPrice())
                .renewalPrice(searchRequest.getRenewalPrice())
                .memberAccountId(searchRequest.getMemberAccountId())
                .withStatusActions(searchRequest.isWithStatusActions())
                .withMemberAccount(searchRequest.isWithMemberAccount())
                .build();
    }

    private MemberAccountSearch buildMemberAccountSearch(MemberAccountSearchRequest searchRequest, boolean forceWithoutMembership) {
        return new MemberAccountSearch.Builder()
                .name(searchRequest.getFirstName())
                .lastNames(searchRequest.getLastName())
                .userId(searchRequest.getUserId())
                .withMembership(!forceWithoutMembership && searchRequest.isWithMemberships())
                .build();
    }

    private MemberAccountService getMemberAccountService() {
        return Optional.ofNullable(memberAccountService).orElseGet(() -> {
            memberAccountService = ConfigurationEngine.getInstance(MemberAccountService.class);
            return memberAccountService;
        });
    }

    private SearchService getSearchService() {
        return Optional.ofNullable(searchService).orElseGet(() -> {
            searchService = ConfigurationEngine.getInstance(SearchService.class);
            return searchService;
        });
    }

    private MemberService getMembershipService() {
        return Optional.ofNullable(membershipService).orElseGet(() -> {
            membershipService = ConfigurationEngine.getInstance(MemberService.class);
            return membershipService;
        });
    }
}

