package com.odigeo.membership.member.memberapi.v1;

import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.RetryBookingTracking;
import com.odigeo.membership.exception.InvalidParametersException;
import com.odigeo.membership.exception.RetryableException;
import com.odigeo.membership.member.memberapi.v1.util.MembershipExceptionMapper;
import com.odigeo.membership.tracking.BookingTrackingService;

import java.util.Objects;

import static com.odigeo.membership.exception.InvalidParametersException.InvalidParametersReason.BAD_REQUEST;

public class BookingTrackingController implements RetryBookingTracking {

    @Override
    public Boolean retryBookingTracking(Long bookingId) throws InvalidParametersException {
        if (Objects.isNull(bookingId)) {
            throw new InvalidParametersException(BAD_REQUEST, "bookingId cannot be NULL");
        }
        try {
            getBookingTrackingService().processBooking(bookingId);
        } catch (MissingElementException | RetryableException e) {
            throw MembershipExceptionMapper.map(e);
        }
        return Boolean.TRUE;
    }

    private BookingTrackingService getBookingTrackingService() {
        return ConfigurationEngine.getInstance(BookingTrackingService.class);
    }
}
