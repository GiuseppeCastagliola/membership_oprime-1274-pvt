package com.odigeo.membership.member.monitoring;

import com.codahale.metrics.health.HealthCheck;
import com.odigeo.messaging.utils.BasicMessage;
import com.odigeo.messaging.utils.Consumer;
import com.odigeo.messaging.utils.kafka.KafkaJsonConsumer;
import kafka.admin.AdminUtils;
import kafka.utils.ZKStringSerializer$;
import kafka.utils.ZkUtils;
import org.I0Itec.zkclient.ZkClient;

import java.util.List;

public class KafkaConsumerHealthCheck extends HealthCheck {

    private static final Integer ZK_CONNECTION_TIMEOUT_MS = 25000;
    private static final Integer ZK_SESSION_TIMEOUT_MS = 60000;
    private final String topicName;
    private final String url;
    private final String consumerGroup;

    public KafkaConsumerHealthCheck(String url, String topicName, String consumerGroup) {
        this.topicName = topicName;
        this.url = url;
        this.consumerGroup = consumerGroup;
    }

    @Override
    protected Result check() {
        ZkClient zkClient = new ZkClient(url, ZK_SESSION_TIMEOUT_MS, ZK_CONNECTION_TIMEOUT_MS, ZKStringSerializer$.MODULE$);
        if (!AdminUtils.topicExists(ZkUtils.apply(zkClient, false), topicName)) {
            zkClient.close();
            return Result.unhealthy("Kafka topic \"" + topicName + "\" does not exist");
        }
        List<String> brokerIds = zkClient.getChildren("/brokers/ids");
        int brokersAvailable = brokerIds.size();
        if (brokersAvailable == 0) {
            zkClient.close();
            return Result.unhealthy("No Kafka brokers available!");
        }
        zkClient.close();
        return getConsumerAndConnect(brokersAvailable);
    }

    Result getConsumerAndConnect(final int brokersAvailable) {
        Consumer<BasicMessage> consumer = new KafkaJsonConsumer<>(BasicMessage.class, url, consumerGroup, topicName);
        consumer.connectAndIterate(1);
        consumer.close();
        return getResult(brokersAvailable);
    }

    Result getResult(final int brokersAvailable) {
        StringBuilder builder = new StringBuilder(64)
                .append(topicName)
                .append(" topic ok: ")
                .append(brokersAvailable)
                .append(" brokers available");
        return Result.healthy(builder.toString());
    }
}
