package com.odigeo.membership.member.memberapi.v1;

import com.edreams.base.DataAccessException;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.PostBookingMemberService;
import com.odigeo.membership.PostBookingPageInfo;
import com.odigeo.membership.exception.ActivatedMembershipException;
import com.odigeo.membership.exception.InvalidParametersException;
import com.odigeo.membership.exception.MembershipBadRequestException;
import com.odigeo.membership.exception.MembershipInternalServerErrorException;
import com.odigeo.membership.exception.MembershipServiceException;
import com.odigeo.membership.member.MembershipPostBookingService;
import com.odigeo.membership.member.memberapi.v1.util.MembershipExceptionMapper;
import com.odigeo.membership.member.memberapi.v1.util.PostBookingServiceMapping;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.request.postbooking.PostBookingPageInfoRequest;
import com.odigeo.membership.request.product.CreateMembershipSubscriptionRequest;
import com.odigeo.membership.response.PostBookingBatchResultResponse;
import com.odigeo.membership.response.PostBookingPageInfoResponse;
import ma.glasnost.orika.MapperFacade;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import javax.ejb.EJBException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

public class PostBookingMemberServiceController implements PostBookingMemberService {
    private final MapperFacade mapper;
    private static final Logger LOGGER = Logger.getLogger(PostBookingMemberServiceController.class);

    public PostBookingMemberServiceController(MapperFacade mapper) {
        this.mapper = mapper;
    }

    @Override
    public PostBookingBatchResultResponse processPostBookingCreation(MultipartFormDataInput input) {
        Map<String, List<InputPart>> formDataMap = input.getFormDataMap();
        try {
            byte[] body = formDataMap.get("file").get(0).getBody(byte[].class, null);
            boolean isInternalEmployees = Boolean
                    .parseBoolean(formDataMap.get("internalEmployees").get(0).getBodyAsString());
            String channel = isInternalEmployees ? StringUtils.EMPTY : formDataMap.get("channel").get(0).getBodyAsString();
            String csv = new String(body, StandardCharsets.UTF_8.name());
            List<String> errors = getMembershipPostBookingService().parseInfoAndCreateAccounts(csv, channel, isInternalEmployees);
            return new PostBookingBatchResultResponse(errors);
        } catch (IOException e) {
            throw new MembershipInternalServerErrorException("Error retrieving file: " + e.getMessage(), e);
        }
    }

    @Override
    public Long createPostBookingMembership(CreateMembershipSubscriptionRequest createMembershipRequest) {
        MembershipCreation membershipCreation = PostBookingServiceMapping.getMembershipCreationFromPostBookingRequest(createMembershipRequest);
        try {
            return getMembershipPostBookingService().createPostBookingMembership(membershipCreation, createMembershipRequest.getEmail());
        } catch (DataAccessException | ActivatedMembershipException e) {
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    public Long createPostBookingPendingMembership(CreateMembershipSubscriptionRequest createMembershipSubscriptionRequest) throws InvalidParametersException {
        MembershipCreation membershipCreation = PostBookingServiceMapping.getMembershipPendingCreationFromPostBookingRequest(createMembershipSubscriptionRequest);
        try {
            return getMembershipPostBookingService().createPostBookingMembershipPending(membershipCreation);
        } catch (DataAccessException | ActivatedMembershipException e) {
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    @SuppressWarnings("PMD.PreserveStackTrace")
    public PostBookingPageInfoResponse getPostBookingPageInfo(PostBookingPageInfoRequest postBookingPageInfoRequest) throws MembershipServiceException {
        try {
            PostBookingPageInfo postBookingPageInfo = getMembershipPostBookingService().getPostBookingPageInfo(postBookingPageInfoRequest.getToken());
            return mapper.map(postBookingPageInfo, PostBookingPageInfoResponse.class);
        } catch (UnsupportedEncodingException | IllegalArgumentException | EJBException e) {
            LOGGER.error("Failed to decrypt token: " + e.getMessage());
            throw new MembershipBadRequestException(PostBookingPageInfo.EXCEPTION_MESSAGE);
        }
    }

    private MembershipPostBookingService getMembershipPostBookingService() {
        return ConfigurationEngine.getInstance(MembershipPostBookingService.class);
    }
}
