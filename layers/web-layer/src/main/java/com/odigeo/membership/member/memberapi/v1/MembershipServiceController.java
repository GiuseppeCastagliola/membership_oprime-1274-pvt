package com.odigeo.membership.member.memberapi.v1;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.BookingTracking;
import com.odigeo.membership.MembershipService;
import com.odigeo.membership.UpdateMemberAccount;
import com.odigeo.membership.UpdateMembership;
import com.odigeo.membership.exception.ExistingRecurringException;
import com.odigeo.membership.exception.InvalidParametersException;
import com.odigeo.membership.exception.MembershipServiceException;
import com.odigeo.membership.exception.bookingapi.BookingApiException;
import com.odigeo.membership.mapper.GeneralMapperCreator;
import com.odigeo.membership.member.MemberAccountService;
import com.odigeo.membership.member.MemberService;
import com.odigeo.membership.member.MembershipValidationService;
import com.odigeo.membership.member.UpdateMembershipService;
import com.odigeo.membership.member.memberapi.converter.CreateMembershipConverter;
import com.odigeo.membership.member.memberapi.util.LogUtils;
import com.odigeo.membership.member.memberapi.v1.util.MemberServiceMapping;
import com.odigeo.membership.member.memberapi.v1.util.MembershipCreationRequestMapper;
import com.odigeo.membership.member.memberapi.v1.util.MembershipExceptionMapper;
import com.odigeo.membership.parameters.MemberOnPassengerListParameter;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.product.BlacklistPaymentService;
import com.odigeo.membership.request.CheckMemberOnPassengerListRequest;
import com.odigeo.membership.request.product.ActivationRequest;
import com.odigeo.membership.request.product.AddToBlackListRequest;
import com.odigeo.membership.request.product.FreeTrialCandidateRequest;
import com.odigeo.membership.request.product.UpdateMemberAccountRequest;
import com.odigeo.membership.request.product.UpdateMembershipRequest;
import com.odigeo.membership.request.product.creation.CreateMembershipRequest;
import com.odigeo.membership.request.tracking.MembershipBookingTrackingRequest;
import com.odigeo.membership.response.BlackListedPaymentMethod;
import com.odigeo.membership.response.Membership;
import com.odigeo.membership.response.MembershipRenewal;
import com.odigeo.membership.response.MembershipStatus;
import com.odigeo.membership.response.WebsiteMembership;
import com.odigeo.membership.response.search.MembershipResponse;
import com.odigeo.membership.response.tracking.MembershipBookingTracking;
import com.odigeo.membership.search.SearchService;
import com.odigeo.membership.tracking.BookingTrackingService;
import com.odigeo.messaging.utils.MessageDataAccessException;
import com.odigeo.tracking.client.track.method.Track;
import ma.glasnost.orika.MapperFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static java.util.Objects.isNull;

public class MembershipServiceController implements MembershipService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MembershipServiceController.class);
    private static final String WITH_MEMBERSHIP_ID = "- membershipId: ";
    private static final String WITH_USER_ID = "- userId: ";
    private final MapperFacade mapperFacade;
    private MemberServiceMapping membershipServiceMapper;

    @SuppressWarnings("unused")
    public MembershipServiceController() {
        this.mapperFacade = new GeneralMapperCreator().getMapper();
    }

    MembershipServiceController(MapperFacade mapperFacade) {
        this.mapperFacade = mapperFacade;
    }

    @Override
    @Track(name = "getMembership")
    public Membership getMembership(Long userId, String website) {
        try {
            return Optional.ofNullable(getMemberAccountService().getMembershipAccount(userId, website))
                    .map(getMembershipServiceMapper()::asMembershipResponse)
                    .orElse(null);
        } catch (DataAccessException e) {
            LogUtils.defaultLoggerError(LOGGER, e, WITH_USER_ID + userId + " website: " + website);
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    public MembershipResponse getCurrentMembership(Long userId, String website) {
        return Optional.ofNullable(ConfigurationEngine.getInstance(SearchService.class))
                .flatMap(searchService -> {
                    try {
                        return searchService.getCurrentMembership(userId, website);
                    } catch (DataAccessException e) {
                        LogUtils.defaultLoggerError(LOGGER, e);
                        throw MembershipExceptionMapper.map(e);
                    }
                })
                .map(currentMembership -> mapperFacade.map(currentMembership, MembershipResponse.class))
                .orElse(null);
    }

    @Override
    public MembershipResponse activateMembership(long membershipId, ActivationRequest activateRequest) {
        try {
            getMemberService().activateMembership(membershipId, activateRequest.getBookingId(), activateRequest.getBalance());
            return Optional.of(getMemberService().getMembershipById(membershipId))
                    .map(membership -> mapperFacade.map(membership, MembershipResponse.class))
                    .orElse(null);
        } catch (DataAccessException | MessageDataAccessException | MissingElementException e) {
            LogUtils.defaultLoggerError(LOGGER, e);
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    public Boolean isEligibleForFreeTrial(FreeTrialCandidateRequest freeTrialCandidateRequest) throws MembershipServiceException {
        try {
            return getMembershipValidationService().isEligibleForFreeTrial(freeTrialCandidateRequest);
        } catch (DataAccessException e) {
            LogUtils.defaultLoggerError(LOGGER, e);
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    @Track(name = "getAllMembership")
    public WebsiteMembership getAllMembership(Long userId) {
        try {
            WebsiteMembership websiteMembershipMap = new WebsiteMembership();
            websiteMembershipMap.setWebsiteMembershipMap(
                    Optional.of(getMemberAccountService().getActiveMembersByUserId(userId))
                            .map(getMembershipServiceMapper()::asWebsiteMemberMap)
                            .orElse(Collections.emptyMap()));
            return websiteMembershipMap;
        } catch (DataAccessException e) {
            LogUtils.defaultLoggerError(LOGGER, e, WITH_USER_ID + userId);
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    @Track(name = "applyMembership")
    public Boolean applyMembership(CheckMemberOnPassengerListRequest checkMemberOnPassengerListRequest) {
        Boolean result = Boolean.FALSE;
        MemberOnPassengerListParameter memberOnPassengerListParameter = getMembershipServiceMapper()
                .asPassengerList(checkMemberOnPassengerListRequest);
        try {
            result = getMembershipValidationService().applyMembership(memberOnPassengerListParameter);
        } catch (MissingElementException e) {
            LogUtils.smallLog(LOGGER, "MissingElementException checking member on PassengerList:", e);
        } catch (DataAccessException e) {
            LogUtils.defaultLoggerError(LOGGER, e, WITH_USER_ID + checkMemberOnPassengerListRequest.getUserId());
            throw MembershipExceptionMapper.map(e);
        }
        return result;
    }

    @Override
    public MembershipStatus disableMembership(Long membershipId) {
        try {
            UpdateMembership updateMembership = new UpdateMembership().setMembershipId(String.valueOf(membershipId));
            return new MembershipStatus(getUpdateMembershipService().deactivateMembership(updateMembership));
        } catch (MissingElementException | DataAccessException | BookingApiException e) {
            LogUtils.defaultLoggerError(LOGGER, e, WITH_MEMBERSHIP_ID + membershipId);
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    public MembershipRenewal disableAutoRenewal(Long membershipId) {
        try {
            UpdateMembership updateMembership = new UpdateMembership().setMembershipId(String.valueOf(membershipId));
            return new MembershipRenewal(getUpdateMembershipService().disableAutoRenewal(updateMembership));
        } catch (MissingElementException | DataAccessException e) {
            LogUtils.defaultLoggerError(LOGGER, e, WITH_MEMBERSHIP_ID + membershipId);
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    public MembershipRenewal enableAutoRenewal(Long membershipId) {
        try {
            return new MembershipRenewal(getUpdateMembershipService().enableAutoRenewal(new UpdateMembership()
                    .setMembershipId(String.valueOf(membershipId))));
        } catch (MissingElementException | DataAccessException e) {
            LogUtils.defaultLoggerError(LOGGER, e, WITH_MEMBERSHIP_ID + membershipId);
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    public MembershipStatus reactivateMembership(Long membershipId) {
        try {
            UpdateMembership updateMembership = new UpdateMembership().setMembershipId(String.valueOf(membershipId));
            String result = getUpdateMembershipService().reactivateMembership(updateMembership);
            return new MembershipStatus(result);
        } catch (MissingElementException | DataAccessException | BookingApiException e) {
            LogUtils.defaultLoggerError(LOGGER, e, WITH_MEMBERSHIP_ID + membershipId);
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    public Boolean isMembershipPerksActiveOn(String siteId) {
        return getMembershipValidationService().isMembershipActiveOnWebsite(siteId);
    }

    @Override
    @Track(name = "createMembership", converter = CreateMembershipConverter.class)
    public Long createMembership(CreateMembershipRequest createMembershipRequest) {
        try {
            MembershipCreation membershipCreation = MembershipCreationRequestMapper
                    .getMembershipCreationFromRequestFields(createMembershipRequest.getAsMap());
            return getMemberService().createMembership(membershipCreation);
        } catch (DataAccessException | MissingElementException e) {
            LogUtils.defaultLoggerError(LOGGER, e);
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    public Boolean updateMemberAccount(UpdateMemberAccountRequest updateMemberAccountRequest) {
        try {
            return getMemberAccountService().updateMemberAccount(mapperFacade.map(updateMemberAccountRequest, UpdateMemberAccount.class));
        } catch (DataAccessException | MissingElementException e) {
            LogUtils.defaultLoggerError(LOGGER, e);
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    public Boolean updateMembership(UpdateMembershipRequest updateMembershipRequest) {
        try {
            return getUpdateMembershipService().updateMembership(mapperFacade.map(updateMembershipRequest, UpdateMembership.class));
        } catch (DataAccessException | MissingElementException | BookingApiException | ExistingRecurringException e) {
            LogUtils.defaultLoggerError(LOGGER, e);
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    public Boolean retryMembershipActivation(Long bookingId) throws InvalidParametersException {
        throw new UnsupportedOperationException("Activation logic migrated to membership-robots, to retry an activation by booking id,"
                + " trigger a void update of that booking or check for utilities in membership-robots module");
    }

    @Override
    public List<BlackListedPaymentMethod> getBlacklistedPaymentMethods(Long membershipId) {
        try {
            return mapperFacade.mapAsList(getBlacklistPaymentService().getBlacklistedPaymentMethods(membershipId).toArray(), BlackListedPaymentMethod.class);
        } catch (DataAccessException e) {
            LogUtils.defaultLoggerError(LOGGER, e, WITH_MEMBERSHIP_ID + membershipId);
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    public Boolean addToBlackList(Long membershipId, AddToBlackListRequest addToBlackListRequest) {
        try {
            return getBlacklistPaymentService().addToBlackList(getMembershipServiceMapper()
                    .mapToBlackListedPaymentMethods(membershipId, addToBlackListRequest.getBlackListedPaymentMethods()));
        } catch (DataAccessException e) {
            LogUtils.defaultLoggerError(LOGGER, e, WITH_MEMBERSHIP_ID + membershipId);
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    public String getNewVatModelDate() {
        return getMembershipValidationService().getNewVatModelDate();
    }

    @Override
    public Boolean isMembershipToBeRenewed(Long membershipId) {
        try {
            return getMembershipValidationService().isMembershipToBeRenewed(membershipId);
        } catch (MissingElementException | DataAccessException e) {
            LogUtils.defaultLoggerError(LOGGER, e, WITH_MEMBERSHIP_ID + membershipId);
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    public MembershipBookingTracking getBookingTracking(long bookingId) throws MembershipServiceException {
        try {
            return mapperFacade.map(getBookingTrackingService()
                    .getMembershipBookingTracking(bookingId), MembershipBookingTracking.class);
        } catch (DataAccessException e) {
            LogUtils.defaultLoggerError(LOGGER, e, "- booking id " + bookingId);
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    public MembershipBookingTracking addBookingTrackingUpdateBalance(MembershipBookingTrackingRequest membershipBookingTrackingRequest) throws MembershipServiceException {
        try {
            return mapperFacade.map(getBookingTrackingService()
                    .insertMembershipBookingTracking(mapperFacade.map(membershipBookingTrackingRequest, BookingTracking.class)), MembershipBookingTracking.class);
        } catch (DataAccessException e) {
            LogUtils.defaultLoggerError(LOGGER, e, "- booking id " + membershipBookingTrackingRequest.getBookingId()
                    + " membershipId " + membershipBookingTrackingRequest.getMembershipId());
            throw MembershipExceptionMapper.map(e);
        }
    }

    @Override
    public void deleteBookingTrackingUpdateBalance(MembershipBookingTrackingRequest membershipBookingTrackingRequest) throws MembershipServiceException {
        try {
            getBookingTrackingService().deleteMembershipBookingTracking(mapperFacade.map(membershipBookingTrackingRequest, BookingTracking.class));
        } catch (DataAccessException e) {
            LogUtils.defaultLoggerError(LOGGER, e, "- booking id " + membershipBookingTrackingRequest.getBookingId()
                    + " membershipId " + membershipBookingTrackingRequest.getMembershipId());
            throw MembershipExceptionMapper.map(e);
        }
    }

    private MemberServiceMapping getMembershipServiceMapper() {
        if (isNull(membershipServiceMapper)) {
            membershipServiceMapper = ConfigurationEngine.getInstance(MemberServiceMapping.class);
        }
        return membershipServiceMapper;
    }

    private MemberService getMemberService() {
        return ConfigurationEngine.getInstance(MemberService.class);
    }

    private UpdateMembershipService getUpdateMembershipService() {
        return ConfigurationEngine.getInstance(UpdateMembershipService.class);
    }

    private MembershipValidationService getMembershipValidationService() {
        return ConfigurationEngine.getInstance(MembershipValidationService.class);
    }

    private MemberAccountService getMemberAccountService() {
        return ConfigurationEngine.getInstance(MemberAccountService.class);
    }

    private BlacklistPaymentService getBlacklistPaymentService() {
        return ConfigurationEngine.getInstance(BlacklistPaymentService.class);
    }

    private BookingTrackingService getBookingTrackingService() {
        return ConfigurationEngine.getInstance(BookingTrackingService.class);
    }
}
