package com.odigeo.membership.member.rest;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.commons.monitoring.metrics.MetricsBuilder;
import com.odigeo.commons.monitoring.metrics.MetricsNames;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.membership.AutoRenewalOperation;
import com.odigeo.membership.AutorenewalTracking;
import com.odigeo.membership.UpdateMembershipAction;
import com.odigeo.membership.request.product.UpdateMembershipRequest;
import com.odigeo.membership.tracking.AutorenewalTrackingService;
import org.apache.commons.io.IOUtils;
import org.jboss.resteasy.annotations.interception.ServerInterceptor;
import org.jboss.resteasy.core.ResourceMethod;
import org.jboss.resteasy.core.ServerResponse;
import org.jboss.resteasy.spi.HttpRequest;
import org.jboss.resteasy.spi.interception.AcceptedByMethod;
import org.jboss.resteasy.spi.interception.PreProcessInterceptor;

import javax.ws.rs.ext.Provider;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Optional;
import java.util.stream.Stream;

@Provider
@ServerInterceptor
public class MembershipAutorenewalInterceptor implements PreProcessInterceptor, AcceptedByMethod {

    private static final String UPDATE_MEMBERSHIP_PATH = "updateMembership";
    private static final String ENABLE_AUTO_RENEWAL_PATH = "enable-auto-renewal";
    private static final String DISABLE_AUTO_RENEWAL_PATH = "disable-auto-renewal";
    private static final String DISABLE_MEMBERSHIP_PATH = "disable";
    private static final String REACTIVATE_MEMBERSHIP_PATH = "reactivate";
    private static final String ACTIVATE_MEMBERSHIP_PATH = "activate";
    private static final String SUB_PATH = "/";
    private static final int AUTORENEWAL_REQUEST_ID_INDEX = 1;
    private static final int AUTORENEWAL_REQUEST_METHOD_INDEX = 2;
    private static final int UPDATE_MEMBERSHIP_REQUEST_METHOD_INDEX = 1;

    @Override
    public boolean accept(Class aClass, Method method) {
        return aClass.getSimpleName().equals("MembershipService")
                && getInterceptorHelper().isRequestOf(method, UPDATE_MEMBERSHIP_PATH, ENABLE_AUTO_RENEWAL_PATH,
                DISABLE_AUTO_RENEWAL_PATH, DISABLE_MEMBERSHIP_PATH + SUB_PATH, REACTIVATE_MEMBERSHIP_PATH + SUB_PATH,
                ACTIVATE_MEMBERSHIP_PATH + SUB_PATH);
    }

    @Override
    public ServerResponse preProcess(HttpRequest request, ResourceMethod resourceMethod) {
        extractFields(request).ifPresent(trackingFields -> {
            MetricsUtils.incrementCounter(MetricsBuilder.buildAutorenewalBySource(trackingFields), MetricsNames.METRICS_REGISTRY_NAME);
            getAutorenewalTrackingService().trackAutorenewalOperation(trackingFields);
        });
        return null;
    }

    private Optional<AutorenewalTracking> extractFields(HttpRequest request) {
        Optional<AutorenewalTracking> autorenewalTrackingBuilder = Optional.empty();
        String path = request.getUri().getPath();
        if (path.contains(UPDATE_MEMBERSHIP_PATH)) {
            autorenewalTrackingBuilder = extractInfoFromUpdateMembershipRequest(request);
        } else if (Stream.of(ENABLE_AUTO_RENEWAL_PATH, DISABLE_AUTO_RENEWAL_PATH, ACTIVATE_MEMBERSHIP_PATH, REACTIVATE_MEMBERSHIP_PATH, DISABLE_MEMBERSHIP_PATH).anyMatch(path::contains)) {
            autorenewalTrackingBuilder = extractInfoFromAutorenewalRequest(request);
        }
        return autorenewalTrackingBuilder;
    }

    private Optional<AutorenewalTracking> extractInfoFromAutorenewalRequest(HttpRequest request) {
        final AutorenewalTracking fields = new AutorenewalTracking();
        return getInterceptorHelper().extractSegmentFromPath(request, AUTORENEWAL_REQUEST_ID_INDEX)
                .flatMap(membershipId -> {
                    fields.setMembershipId(membershipId);
                    return getInterceptorHelper().extractSegmentFromPath(request, AUTORENEWAL_REQUEST_METHOD_INDEX);
                }).flatMap(method -> {
                    fields.setRequestedMethod(method);
                    return toValidOperation(method);
                }).map(operation -> {
                    fields.setAutoRenewalOperation(operation);
                    return getInterceptorHelper().extractClientModuleFromHeader(request);
                }).map(requester -> {
                    fields.setRequester(requester);
                    return fields;
                });
    }

    private Optional<AutorenewalTracking> extractInfoFromUpdateMembershipRequest(HttpRequest request) {
        Optional<AutorenewalTracking> collectedFields = Optional.empty();
        byte[] content = new byte[0];
        try {
            content = IOUtils.toByteArray(request.getInputStream());
            String body = getInterceptorHelper().buildOneLineString(content);

            final AutorenewalTracking fields = new AutorenewalTracking();
            collectedFields = getInterceptorHelper().extractFieldFromBody(body, UpdateMembershipRequest::getMembershipId)
                    .flatMap(membershipId -> {
                        fields.setMembershipId(membershipId);
                        return getInterceptorHelper().extractSegmentFromPath(request, UPDATE_MEMBERSHIP_REQUEST_METHOD_INDEX);
                    })
                    .flatMap(method -> {
                        fields.setRequestedMethod(method);
                        return getInterceptorHelper().extractFieldFromBody(body, UpdateMembershipRequest::getOperation).flatMap(this::toValidOperation);
                    })
                    .map(operation -> {
                        fields.setAutoRenewalOperation(operation);
                        return getInterceptorHelper().extractClientModuleFromHeader(request);
                    }).map(requester -> {
                        fields.setRequester(requester);
                        return fields;
                    });

        } catch (IOException | NumberFormatException e) {
            return collectedFields;
        } finally {
            request.setInputStream(new ByteArrayInputStream(content));
        }
        return collectedFields;
    }

    private Optional<AutoRenewalOperation> toValidOperation(String value) {
        Optional<AutoRenewalOperation> operation = Optional.empty();
        if (value.contains(UpdateMembershipAction.ENABLE_AUTO_RENEWAL.name())
                || value.contains(ENABLE_AUTO_RENEWAL_PATH)
                || value.contains(ACTIVATE_MEMBERSHIP_PATH)
                || value.contains(REACTIVATE_MEMBERSHIP_PATH)) {
            operation = Optional.of(AutoRenewalOperation.ENABLE_AUTO_RENEW);
        } else if (value.contains(UpdateMembershipAction.DISABLE_AUTO_RENEWAL.name())
                || value.contains(DISABLE_AUTO_RENEWAL_PATH)
                || value.contains(DISABLE_MEMBERSHIP_PATH)) {
            operation = Optional.of(AutoRenewalOperation.DISABLE_AUTO_RENEW);
        }
        return operation;
    }

    InterceptorHelper getInterceptorHelper() {
        return ConfigurationEngine.getInstance(InterceptorHelper.class);
    }

    AutorenewalTrackingService getAutorenewalTrackingService() {
        return ConfigurationEngine.getInstance(AutorenewalTrackingService.class);
    }
}
