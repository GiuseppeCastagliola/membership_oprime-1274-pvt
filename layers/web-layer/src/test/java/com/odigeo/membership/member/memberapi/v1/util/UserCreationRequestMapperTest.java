package com.odigeo.membership.member.memberapi.v1.util;

import com.odigeo.membership.exception.MembershipBadRequestException;
import com.odigeo.membership.parameters.UserCreation;
import com.odigeo.membership.request.product.creation.CreateNewMembershipRequest;
import com.odigeo.membership.request.product.creation.UserCreationInfo;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class UserCreationRequestMapperTest {

    private static final String LOCALE = "es_ES";
    private static final String EMAIL = "name.surname@mail.com";
    private static final Integer TRAFFIC_INTERFACE_ID = 1;
    private static final String WEBSITE = "ES";

    @Test
    public void testGetUserCreationWithUserCreationInfo() {
        CreateNewMembershipRequest request = new CreateNewMembershipRequest.Builder()
                .withWebsite(WEBSITE)
                .withUserCreationInfo(getValidUserCreationInfo())
                .build();
        UserCreation userCreation = UserCreationRequestMapper.getMandatoryUserCreation(request.getAsMap());
        assertEquals(userCreation.getWebsite(), WEBSITE);
        assertEquals(userCreation.getLocale(), LOCALE);
        assertEquals(userCreation.getEmail(), EMAIL);
        assertEquals(userCreation.getTrafficInterfaceId(), TRAFFIC_INTERFACE_ID);
    }

    @Test(expectedExceptions = {MembershipBadRequestException.class}, expectedExceptionsMessageRegExp = ".*mandatory.*")
    public void testGetUserCreationWithoutMandatoryField() {
        UserCreationInfo userCreationInfo = getValidUserCreationInfo();
        userCreationInfo.setEmail(null);
        CreateNewMembershipRequest request = new CreateNewMembershipRequest.Builder()
                .withWebsite(WEBSITE)
                .withUserCreationInfo(userCreationInfo)
                .build();
        UserCreationRequestMapper.getMandatoryUserCreation(request.getAsMap());
    }

    @Test(expectedExceptions = {MembershipBadRequestException.class}, expectedExceptionsMessageRegExp = ".*User creation details not provided.*")
    public void testGetUserCreationWithoutUserCreationInfo() {
        CreateNewMembershipRequest request = new CreateNewMembershipRequest.Builder().build();
        UserCreationRequestMapper.getMandatoryUserCreation(request.getAsMap());
    }

    private UserCreationInfo getValidUserCreationInfo() {
        return new UserCreationInfo.Builder()
                .withLocale(LOCALE)
                .withEmail(EMAIL)
                .withTrafficInterfaceId(TRAFFIC_INTERFACE_ID)
                .build();
    }
}
