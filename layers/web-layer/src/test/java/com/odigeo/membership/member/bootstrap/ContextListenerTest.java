package com.odigeo.membership.member.bootstrap;

import com.codahale.metrics.health.HealthCheckRegistry;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.AbstractModule;
import com.google.inject.Module;
import com.odigeo.commons.rest.monitoring.healthcheck.AutoHealthCheckRegister;
import com.odigeo.membership.robots.activator.BookingUpdatesConsumerConfiguration;
import com.odigeo.robots.RobotsBootstrap;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.fail;

public class ContextListenerTest {

    private static final int EXPECTED_MODULES_NUMBER = 12;

    @Mock
    RobotsBootstrap bootstrap;

    private ContextListener contextListener;
    private ServletContextEvent servletContextEventMock;
    private ServletContext servletContextMock;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        ConfigurationEngine.init(new AbstractModule() {
            @Override
            protected void configure() {
                bind(RobotsBootstrap.class).toInstance(bootstrap);
            }
        });
        setUpMocks();
    }

    private void setUpMocks() {
        this.contextListener = new ContextListener();
        this.servletContextEventMock = mock(ServletContextEvent.class);
        this.servletContextMock = mock(ServletContext.class);
        when(servletContextEventMock.getServletContext()).thenReturn(servletContextMock);
    }

    @Test
    public void testContextDestroyed() {
        try {
            this.contextListener.contextDestroyed(servletContextEventMock);
        } catch (final Exception e) {
            fail();
        }
    }

    @Test
    public void testRegisterIfValidPropertiesWithStartRobotsValue() {
        testRegisterWithCustomProperties(ContextListener.START_ROBOTS_VALUE);
    }

    @Test
    public void testRegisterIfValidPropertiesWithRegisterRobotsValue() {
        testRegisterWithCustomProperties(ContextListener.REGISTER_ROBOTS_VALUE);
    }

    private void testRegisterWithCustomProperties(final String propertyValue) {
        initSupportSystems();
        try {
            System.setProperty(ContextListener.INITIALIZE_ROBOTS_SYSTEM_VAR, propertyValue);
            contextListener.registerIfValidProperties(mock(HealthCheckRegistry.class),
                    mock(BookingUpdatesConsumerConfiguration.class));
        } catch (final Exception e) {
            fail();
        }
    }

    @Test
    public void testRegisterWithNoProperties() {
        initSupportSystems();
        contextListener.registerIfValidProperties(mock(HealthCheckRegistry.class),
                mock(BookingUpdatesConsumerConfiguration.class));
    }

    private void initSupportSystems() {
        contextListener.initConfigurationEngine(servletContextMock, mock(Module.class));
        contextListener.initLog4J(servletContextMock);
        contextListener.includeConfigurationsInDump();
    }

    @Test
    public void testInitializeRobotsFrameworksAndStartSchedulerIfApplicableThrowsNCDFEIfNoContextGiven() {
        contextListener.initializeRobotsFrameworksAndStartSchedulerIfApplicable(mock(ServletContext.class));
    }

    @Test
    public void testAssembleModulesList() {
        final List<Module> receivedList = contextListener.assembleModulesList(mock(AutoHealthCheckRegister.class));
        assertEquals(receivedList.size(), EXPECTED_MODULES_NUMBER);
    }
}
