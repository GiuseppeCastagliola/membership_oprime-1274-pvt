package com.odigeo.membership.member.memberapi.v1.util;

import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.exception.MembershipBadRequestException;
import com.odigeo.membership.exception.MembershipServiceException;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.request.product.CreateMembershipSubscriptionRequest;
import org.apache.commons.lang.StringUtils;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

import static com.odigeo.membership.member.PostBookingParser.DEFAULT_MONTHS_TO_RENEWAL;
import static org.testng.Assert.assertEquals;

public class PostBookingServiceMappingTest {

    private static final String MONTHS_TO_RENEWAL = "6";

    @Test(expectedExceptions = MembershipServiceException.class,
        expectedExceptionsMessageRegExp = "Number of months to renewal with non numerical format")
    public void testGetPostBookingCreationFromRequestWrongMonthsFormatKO() {
        CreateMembershipSubscriptionRequest createPostBookingRequest = getCreatePostBookingRequest();
        createPostBookingRequest.setMonthsToRenewal("3.");
        PostBookingServiceMapping.getMembershipCreationFromPostBookingRequest(createPostBookingRequest);
    }

    @Test
    public void testGetPostBookingCreationFromRequestEmptyMonthsOneYearOK() {
        CreateMembershipSubscriptionRequest createPostBookingRequest = getCreatePostBookingRequest();
        createPostBookingRequest.setMonthsToRenewal(StringUtils.EMPTY);
        MembershipCreation membershipCreationFromRequest = PostBookingServiceMapping
                .getMembershipCreationFromPostBookingRequest(createPostBookingRequest);
        assertEquals(membershipCreationFromRequest.getExpirationDate(), LocalDate.now().plusMonths(DEFAULT_MONTHS_TO_RENEWAL));
        assertEquals(MemberStatus.ACTIVATED, membershipCreationFromRequest.getMemberStatus());
        assertEquals(StatusAction.CREATION, membershipCreationFromRequest.getStatusAction());
        assertEquals(MembershipType.BASIC, membershipCreationFromRequest.getMembershipType());
    }

    @Test(expectedExceptions = MembershipServiceException.class,
        expectedExceptionsMessageRegExp = "Maximum number of months to renewal exceeded")
    public void testGetPostBookingCreationFromRequestNotAllowedMonthsKO() {
        CreateMembershipSubscriptionRequest createPostBookingRequest = getCreatePostBookingRequest();
        createPostBookingRequest.setMonthsToRenewal("18");
        PostBookingServiceMapping.getMembershipCreationFromPostBookingRequest(createPostBookingRequest);
    }

    @Test(expectedExceptions = MembershipServiceException.class,
            expectedExceptionsMessageRegExp = "Months to renewal cannot be 0 or negative")
    public void testGetPostBookingCreationFromRequestZeroMonthsKO() {
        CreateMembershipSubscriptionRequest createPostBookingRequest = getCreatePostBookingRequest();
        createPostBookingRequest.setMonthsToRenewal("0");
        PostBookingServiceMapping.getMembershipCreationFromPostBookingRequest(createPostBookingRequest);
    }

    @Test
    public void testGetPostBookingCreationFromRequestCorrectMonthsOK() {
        CreateMembershipSubscriptionRequest createPostBookingRequest = getCreatePostBookingRequest();
        createPostBookingRequest.setMonthsToRenewal(MONTHS_TO_RENEWAL);
        MembershipCreation membershipCreationFromRequest = PostBookingServiceMapping
                .getMembershipCreationFromPostBookingRequest(createPostBookingRequest);
        assertEquals(membershipCreationFromRequest.getExpirationDate(), LocalDate.now().plusMonths(6));
    }

    @Test
    public void testGetPostBookingCreationFromRequestStatusActionEmail() {
        CreateMembershipSubscriptionRequest createPostBookingRequest = getCreatePostBookingRequest();
        createPostBookingRequest.setChannel(PostBookingServiceMapping.EMAIL);
        MembershipCreation membershipCreationFromRequest = PostBookingServiceMapping
                .getMembershipCreationFromPostBookingRequest(createPostBookingRequest);
        assertEquals(membershipCreationFromRequest.getStatusAction(), StatusAction.PB_EMAIL_CREATION);
    }

    @Test
    public void testGetPostBookingCreationFromRequestStatusActionPhone() {
        CreateMembershipSubscriptionRequest createPostBookingRequest = getCreatePostBookingRequest();
        createPostBookingRequest.setChannel(PostBookingServiceMapping.PHONE);
        MembershipCreation membershipCreationFromRequest = PostBookingServiceMapping
                .getMembershipCreationFromPostBookingRequest(createPostBookingRequest);
        assertEquals(membershipCreationFromRequest.getStatusAction(), StatusAction.PB_PHONE_CREATION);
    }

    @Test
    public void testGetPostBookingCreationFromRequestStatusActionAndroid() {
        CreateMembershipSubscriptionRequest createPostBookingRequest = getCreatePostBookingRequest();
        createPostBookingRequest.setChannel(PostBookingServiceMapping.ANDROID);
        MembershipCreation membershipCreationFromRequest = PostBookingServiceMapping
                .getMembershipCreationFromPostBookingRequest(createPostBookingRequest);
        assertEquals(membershipCreationFromRequest.getStatusAction(), StatusAction.PB_ANDROID_CREATION);
    }

    @Test
    public void testGetPostBookingCreationFromRequestStatusActioniOS() {
        CreateMembershipSubscriptionRequest createPostBookingRequest = getCreatePostBookingRequest();
        createPostBookingRequest.setChannel(PostBookingServiceMapping.IOS);
        MembershipCreation membershipCreationFromRequest = PostBookingServiceMapping
                .getMembershipCreationFromPostBookingRequest(createPostBookingRequest);
        assertEquals(membershipCreationFromRequest.getStatusAction(), StatusAction.PB_IOS_CREATION);
    }

    private CreateMembershipSubscriptionRequest getCreatePostBookingRequest() {
        CreateMembershipSubscriptionRequest createPostBookingRequest = new CreateMembershipSubscriptionRequest();
        createPostBookingRequest.setUserId("333");
        createPostBookingRequest.setName("test");
        createPostBookingRequest.setLastNames("test");
        createPostBookingRequest.setWebsite("ES");
        return createPostBookingRequest;
    }

    @Test(expectedExceptions = MembershipBadRequestException.class)
    public void testGetMembershipPendingCreationFromPostBookingRequestException() {
        PostBookingServiceMapping.getMembershipPendingCreationFromPostBookingRequest(getCreatePostBookingRequest());
    }

    @Test
    public void testGetMembershipPendingCreationFromPostBookingRequest() {
        CreateMembershipSubscriptionRequest createPostBookingRequest = getCreatePostBookingRequest();
        createPostBookingRequest.setMonthsToRenewal(MONTHS_TO_RENEWAL);
        final MembershipCreation membershipCreation = PostBookingServiceMapping.getMembershipPendingCreationFromPostBookingRequest(createPostBookingRequest);
        assertEquals(membershipCreation.getMembershipType(), MembershipType.BASIC);
        assertEquals(membershipCreation.getMemberStatus(), MemberStatus.PENDING_TO_ACTIVATE);
    }

    @Test(expectedExceptions = MembershipBadRequestException.class)
    public void testValidateMembershipRequestParametersWithEmptyUserId() {
        CreateMembershipSubscriptionRequest createPostBookingRequest = getCreatePostBookingRequest();
        createPostBookingRequest.setUserId(StringUtils.EMPTY);
        PostBookingServiceMapping.validateMembershipRequestParameters(createPostBookingRequest);
    }

    @Test(expectedExceptions = MembershipBadRequestException.class)
    public void testValidateMembershipRequestParametersWithNegativeBalance() {
        CreateMembershipSubscriptionRequest createPostBookingRequest = getCreatePostBookingRequest();
        createPostBookingRequest.setBalance(BigDecimal.valueOf(-10));
        PostBookingServiceMapping.validateMembershipRequestParameters(createPostBookingRequest);
    }
}