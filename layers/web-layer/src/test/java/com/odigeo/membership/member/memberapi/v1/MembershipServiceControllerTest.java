package com.odigeo.membership.member.memberapi.v1;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.AbstractModule;
import com.odigeo.membership.BlacklistedPaymentMethod;
import com.odigeo.membership.BookingTracking;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.ProductStatus;
import com.odigeo.membership.UpdateMemberAccount;
import com.odigeo.membership.UpdateMembership;
import com.odigeo.membership.UpdateMembershipAction;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.enums.SourceType;
import com.odigeo.membership.exception.MembershipInternalServerErrorException;
import com.odigeo.membership.exception.MembershipNotFoundException;
import com.odigeo.membership.exception.MembershipServiceException;
import com.odigeo.membership.exception.bookingapi.BookingApiException;
import com.odigeo.membership.member.MemberAccountService;
import com.odigeo.membership.member.MemberService;
import com.odigeo.membership.member.MembershipValidationService;
import com.odigeo.membership.member.UpdateMembershipService;
import com.odigeo.membership.member.memberapi.v1.util.MemberServiceMapping;
import com.odigeo.membership.member.userarea.MemberUserAreaService;
import com.odigeo.membership.parameters.MemberOnPassengerListParameter;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.product.BlacklistPaymentService;
import com.odigeo.membership.request.CheckMemberOnPassengerListRequest;
import com.odigeo.membership.request.container.TravellerParametersContainer;
import com.odigeo.membership.request.product.ActivationRequest;
import com.odigeo.membership.request.product.AddToBlackListRequest;
import com.odigeo.membership.request.product.UpdateMemberAccountRequest;
import com.odigeo.membership.request.product.UpdateMembershipRequest;
import com.odigeo.membership.request.product.creation.CreateMembershipRequest;
import com.odigeo.membership.request.product.creation.CreateNewMembershipRequest;
import com.odigeo.membership.request.product.creation.CreatePendingToCollectRequest;
import com.odigeo.membership.request.product.creation.UserCreationInfo;
import com.odigeo.membership.request.tracking.MembershipBookingTrackingRequest;
import com.odigeo.membership.response.Membership;
import com.odigeo.membership.response.WebsiteMembership;
import com.odigeo.membership.response.search.MembershipResponse;
import com.odigeo.membership.response.tracking.MembershipBookingTracking;
import com.odigeo.membership.search.SearchService;
import com.odigeo.membership.tracking.BookingTrackingService;
import com.odigeo.messaging.utils.MessageDataAccessException;
import ma.glasnost.orika.MapperFacade;
import org.apache.commons.lang.StringUtils;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.OngoingStubbing;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class MembershipServiceControllerTest {
    private static final long MEMBER_ACCOUNT_ID1 = 1L;
    private static final long MEMBER_ACCOUNT_ID2 = 2L;
    private static final long MEMBERSHIP_ID1 = 10L;
    private static final long MEMBERSHIP_ID2 = 11L;
    private static final long USER_ID = 1234L;
    private static final String NAME = "Name";
    private static final String LAST_NAMES = "LastNames";
    private static final String WEBSITE1 = "ES";
    private static final String WEBSITE2 = "DE";
    private static final String EMAIL = "write.me@mai.com";
    private static final String LOCALE = "fr_FR";
    private static final Integer TRAFFIC_INTERFACE_ID = 2;
    private static final MemberStatus MEMBER_STATUS_ACTIVATED = MemberStatus.ACTIVATED;
    private static final String ERROR_MESSAGE = "Error Message";
    private static final String ERROR_TYPE = "1";
    private static final Long BOOKING_ID = 123456L;
    private static final MembershipRenewal MEMBERSHIP_RENEWAL_ENABLED = MembershipRenewal.ENABLED;
    private static final MembershipRenewal MEMBERSHIP_RENEWAL_DISABLED = MembershipRenewal.DISABLED;
    private static final LocalDateTime MEMBERSHIP_EXPIRATION_DATE = LocalDateTime.now().plusYears(1L);
    private static final long MEMBER_ID_ACTIVATED = 123L;
    private static final long MEMBER_ID_DEACTIVATED = 456L;
    private static final long MEMBER_ID_PENDING_TO_ACTIVATED = 789L;
    private static final String ERROR = "ERROR";
    private static final String DATA_ACCESS_EXCEPTION_CREATING_MEMBERSHIP_SUBSCRIPTION_PRODUCT = "DataAccessException creating membershipSubscription product";
    private static final String ERROR_CHECKING_IF_MEMBER_IS_ON_BOARD = "Error checking if member is on board";
    private static final String EXCEPTION_TEST = "Exception Test";
    private static final MissingElementException MISSING_ELEMENT_EXCEPTION = new MissingElementException(StringUtils.EMPTY);

    @Captor
    ArgumentCaptor<MembershipCreation> membershipCreationCaptor;
    @Mock
    private MemberService memberService;
    @Mock
    private MemberAccountService memberAccountService;
    @Mock
    private UpdateMembershipService updateMembershipService;
    @Mock
    private BookingTrackingService bookingTrackingService;
    @Mock
    private MembershipValidationService membershipValidationService;
    @Mock
    private MapperFacade mapper;
    @Mock
    private BlacklistPaymentService blacklistPaymentService;
    @Mock
    private MemberUserAreaService memberUserAreaService;
    @Mock
    private MemberServiceMapping memberServiceMapping;
    @Mock
    private SearchService searchService;

    private MembershipServiceController membershipServiceController;

    @BeforeMethod
    public void init() {
        initMocks(this);
        ConfigurationEngine.init(new AbstractModule() {
            @Override
            protected void configure() {
                bind(MemberService.class).toInstance(memberService);
                bind(MemberAccountService.class).toInstance(memberAccountService);
                bind(UpdateMembershipService.class).toInstance(updateMembershipService);
                bind(BookingTrackingService.class).toInstance(bookingTrackingService);
                bind(BlacklistPaymentService.class).toProvider(() -> blacklistPaymentService);
                bind(MemberUserAreaService.class).toInstance(memberUserAreaService);
                bind(MemberServiceMapping.class).toInstance(memberServiceMapping);
                bind(MembershipValidationService.class).toInstance(membershipValidationService);
                bind(SearchService.class).toInstance(searchService);
            }
        });
        membershipServiceController = new MembershipServiceController(mapper);
        when(memberServiceMapping.asWebsiteMemberMap(any(List.class))).thenCallRealMethod();
        when(memberServiceMapping.asMembershipResponse(any(MemberAccount.class))).thenCallRealMethod();
        when(memberServiceMapping.min(any(Date.class), any(Date.class))).thenCallRealMethod();
        when(memberServiceMapping.max(any(Date.class), any(Date.class))).thenCallRealMethod();
        when(memberServiceMapping.parseStringToDate(anyString())).thenCallRealMethod();
    }

    @Test
    public void testGetMembership() throws Exception {
        setSingleActiveMember(MEMBERSHIP_RENEWAL_ENABLED, MEMBERSHIP_EXPIRATION_DATE, false);
        Membership membership = membershipServiceController.getMembership(USER_ID, WEBSITE1);
        assertTrue(membership.getWarnings().isEmpty());
        assertMembershipParameters(membership, WEBSITE1, LAST_NAMES, NAME, MEMBERSHIP_ID1);
    }

    @Test
    public void testGetMembershipWithWarnings() throws Exception {
        setSingleActiveMember(MEMBERSHIP_RENEWAL_ENABLED, MEMBERSHIP_EXPIRATION_DATE, true);
        Membership membership = membershipServiceController.getMembership(USER_ID, WEBSITE1);
        assertFalse(membership.getWarnings().isEmpty());
    }

    @Test
    public void testGetMembershipMultipleSites() throws Exception {
        setMultiActiveMember();
        setBookingLimitReached(false);
        Membership membership = membershipServiceController.getMembership(USER_ID, WEBSITE1);
        assertTrue(membership.getWarnings().isEmpty());
        assertMembershipParameters(membership, WEBSITE1, LAST_NAMES, NAME, MEMBERSHIP_ID1);
    }

    @Test
    public void testGetMembershipMissingMember() throws Exception {
        setMissingMember();
        setBookingLimitReached(false);
        assertNull(membershipServiceController.getMembership(USER_ID, WEBSITE1));
    }

    @Test
    public void testGetAllMembership() throws Exception {
        setSingleActiveMember(MEMBERSHIP_RENEWAL_ENABLED, MEMBERSHIP_EXPIRATION_DATE, false);
        Map<String, Membership> websiteMembershipMap = membershipServiceController.getAllMembership(USER_ID).getWebsiteMembershipMap();
        for (String key : websiteMembershipMap.keySet()) {
            assertMembershipParameters(websiteMembershipMap.get(key), WEBSITE1, LAST_NAMES, NAME, MEMBERSHIP_ID1);
        }
    }

    @Test
    public void testGetAllMembershipWithNoMatchingUserId() throws Exception {
        when(memberAccountService.getActiveMembersByUserId(MEMBERSHIP_ID1)).thenReturn(Collections.emptyList());
        WebsiteMembership websiteMembership = membershipServiceController.getAllMembership(USER_ID);
        assertTrue(websiteMembership.getWebsiteMembershipMap().isEmpty());
    }

    @Test
    public void testApplyMembershipMissingMember() throws Exception {
        when(membershipValidationService.applyMembership(anyObject())).thenThrow(MISSING_ELEMENT_EXCEPTION);
        CheckMemberOnPassengerListRequest request = getCheckMemberOnPassengerListRequest();
        assertFalse(membershipServiceController.applyMembership(request));
    }

    @Test
    public void testApplyMembershipHappyPath() throws Exception {
        testApplyMembership(true);
    }

    @Test
    public void testApplyMembershipFalse() throws Exception {
        testApplyMembership(false);
    }

    private void testApplyMembership(Boolean setApplyMember) throws Exception {
        setApplyMember(setApplyMember);
        CheckMemberOnPassengerListRequest request = getCheckMemberOnPassengerListRequest();
        assertEquals(membershipServiceController.applyMembership(request), setApplyMember);
    }

    @Test(expectedExceptions = MembershipServiceException.class)
    public void testApplyMembershipDataAccessException() throws Exception {
        CheckMemberOnPassengerListRequest checkMemberOnPassengerListRequest = mock(CheckMemberOnPassengerListRequest.class);
        when(membershipValidationService.applyMembership(any(MemberOnPassengerListParameter.class))).
                thenThrow(new DataAccessException(ERROR_CHECKING_IF_MEMBER_IS_ON_BOARD));
        assertFalse(membershipServiceController.applyMembership(checkMemberOnPassengerListRequest));
    }

    @Test
    public void testIsMembershipPerksActiveOnTrue() {
        setIsMembershipPerksActive(true);
        assertTrue(membershipServiceController.isMembershipPerksActiveOn(WEBSITE1));
    }

    @Test
    public void testIsMembershipPerksActiveOnFalse() {
        setIsMembershipPerksActive(false);
        assertFalse(membershipServiceController.isMembershipPerksActiveOn(WEBSITE1));
    }

    @Test(expectedExceptions = MembershipNotFoundException.class)
    public void testEnableAutoRenewalThrowsMee() throws MissingElementException, DataAccessException {
        when(updateMembershipService.enableAutoRenewal(any(UpdateMembership.class))).
                thenThrow(new MissingElementException(StringUtils.EMPTY));
        membershipServiceController.enableAutoRenewal(MEMBERSHIP_ID1);
    }

    @Test(expectedExceptions = MembershipServiceException.class)
    public void testGetMembershipThrowsMse() throws Exception {
        when(memberAccountService.getMembershipAccount(MEMBERSHIP_ID1, StringUtils.EMPTY)).
                thenThrow(new DataAccessException(DATA_ACCESS_EXCEPTION_CREATING_MEMBERSHIP_SUBSCRIPTION_PRODUCT));
        when(memberAccountService.getMemberWithActivatedMemberships(MEMBERSHIP_ID1, StringUtils.EMPTY)).
                thenThrow(new DataAccessException(DATA_ACCESS_EXCEPTION_CREATING_MEMBERSHIP_SUBSCRIPTION_PRODUCT));
        membershipServiceController.getMembership(MEMBERSHIP_ID1, StringUtils.EMPTY);
    }

    @Test(expectedExceptions = MembershipServiceException.class)
    public void testGetAllMembershipThrowsMse() throws Exception {
        when(memberAccountService.getActiveMembersByUserId(MEMBERSHIP_ID1)).
                thenThrow(new DataAccessException(DATA_ACCESS_EXCEPTION_CREATING_MEMBERSHIP_SUBSCRIPTION_PRODUCT));
        membershipServiceController.getAllMembership(MEMBERSHIP_ID1);
    }

    @Test(expectedExceptions = MembershipNotFoundException.class)
    public void testDisableMissingMember() throws Exception {
        when(updateMembershipService.deactivateMembership(anyObject())).thenThrow(MISSING_ELEMENT_EXCEPTION);
        membershipServiceController.disableMembership(MEMBERSHIP_ID1);
    }

    @Test(expectedExceptions = MembershipInternalServerErrorException.class)
    public void testDisableDataException() throws Exception {
        when(updateMembershipService.deactivateMembership(anyObject())).thenThrow(new DataAccessException(StringUtils.EMPTY));
        membershipServiceController.disableMembership(MEMBERSHIP_ID1);
    }

    @Test
    public void testDisableMembership() throws Exception {
        when(updateMembershipService.deactivateMembership(anyObject())).thenReturn(MemberStatus.DEACTIVATED.toString());
        assertEquals(membershipServiceController.disableMembership(MEMBERSHIP_ID1).getStatus(), MemberStatus.DEACTIVATED.toString());
    }

    @Test
    public void testReactivateMember() throws Exception {
        setReactivateMember();
        assertReactivation();
    }

    @Test(expectedExceptions = MembershipInternalServerErrorException.class)
    public void testReactivateMemberMembershipServiceException() throws Exception {
        setReactivateDataAccessExceptions();
        membershipServiceController.reactivateMembership(MEMBERSHIP_ID1);
    }

    @Test(expectedExceptions = MembershipNotFoundException.class)
    public void testReactivateMemberMembershipNotFoundException() throws Exception {
        setReactivateMissingExceptions();
        membershipServiceController.reactivateMembership(MEMBERSHIP_ID1);
    }

    @Test(expectedExceptions = MembershipNotFoundException.class)
    public void testDisableAutoRenewalThrowsMneException() throws Exception {
        when(updateMembershipService.disableAutoRenewal(any(UpdateMembership.class))).thenThrow(new MissingElementException(ERROR));
        membershipServiceController.disableAutoRenewal(MEMBERSHIP_ID1);
    }

    @Test(expectedExceptions = MembershipServiceException.class)
    public void testDisableAutoRenewalThrowsMseException() throws Exception {
        when(updateMembershipService.disableAutoRenewal(any(UpdateMembership.class))).thenThrow(new DataAccessException(ERROR));
        membershipServiceController.disableAutoRenewal(MEMBERSHIP_ID1);
    }

    @Test(expectedExceptions = MembershipNotFoundException.class)
    public void testEnableAutoRenewalThrowsMneException() throws Exception {
        when(updateMembershipService.enableAutoRenewal(any(UpdateMembership.class))).thenThrow(new MembershipNotFoundException(ERROR));
        membershipServiceController.enableAutoRenewal(MEMBERSHIP_ID1);
    }

    @Test(expectedExceptions = MembershipServiceException.class)
    public void testEnableAutoRenewalThrowsMseException() throws Exception {
        when(updateMembershipService.enableAutoRenewal(any(UpdateMembership.class))).thenThrow(new DataAccessException(ERROR));
        membershipServiceController.enableAutoRenewal(MEMBERSHIP_ID1);
    }

    private void assertReactivation() {
        assertEquals(MemberStatus.ACTIVATED.toString(), membershipServiceController.reactivateMembership(MEMBER_ID_ACTIVATED).getStatus());
        assertEquals(MemberStatus.ACTIVATED.toString(), membershipServiceController.reactivateMembership(MEMBER_ID_DEACTIVATED).getStatus());
        assertEquals(MemberStatus.PENDING_TO_ACTIVATE.toString(), membershipServiceController.reactivateMembership(MEMBER_ID_PENDING_TO_ACTIVATED).getStatus());
    }

    private void setReactivateMember() throws DataAccessException, MissingElementException, BookingApiException {
        when(updateMembershipService.reactivateMembership(any(UpdateMembership.class))).thenAnswer((invocation) -> {
            long memberId = Long.parseLong(((UpdateMembership) invocation.getArguments()[0]).getMembershipId());
            if (memberId == MEMBER_ID_ACTIVATED || memberId == MEMBER_ID_DEACTIVATED)
                return MemberStatus.ACTIVATED.toString();
            else if (memberId == MEMBER_ID_PENDING_TO_ACTIVATED)
                return MemberStatus.PENDING_TO_ACTIVATE.toString();
            else
                return StringUtils.EMPTY;
        });
    }

    private void setReactivateDataAccessExceptions() throws Exception {
        when(updateMembershipService.reactivateMembership(any(UpdateMembership.class))).thenThrow(new DataAccessException(ERROR));
    }

    private void setReactivateMissingExceptions() throws Exception {
        when(updateMembershipService.reactivateMembership(any(UpdateMembership.class))).thenThrow(new MissingElementException(ERROR));
    }

    @Test
    public void testDisableAutoRenewal() throws Exception {
        when(updateMembershipService.disableAutoRenewal(anyObject())).thenReturn(MEMBERSHIP_RENEWAL_DISABLED.toString());
        assertEquals(membershipServiceController.disableAutoRenewal(MEMBERSHIP_ID1).getRenewalStatus(), MembershipRenewal.DISABLED.toString());
    }

    @Test
    public void testEnableAutoRenewal() throws Exception {
        when(updateMembershipService.enableAutoRenewal(anyObject())).thenReturn(MEMBERSHIP_RENEWAL_ENABLED.toString());
        assertEquals(membershipServiceController.enableAutoRenewal(MEMBERSHIP_ID1).getRenewalStatus(), MembershipRenewal.ENABLED.toString());
    }

    @Test(expectedExceptions = MembershipInternalServerErrorException.class)
    public void testCreateMembershipDataAccessException() throws MissingElementException, DataAccessException {
        CreateNewMembershipRequest createNewMembershipRequest = getCreateNewMembershipRequest();
        when(memberService.createMembership(any(MembershipCreation.class))).thenThrow(new DataAccessException(ERROR_MESSAGE));
        membershipServiceController.createMembership(createNewMembershipRequest);
    }

    @Test(expectedExceptions = MembershipNotFoundException.class)
    public void testCreateMembershipMissingElementException() throws MissingElementException, DataAccessException {
        CreateNewMembershipRequest createNewMembershipRequest = getCreateNewMembershipRequest();
        when(memberService.createMembership(any(MembershipCreation.class))).thenThrow(MISSING_ELEMENT_EXCEPTION);
        membershipServiceController.createMembership(createNewMembershipRequest);
    }

    private CreateNewMembershipRequest getCreateNewMembershipRequest() {
        return new CreateNewMembershipRequest.Builder()
                .withUserId(String.valueOf(USER_ID))
                .withName(NAME)
                .withLastNames(LAST_NAMES)
                .withWebsite(WEBSITE1)
                .withSourceType(SourceType.FUNNEL_BOOKING.toString())
                .withMembershipType(MembershipType.BASIC.toString())
                .build();
    }

    @Test
    public void testCreateMembershipWithUserCreationInfo() throws MissingElementException, DataAccessException {
        CreateMembershipRequest request = getCreateRequestWithUserCreation();
        Long createdMembershipId = membershipServiceController.createMembership(request);
        assertNotNull(createdMembershipId);
        verify(memberService).createMembership(membershipCreationCaptor.capture());
        assertEquals(MemberStatus.PENDING_TO_ACTIVATE, membershipCreationCaptor.getValue().getMemberStatus());
    }

    @Test
    public void testCreateMembershipWithoutUserCreationInfo() throws MissingElementException, DataAccessException {
        CreateMembershipRequest request = new CreateNewMembershipRequest.Builder()
                .withWebsite(WEBSITE1)
                .withLastNames(LAST_NAMES)
                .withSourceType(SourceType.FUNNEL_BOOKING.name())
                .withMembershipType(MembershipType.BASIC.name())
                .withName(NAME)
                .withUserId(String.valueOf(USER_ID))
                .build();

        Long createdMembershipId = membershipServiceController.createMembership(request);
        assertNotNull(createdMembershipId);

        verify(memberService).createMembership(membershipCreationCaptor.capture());
        assertEquals(MemberStatus.PENDING_TO_ACTIVATE, membershipCreationCaptor.getValue().getMemberStatus());
    }

    @Test
    public void testCreateMembershipPendingToCollect() throws MissingElementException, DataAccessException {
        CreateMembershipRequest request = new CreatePendingToCollectRequest.Builder()
                .withWebsite(WEBSITE1)
                .withSourceType(SourceType.FUNNEL_BOOKING.name())
                .withMembershipType(MembershipType.BASIC.name())
                .withMemberAccountId(MEMBER_ACCOUNT_ID1)
                .withSubscriptionPrice(BigDecimal.TEN)
                .build();

        Long createdMembershipId = membershipServiceController.createMembership(request);
        assertNotNull(createdMembershipId);

        verify(memberService).createMembership(membershipCreationCaptor.capture());
        assertEquals(MemberStatus.PENDING_TO_COLLECT, membershipCreationCaptor.getValue().getMemberStatus());
    }

    @Test
    public void testUpdateMemberAccount() throws DataAccessException, MembershipServiceException, MissingElementException {
        UpdateMemberAccountRequest updateMemberAccountRequest = setUpdateUpdateMemberAccountRequest();
        when(memberAccountService.updateMemberAccount(mapper.map(updateMemberAccountRequest, UpdateMemberAccount.class))).thenReturn(Boolean.TRUE);
        assertTrue(membershipServiceController.updateMemberAccount(updateMemberAccountRequest));
    }

    @Test(expectedExceptions = MembershipServiceException.class)
    public void testUpdateMemberAccountThrowsException() throws DataAccessException, MembershipServiceException, MissingElementException {
        UpdateMemberAccountRequest updateMemberAccountRequest = setUpdateUpdateMemberAccountRequest();
        when(memberAccountService.updateMemberAccount(mapper.map(updateMemberAccountRequest, UpdateMemberAccount.class))).thenThrow(new DataAccessException(EXCEPTION_TEST));
        membershipServiceController.updateMemberAccount(updateMemberAccountRequest);
    }

    @Test
    public void testUpdateMembership() throws DataAccessException, MembershipServiceException {
        setSingleActiveMember(MEMBERSHIP_RENEWAL_DISABLED, MEMBERSHIP_EXPIRATION_DATE, false);
        UpdateMembershipRequest updateMembershipRequest = getUpdateMembershipRequest();
        if (membershipServiceController.updateMembership(updateMembershipRequest)) {
            assertEquals(MemberStatus.DEACTIVATED.name(), membershipServiceController.getMembership(USER_ID, WEBSITE1).getMembershipStatus());
        }
    }

    @Test(expectedExceptions = MembershipServiceException.class)
    public void testUpdateMembershipThrowsException() throws DataAccessException, MembershipServiceException {
        setSingleActiveMember(MEMBERSHIP_RENEWAL_DISABLED, MEMBERSHIP_EXPIRATION_DATE, false);
        UpdateMembershipRequest updateMembershipRequest = getUpdateMembershipRequest();
        when(membershipServiceController.updateMembership(mapper.map(updateMembershipRequest, UpdateMembershipRequest.class))).thenThrow(new DataAccessException(EXCEPTION_TEST));
        membershipServiceController.updateMembership(updateMembershipRequest);
    }

    @Test
    public void testGetBlacklistedPaymentMethods() throws MembershipServiceException {
        assertNotNull(membershipServiceController.getBlacklistedPaymentMethods(MEMBERSHIP_ID1));
    }

    @Test(expectedExceptions = MembershipInternalServerErrorException.class)
    public void testGetBlacklistedPaymentMethodsException() throws MembershipServiceException, DataAccessException {
        when(blacklistPaymentService.getBlacklistedPaymentMethods(eq(MEMBERSHIP_ID1))).thenThrow(new DataAccessException(StringUtils.EMPTY));
        membershipServiceController.getBlacklistedPaymentMethods(MEMBERSHIP_ID1);
    }

    @Test
    public void testAddToBlackList() throws MembershipServiceException {
        assertNotNull(membershipServiceController.addToBlackList(MEMBERSHIP_ID1, getAddToBlackListRequest()));
    }

    @Test(expectedExceptions = MembershipServiceException.class)
    public void testAddToBlackListThrowException() throws MembershipServiceException {
        when(membershipServiceController.addToBlackList(MEMBERSHIP_ID1, getAddToBlackListRequest())).thenThrow(new DataAccessException(EXCEPTION_TEST));
        membershipServiceController.addToBlackList(MEMBERSHIP_ID1, getAddToBlackListRequest());
    }

    @Test
    public void testIsMembershipToBeRenewed() throws DataAccessException, MissingElementException {
        when(membershipValidationService.isMembershipToBeRenewed(MEMBERSHIP_ID1)).thenReturn(true);
        assertTrue(membershipServiceController.isMembershipToBeRenewed(MEMBERSHIP_ID1));
    }

    @Test(expectedExceptions = MembershipServiceException.class, expectedExceptionsMessageRegExp = "MissingElementException retrieving membership " + MEMBERSHIP_ID1)
    public void testIsMembershipToBeRenewedMissingElementException() throws MissingElementException, DataAccessException {
        when(membershipValidationService.isMembershipToBeRenewed(MEMBERSHIP_ID1)).thenThrow(new MissingElementException("MissingElementException retrieving membership " + MEMBERSHIP_ID1));
        membershipServiceController.isMembershipToBeRenewed(MEMBERSHIP_ID1);
    }

    @Test(expectedExceptions = MembershipServiceException.class, expectedExceptionsMessageRegExp = "DataAccessException retrieving membership " + MEMBERSHIP_ID1)
    public void testIsMembershipToBeRenewedDataAccessException() throws MissingElementException, DataAccessException {
        when(membershipValidationService.isMembershipToBeRenewed(MEMBERSHIP_ID1)).thenThrow(new DataAccessException("DataAccessException retrieving membership " + MEMBERSHIP_ID1));
        membershipServiceController.isMembershipToBeRenewed(MEMBERSHIP_ID1);
    }

    @Test
    public void testGetCurrentMembership() throws DataAccessException, MembershipServiceException {
        com.odigeo.membership.Membership mockMembership = new MembershipBuilder().setStatus(MemberStatus.ACTIVATED).build();
        when(searchService.getCurrentMembership(any(), any())).thenReturn(Optional.of(mockMembership));
        when(mapper.map(any(com.odigeo.membership.Membership.class), any())).thenAnswer(this::setCurrentMembershipResponse);

        MembershipResponse currentMembership = membershipServiceController.getCurrentMembership(USER_ID, WEBSITE1);

        verify(searchService).getCurrentMembership(eq(USER_ID), eq(WEBSITE1));
        verify(mapper).map(eq(mockMembership), any());
        assertEquals(currentMembership.getStatus(), mockMembership.getStatus().name());
    }

    @Test
    public void testGetNoneCurrentMembership() throws DataAccessException, MembershipServiceException {
        when(searchService.getCurrentMembership(any(), any())).thenReturn(Optional.empty());
        when(mapper.map(any(com.odigeo.membership.Membership.class), any())).thenAnswer(this::setCurrentMembershipResponse);

        MembershipResponse currentMembership = membershipServiceController.getCurrentMembership(USER_ID, WEBSITE1);

        verify(searchService).getCurrentMembership(eq(USER_ID), eq(WEBSITE1));
        verify(mapper, never()).map(any(), any());
        assertNull(currentMembership);
    }

    @Test(expectedExceptions = MembershipServiceException.class)
    public void testGetExceptionInsteadOfCurrentMembership() throws DataAccessException, MembershipServiceException {
        when(searchService.getCurrentMembership(any(), any())).thenThrow(new DataAccessException("There was an error searching memberships: "));
        membershipServiceController.getCurrentMembership(USER_ID, WEBSITE1);
    }

    @Test
    public void testActivateMembership() throws MissingElementException, DataAccessException, MessageDataAccessException {
        ActivationRequest activationRequest = getActivationRequest();
        when(memberService.getMembershipById(eq(MEMBERSHIP_ID1))).thenReturn(new MembershipBuilder().setId(MEMBERSHIP_ID1).build());
        membershipServiceController.activateMembership(MEMBERSHIP_ID1, activationRequest);
        verify(memberService).activateMembership(eq(MEMBERSHIP_ID1), eq(BOOKING_ID), eq(activationRequest.getBalance()));
    }

    @Test(expectedExceptions = MembershipNotFoundException.class)
    public void testActivateMembershipMissingElement() throws MissingElementException, DataAccessException {
        when(memberService.getMembershipById(eq(MEMBERSHIP_ID1))).thenThrow(new MissingElementException(""));
        membershipServiceController.activateMembership(MEMBERSHIP_ID1, getActivationRequest());
    }

    @Test(expectedExceptions = MembershipInternalServerErrorException.class)
    public void testActivateMembershipDataAccessException() throws MissingElementException, DataAccessException {
        when(memberService.getMembershipById(eq(MEMBERSHIP_ID1))).thenThrow(new DataAccessException(""));
        membershipServiceController.activateMembership(MEMBERSHIP_ID1, getActivationRequest());
    }

    @Test(expectedExceptions = MembershipInternalServerErrorException.class)
    public void testActivateMembershipMessageDataAccessException() throws DataAccessException, MessageDataAccessException {
        ActivationRequest activationRequest = getActivationRequest();
        when(memberService.activateMembership(eq(MEMBERSHIP_ID1), eq(BOOKING_ID), eq(activationRequest.getBalance()))).thenThrow(new MessageDataAccessException("", new Exception()));
        membershipServiceController.activateMembership(MEMBERSHIP_ID1, activationRequest);
    }

    @Test(expectedExceptions = UnsupportedOperationException.class)
    public void testDeprecatedRetryActivation() {
        membershipServiceController.retryMembershipActivation(BOOKING_ID);
    }

    @Test
    public void testGetMembershipBookingTracking() throws DataAccessException {
        MembershipBookingTracking membershipBookingTracking = new MembershipBookingTracking();
        BookingTracking bookingTracking = new BookingTracking();
        when(bookingTrackingService.getMembershipBookingTracking(eq(BOOKING_ID))).thenReturn(bookingTracking);
        when(mapper.map(eq(bookingTracking), eq(MembershipBookingTracking.class))).thenReturn(membershipBookingTracking);
        MembershipBookingTracking membershipBookingTrackingResp = membershipServiceController.getBookingTracking(BOOKING_ID);
        verify(bookingTrackingService).getMembershipBookingTracking(eq(BOOKING_ID));
        verify(mapper).map(eq(bookingTracking), eq(MembershipBookingTracking.class));
        assertEquals(membershipBookingTrackingResp, membershipBookingTracking);
    }

    @Test(expectedExceptions = MembershipServiceException.class)
    public void testGetMembershipBookingTrackingException() throws DataAccessException {
        when(bookingTrackingService.getMembershipBookingTracking(eq(BOOKING_ID))).thenThrow(new DataAccessException(EXCEPTION_TEST));
        membershipServiceController.getBookingTracking(BOOKING_ID);
    }

    @Test
    public void testAddMembershipBookingTracking() throws DataAccessException {
        MembershipBookingTracking membershipBookingTracking = new MembershipBookingTracking();
        BookingTracking bookingTracking = new BookingTracking();
        when(bookingTrackingService.insertMembershipBookingTracking(eq(bookingTracking))).thenReturn(bookingTracking);
        when(mapper.map(eq(bookingTracking), eq(MembershipBookingTracking.class))).thenReturn(membershipBookingTracking);
        MembershipBookingTrackingRequest membershipBookingTrackingRequest = MembershipBookingTrackingRequest.builder(BOOKING_ID, MEMBERSHIP_ID1).build();
        when(mapper.map(eq(membershipBookingTrackingRequest), eq(BookingTracking.class))).thenReturn(bookingTracking);
        MembershipBookingTracking membershipBookingTrackingResponse = membershipServiceController.addBookingTrackingUpdateBalance(membershipBookingTrackingRequest);
        verify(bookingTrackingService).insertMembershipBookingTracking(eq(bookingTracking));
        assertEquals(membershipBookingTracking, membershipBookingTrackingResponse);
    }

    @Test(expectedExceptions = MembershipServiceException.class)
    public void testAddMembershipBookingTrackingThrowsException() throws DataAccessException {
        BookingTracking bookingTracking = new BookingTracking();
        when(bookingTrackingService.insertMembershipBookingTracking(eq(bookingTracking))).thenThrow(new DataAccessException(EXCEPTION_TEST));
        MembershipBookingTrackingRequest membershipBookingTrackingRequest = MembershipBookingTrackingRequest.builder(BOOKING_ID, MEMBERSHIP_ID1).build();
        when(mapper.map(eq(membershipBookingTrackingRequest), eq(BookingTracking.class))).thenReturn(bookingTracking);
        membershipServiceController.addBookingTrackingUpdateBalance(membershipBookingTrackingRequest);
    }

    @Test
    public void testDeleteMembershipBookingTracking() throws DataAccessException {
        MembershipBookingTrackingRequest membershipBookingTrackingRequest = MembershipBookingTrackingRequest.builder(BOOKING_ID, MEMBERSHIP_ID1).build();
        BookingTracking bookingTracking = new BookingTracking();
        when(mapper.map(eq(membershipBookingTrackingRequest), eq(BookingTracking.class))).thenReturn(bookingTracking);
        membershipServiceController.deleteBookingTrackingUpdateBalance(membershipBookingTrackingRequest);
        verify(bookingTrackingService).deleteMembershipBookingTracking(eq(bookingTracking));
    }

    @Test(expectedExceptions = MembershipServiceException.class)
    public void testDeleteMembershipBookingTrackingThrowsException() throws DataAccessException {
        MembershipBookingTrackingRequest membershipBookingTrackingRequest = MembershipBookingTrackingRequest.builder(BOOKING_ID, MEMBERSHIP_ID1).build();
        BookingTracking bookingTracking = new BookingTracking();
        when(mapper.map(eq(membershipBookingTrackingRequest), eq(BookingTracking.class))).thenReturn(bookingTracking);
        doThrow(new DataAccessException(EXCEPTION_TEST)).when(bookingTrackingService).deleteMembershipBookingTracking(eq(bookingTracking));
        membershipServiceController.deleteBookingTrackingUpdateBalance(membershipBookingTrackingRequest);
    }

    private ActivationRequest getActivationRequest() {
        ActivationRequest activationRequest = new ActivationRequest();
        activationRequest.setBalance(BigDecimal.ONE);
        activationRequest.setBookingId(BOOKING_ID);
        return activationRequest;
    }

    private MembershipResponse setCurrentMembershipResponse(InvocationOnMock invocation) {
        MembershipResponse membershipResponse = new MembershipResponse();
        com.odigeo.membership.Membership membership = (com.odigeo.membership.Membership) invocation.getArguments()[0];
        membershipResponse.setStatus(membership.getStatus().name());
        return membershipResponse;
    }

    private AddToBlackListRequest getAddToBlackListRequest() {
        AddToBlackListRequest addToBlackListRequest = new AddToBlackListRequest();
        List blackListedMethods = new ArrayList<BlacklistedPaymentMethod>();
        BlacklistedPaymentMethod blacklistedPaymentMethod = new BlacklistedPaymentMethod();
        blacklistedPaymentMethod.setMembershipId(MEMBERSHIP_ID1);
        blacklistedPaymentMethod.setErrorMessage(ERROR_MESSAGE);
        blacklistedPaymentMethod.setErrorType(ERROR_TYPE);
        blacklistedPaymentMethod.setId(1L);
        blacklistedPaymentMethod.setTimestamp(new Date());
        blackListedMethods.add(blacklistedPaymentMethod);
        addToBlackListRequest.setBlackListedPaymentMethods(blackListedMethods);
        return addToBlackListRequest;
    }

    private UpdateMemberAccountRequest setUpdateUpdateMemberAccountRequest() {
        UpdateMemberAccountRequest updateMembershipSubscriptionRequest = new UpdateMemberAccountRequest();
        updateMembershipSubscriptionRequest.setMemberAccountId(String.valueOf(MEMBER_ACCOUNT_ID1));
        updateMembershipSubscriptionRequest.setName(NAME);
        updateMembershipSubscriptionRequest.setLastNames(LAST_NAMES);
        return updateMembershipSubscriptionRequest;
    }

    private UpdateMembershipRequest getUpdateMembershipRequest() {
        UpdateMembershipRequest updateMembershipRequest = new UpdateMembershipRequest();
        updateMembershipRequest.setMembershipId(String.valueOf(MEMBER_ACCOUNT_ID1));
        updateMembershipRequest.setMembershipStatus(MemberStatus.ACTIVATED.name());
        updateMembershipRequest.setOperation(UpdateMembershipAction.REACTIVATE_MEMBERSHIP.toString());
        return updateMembershipRequest;
    }

    private OngoingStubbing<Boolean> setIsMembershipPerksActive(boolean value) {
        return when(membershipValidationService.isMembershipActiveOnWebsite(anyString())).thenReturn(value);
    }

    private CheckMemberOnPassengerListRequest getCheckMemberOnPassengerListRequest() {
        CheckMemberOnPassengerListRequest request = new CheckMemberOnPassengerListRequest();
        request.setSite(WEBSITE1);
        request.setUserId(USER_ID);
        List<TravellerParametersContainer> travellers = new ArrayList<>();
        TravellerParametersContainer traveller = new TravellerParametersContainer();
        traveller.setLastNames(LAST_NAMES);
        traveller.setName(NAME);
        travellers.add(traveller);
        request.setTravellerContainerList(travellers);
        return request;
    }

    private void setApplyMember(boolean apply) throws MissingElementException, DataAccessException {
        when(membershipValidationService.applyMembership(anyObject())).thenReturn(apply);
    }

    private void assertMembershipParameters(Membership membership, String website, String lastNames, String name, long memberId) {
        assertEquals(membership.getWebsite(), website);
        assertEquals(membership.getLastNames(), lastNames);
        assertEquals(membership.getName(), name);
        assertEquals(membership.getMemberId(), memberId);
    }

    private void setSingleActiveMember(MembershipRenewal membershipRenewal, LocalDateTime membershipExpirationDate, boolean isBookingLimitReached) throws DataAccessException {
        List<MemberAccount> memberAccountList = new ArrayList<>();
        List<com.odigeo.membership.Membership> membershipList = new ArrayList<>();

        MemberAccount memberAccount = new MemberAccount(MEMBER_ACCOUNT_ID1, USER_ID, NAME, LAST_NAMES);
        com.odigeo.membership.Membership membership = new MembershipBuilder().setId(MEMBERSHIP_ID1).setWebsite(WEBSITE1).setStatus(MEMBER_STATUS_ACTIVATED)
                .setMembershipRenewal(membershipRenewal).setExpirationDate(membershipExpirationDate).setMemberAccountId(MEMBER_ACCOUNT_ID1).setProductStatus(ProductStatus.CONTRACT).build();
        membership.setBookingLimitReached(isBookingLimitReached);
        membershipList.add(membership);
        memberAccount.setMemberships(membershipList);
        memberAccountList.add(memberAccount);

        when(memberAccountService.getMembershipAccount(anyLong(), anyString())).thenReturn(memberAccount);
        when(memberAccountService.getActiveMembersByUserId(anyLong())).thenReturn(memberAccountList);
        when(memberAccountService.getMemberWithActivatedMemberships(anyLong(), anyString())).thenReturn(memberAccount);
    }

    private void setMultiActiveMember() throws DataAccessException {
        List<MemberAccount> memberAccountList = new ArrayList<>();
        MemberAccount memberAccount1 = new MemberAccount(MEMBER_ACCOUNT_ID1, USER_ID, NAME, LAST_NAMES);
        MemberAccount memberAccount2 = new MemberAccount(MEMBER_ACCOUNT_ID2, USER_ID, NAME, LAST_NAMES);

        List<com.odigeo.membership.Membership> membershipList1 = new ArrayList<>();
        com.odigeo.membership.Membership membership1 = new MembershipBuilder().setId(MEMBERSHIP_ID1).setWebsite(WEBSITE1).setStatus(MEMBER_STATUS_ACTIVATED)
                .setMembershipRenewal(MEMBERSHIP_RENEWAL_ENABLED).setExpirationDate(MEMBERSHIP_EXPIRATION_DATE).setMemberAccountId(MEMBER_ACCOUNT_ID1).setProductStatus(ProductStatus.CONTRACT).build();
        membershipList1.add(membership1);
        memberAccount1.setMemberships(membershipList1);

        List<com.odigeo.membership.Membership> membershipList2 = new ArrayList<>();
        com.odigeo.membership.Membership membership2 = new MembershipBuilder().setId(MEMBERSHIP_ID2).setWebsite(WEBSITE2).setStatus(MEMBER_STATUS_ACTIVATED)
                .setMembershipRenewal(MEMBERSHIP_RENEWAL_ENABLED).setExpirationDate(MEMBERSHIP_EXPIRATION_DATE).setMemberAccountId(MEMBER_ACCOUNT_ID2).setProductStatus(ProductStatus.CONTRACT).build();
        membershipList2.add(membership2);
        memberAccount2.setMemberships(membershipList2);
        memberAccountList.add(memberAccount1);
        memberAccountList.add(memberAccount2);
        when(memberAccountService.getMembershipAccount(anyLong(), anyString())).thenReturn(memberAccount1);
        when(memberAccountService.getMemberWithActivatedMemberships(anyLong(), eq(WEBSITE1))).thenReturn(memberAccount1);
        when(memberAccountService.getMemberWithActivatedMemberships(anyLong(), eq(WEBSITE2))).thenReturn(memberAccount2);
    }

    private void setMissingMember() throws DataAccessException {
        when(memberAccountService.getMembershipAccount(anyLong(), anyString())).thenReturn(null);
        when(memberAccountService.getMemberWithActivatedMemberships(anyLong(), anyString())).thenReturn(null);
    }

    private void setBookingLimitReached(boolean isLimitReached) throws DataAccessException {
        when(bookingTrackingService.isBookingLimitReached(anyLong())).thenReturn(isLimitReached);
    }

    private CreateMembershipRequest getCreateRequestWithUserCreation() {
        UserCreationInfo userCreationInfo = getValidUserCreationInfo();
        return new CreateNewMembershipRequest.Builder()
                .withWebsite(WEBSITE1)
                .withLastNames(LAST_NAMES)
                .withSourceType(SourceType.FUNNEL_BOOKING.name())
                .withMembershipType(MembershipType.BASIC.name())
                .withName(NAME)
                .withUserCreationInfo(userCreationInfo)
                .build();
    }

    private UserCreationInfo getValidUserCreationInfo() {
        return new UserCreationInfo.Builder()
                .withLocale(LOCALE)
                .withEmail(EMAIL)
                .withTrafficInterfaceId(TRAFFIC_INTERFACE_ID)
                .build();
    }
}
