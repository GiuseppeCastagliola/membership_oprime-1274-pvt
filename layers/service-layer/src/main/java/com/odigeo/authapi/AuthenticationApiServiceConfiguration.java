package com.odigeo.authapi;

import com.edreams.configuration.ConfiguredInPropertiesFile;
import com.google.inject.Singleton;

@Singleton
@ConfiguredInPropertiesFile
public class AuthenticationApiServiceConfiguration {

    private String authApiHost;

    public String getAuthApiHost() {
        return authApiHost;
    }

    public void setAuthApiHost(String authApiHost) {
        this.authApiHost = authApiHost;
    }
}
