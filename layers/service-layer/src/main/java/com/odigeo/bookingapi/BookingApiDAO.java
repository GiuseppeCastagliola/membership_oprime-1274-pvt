package com.odigeo.bookingapi;

import com.edreams.base.DataAccessException;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.bookingsearchapi.v1.requests.SearchBookingsRequest;
import com.odigeo.bookingsearchapi.v1.responses.BookingSummary;
import com.odigeo.bookingsearchapi.v1.responses.SearchBookingsPageResponse;
import com.odigeo.membership.booking.SearchBookingsRequestBuilder;
import com.odigeo.membership.exception.bookingapi.BookingApiException;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Optional;

import static java.lang.String.format;
import static java.util.Collections.emptyList;

public class BookingApiDAO {

    private static final Logger LOGGER = Logger.getLogger(BookingApiDAO.class);

    public BookingDetail getBooking(final long bookingId) throws BookingApiException {
        return getBookingApiManager().getBooking(bookingId);
    }

    public List<BookingSummary> searchPrimeBookingsByMembershipId(final long membershipId) {
        return searchBookings(new SearchBookingsRequestBuilder()
                .setMembershipId(membershipId)
                .setBookingSubscriptionPrime(Boolean.TRUE.toString())
                .build());
    }

    public List<BookingSummary> searchBookingsByMembershipId(final long membershipId) {
        return searchBookings(new SearchBookingsRequestBuilder()
                .setMembershipId(membershipId)
                .build());
    }

    private List<BookingSummary> searchBookings(final SearchBookingsRequest searchBookingsRequest) {
        try {
            return Optional.of(getBookingApiManager().searchBookings(searchBookingsRequest))
                    .map(SearchBookingsPageResponse::getBookings)
                    .orElse(emptyList());
        } catch (DataAccessException exception) {
            LOGGER.error(format("Cannot retrieve bookings from booking request: %s", searchBookingsRequest));
            return emptyList();
        }
    }

    private BookingApiManager getBookingApiManager() {
        return ConfigurationEngine.getInstance(BookingApiManager.class);
    }
}
