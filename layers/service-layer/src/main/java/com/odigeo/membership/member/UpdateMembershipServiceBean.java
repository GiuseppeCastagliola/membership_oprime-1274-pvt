package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.crm.MemberStatusToCrmStatusMapper;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.UpdateMembership;
import com.odigeo.membership.UpdateMembershipAction;
import com.odigeo.membership.exception.DataAccessRollbackException;
import com.odigeo.membership.exception.DataNotFoundException;
import com.odigeo.membership.exception.ExistingRecurringException;
import com.odigeo.membership.exception.bookingapi.BookingApiException;
import com.odigeo.membership.member.update.MembershipUpdateOperation;
import com.odigeo.membership.recurring.MembershipRecurringStore;
import com.odigeo.membership.v4.messages.SubscriptionStatus;
import com.odigeo.messaging.MembershipMessageSendingService;
import com.odigeo.messaging.SubscriptionMessagePublisher;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import java.math.BigDecimal;
import java.security.InvalidParameterException;
import java.sql.SQLException;
import java.util.EnumMap;
import java.util.Optional;

@Stateless
@Local(UpdateMembershipService.class)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class UpdateMembershipServiceBean extends AbstractServiceBean implements UpdateMembershipService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateMembershipServiceBean.class);

    private final EnumMap<UpdateMembershipAction, MembershipUpdateOperation> updateOperationMap = new EnumMap<>(UpdateMembershipAction.class);

    private final MembershipUpdateOperation deactivateMemberOperation = updateMembership -> MemberStatus.DEACTIVATED.name().equals(deactivateMembership(updateMembership));
    private final MembershipUpdateOperation reactivateMemberOperation = updateMembership -> MemberStatus.ACTIVATED.name().equals(reactivateMembership(updateMembership));
    private final MembershipUpdateOperation enableAutoRenewalOperation = updateMembership -> MembershipRenewal.ENABLED.name().equals(enableAutoRenewal(updateMembership));
    private final MembershipUpdateOperation disableAutoRenewalOperation = updateMembership -> MembershipRenewal.DISABLED.name().equals(disableAutoRenewal(updateMembership));
    private final MembershipUpdateOperation expireMemberOperation = updateMembership -> MemberStatus.EXPIRED.name().equals(expireMembership(updateMembership));
    private final MembershipUpdateOperation discardMembership = updateMembership -> MemberStatus.DISCARDED.name().equals(discardMembership(updateMembership));
    private final MembershipUpdateOperation consumeMembershipRemnantBalance = this::consumeMembershipRemnantBalance;
    private final MembershipUpdateOperation addRecurringId = this::addRecurringId;

    @EJB
    private MemberService memberService;

    private MembershipStore membershipStore;
    private MemberStatusActionStore memberStatusActionStore;
    private SubscriptionMessagePublisher subscriptionMessagePublisher;
    private MembershipMessageSendingService membershipMessageSendingService;
    private MembershipRecurringStore membershipRecurringStore;

    public UpdateMembershipServiceBean() {
        updateOperationMap.put(UpdateMembershipAction.DEACTIVATE_MEMBERSHIP, deactivateMemberOperation);
        updateOperationMap.put(UpdateMembershipAction.REACTIVATE_MEMBERSHIP, reactivateMemberOperation);
        updateOperationMap.put(UpdateMembershipAction.ENABLE_AUTO_RENEWAL, enableAutoRenewalOperation);
        updateOperationMap.put(UpdateMembershipAction.DISABLE_AUTO_RENEWAL, disableAutoRenewalOperation);
        updateOperationMap.put(UpdateMembershipAction.EXPIRE_MEMBERSHIP, expireMemberOperation);
        updateOperationMap.put(UpdateMembershipAction.CONSUME_MEMBERSHIP_REMNANT_BALANCE, consumeMembershipRemnantBalance);
        updateOperationMap.put(UpdateMembershipAction.DISCARD_MEMBERSHIP, discardMembership);
        updateOperationMap.put(UpdateMembershipAction.INSERT_RECURRING_ID, addRecurringId);
    }

    @Override
    public Boolean updateMembership(UpdateMembership updateMembership) throws MissingElementException, DataAccessException, BookingApiException, ExistingRecurringException {
        return Optional.ofNullable(updateOperationMap.get(UpdateMembershipAction.valueOf(updateMembership.getOperation())))
            .orElseThrow(() -> new InvalidParameterException("Unknown operation parameter: " + updateMembership.getOperation()))
            .apply(updateMembership);
    }

    @Override
    public String disableAutoRenewal(UpdateMembership updateMembership) throws MissingElementException, DataAccessException {
        return changeAutoRenewal(Long.parseLong(updateMembership.getMembershipId()), MembershipRenewal.DISABLED, MembershipRenewal.ENABLED, (membershipId) -> getMemberManager().disableAutoRenewal(dataSource, membershipId));
    }

    @Override
    public String enableAutoRenewal(UpdateMembership updateMembership) throws MissingElementException, DataAccessException {
        return changeAutoRenewal(Long.parseLong(updateMembership.getMembershipId()), MembershipRenewal.ENABLED, MembershipRenewal.DISABLED, (membershipId) -> getMemberManager().enableAutoRenewal(dataSource, membershipId));
    }

    private String changeAutoRenewal(Long memberId, MembershipRenewal desiredValue, MembershipRenewal oldValue, FunctionWithException<Long, Boolean, DataAccessException> function) throws MissingElementException, DataAccessException {
        MembershipRenewal membershipRenewal = getMemberManager().getMembershipById(dataSource, memberId).getAutoRenewal();
        if (membershipRenewal.equals(oldValue) && function.apply(memberId)) {
            membershipRenewal = desiredValue;
            getMembershipMessageSendingService().sendMembershipIdToMembershipReporter(memberId);
        }
        return membershipRenewal.toString();
    }

    @Override
    public String deactivateMembership(UpdateMembership updateMembership) throws MissingElementException, DataAccessException {
        final Membership membership = memberService.getMembershipByIdWithMemberAccount(Long.parseLong(updateMembership.getMembershipId()));
        if (membership.getStatus() == MemberStatus.ACTIVATED) {
            return changeStatusMembership(membership, MemberStatus.DEACTIVATED, StatusAction.DEACTIVATION);
        }
        return membership.getStatus().toString();
    }

    @Override
    public String reactivateMembership(UpdateMembership updateMembership) throws DataAccessException, MissingElementException {
        final Membership membership = memberService.getMembershipByIdWithMemberAccount(Long.parseLong(updateMembership.getMembershipId()));
        if (membership.getStatus() == MemberStatus.DEACTIVATED) {
            return changeStatusMembership(membership, MemberStatus.ACTIVATED, StatusAction.ACTIVATION);
        }
        return membership.getStatus().toString();
    }

    @Override
    public String expireMembership(UpdateMembership updateMembership) throws DataAccessException, MissingElementException {
        final Membership membership = memberService.getMembershipByIdWithMemberAccount(Long.parseLong(updateMembership.getMembershipId()));
        return changeStatusMembership(membership, MemberStatus.EXPIRED, StatusAction.EXPIRATION);
    }

    @Override
    public String discardMembership(UpdateMembership updateMembership) throws MissingElementException, DataAccessException {
        final Membership membership = memberService.getMembershipByIdWithMemberAccount(Long.parseLong(updateMembership.getMembershipId()));
        return changeStatusMembership(membership, MemberStatus.DISCARDED, StatusAction.DISCARD);
    }

    private String changeStatusMembership(Membership membership, MemberStatus desiredNewStatus, StatusAction statusAction) throws DataAccessException {
        MemberStatus previousStatus = membership.getStatus();
        MemberStatus newStatus = performStatusChange(membership, desiredNewStatus);
        if (newStatus.equals(desiredNewStatus)) {
            storeMemberStatusAction(membership.getId(), statusAction);
            if (shouldInformCrm(previousStatus, membership)) {
                SubscriptionStatus subscriptionStatus = getMemberStatusToCrmStatusMapper().mapForUpdatedMembership(membership, newStatus);
                getSubscriptionMessagePublisher().sendSubscriptionMessageToCRMTopic(membership, subscriptionStatus);
            }
            getMembershipMessageSendingService().sendMembershipIdToMembershipReporter(membership.getId());
        }
        return newStatus.toString();
    }

    private boolean shouldInformCrm(MemberStatus previousStatus, final Membership membership) {
        return previousStatus.isNot(MemberStatus.PENDING_TO_ACTIVATE) && membership.getExpirationDate() != null;
    }

    @Override
    public boolean consumeMembershipRemnantBalance(UpdateMembership updateMembership) throws DataAccessException {
        try {
            return getMembershipStore().updateMembershipBalance(dataSource, Long.parseLong(updateMembership.getMembershipId()), BigDecimal.ZERO);
        } catch (SQLException e) {
            throw new DataAccessRollbackException(
                    "Error trying to reset membership balance to zero for membershipId " + updateMembership.getMembershipId(), e);
        }
    }

    private boolean addRecurringId(UpdateMembership updateMembership) throws DataAccessException, MissingElementException, ExistingRecurringException {
        try {
            final long membershipId = Long.parseLong(updateMembership.getMembershipId());
            validateMembershipExistsAndDoesNotHaveRecurring(membershipId);
            String recurringId = Optional.ofNullable(updateMembership.getRecurringId())
                    .filter(StringUtils::isNotEmpty)
                    .orElseThrow(() -> new InvalidParameterException("Recurring id can't be null or empty string"));
            getMembershipRecurringStore().insertMembershipRecurring(dataSource, membershipId, recurringId);
        } catch (SQLException e) {
            throw new DataAccessException("Error inserting the recurring ID for membership ID : " + updateMembership.getMembershipId(), e);
        } catch (DataNotFoundException e) {
            throw new MissingElementException("Membership ID : " + updateMembership.getMembershipId() + " not available.", e);
        }
        return Boolean.TRUE;
    }

    private void validateMembershipExistsAndDoesNotHaveRecurring(long membershipId) throws SQLException, DataNotFoundException, ExistingRecurringException {
        final Membership membership = Optional.ofNullable(getMembershipStore().fetchMembershipById(dataSource, membershipId))
                .orElseThrow(() -> new DataNotFoundException("Membership not found"));
        if (StringUtils.isNotEmpty(membership.getRecurringId())) {
            throw new ExistingRecurringException("Recurring id already exists for membership " + membership.getId());
        }
    }

    private MemberStatus performStatusChange(Membership membership, MemberStatus desiredStatus) throws DataAccessException {
        MemberStatus status = membership.getStatus();
        boolean isTransitionAllowed = status.isStatusTransitionAllowed(desiredStatus);
        if (isTransitionAllowed && getMembershipStore().updateStatus(dataSource, membership.getId(), desiredStatus)) {
            return desiredStatus;
        }
        return status;
    }

    private void storeMemberStatusAction(Long memberId, StatusAction action) throws DataAccessException {
        try {
            getMemberStatusActionStore().createMemberStatusAction(dataSource, memberId, action);
        } catch (SQLException e) {
            throw new DataAccessRollbackException("Error trying to create memberStatusAction " + action.toString() + " for membershipId " + memberId, e);
        }
    }

    @Override
    public boolean activatePendingToCollect(Membership membership) throws DataAccessException {
        boolean activationSuccess = activateRenewedMembership(membership);
        if (activationSuccess) {
            storeStatusActionForRenewedMembership(membership);
            sendRenewalMessageToCRMTopic(membership.getId());
            getMembershipMessageSendingService().sendMembershipIdToMembershipReporter(membership.getId());
        }
        return activationSuccess;
    }

    private boolean activateRenewedMembership(Membership membership) throws DataAccessException {
        try {
            return getMembershipStore().activatePendingToCollect(dataSource, membership);
        } catch (SQLException e) {
            throw new DataAccessException("Cannot activate PTC membership " + membership, e);
        }
    }

    private void storeStatusActionForRenewedMembership(Membership membershipRenewed) {
        try {
            storeMemberStatusAction(membershipRenewed.getId(), StatusAction.ACTIVATION);
        } catch (DataAccessException e) {
            LOGGER.error("Error creating status action for membershipId {}: ", membershipRenewed.getId(), e);
        }
    }

    private void sendRenewalMessageToCRMTopic(long membershipId) {
        LOGGER.info("Send subscription message for membershipId = {}", membershipId);
        try {
            final Membership updatedMembership = memberService.getMembershipByIdWithMemberAccount(membershipId);
            getSubscriptionMessagePublisher().sendSubscriptionMessageToCRMTopic(updatedMembership, SubscriptionStatus.SUBSCRIBED);
        } catch (MissingElementException | DataAccessException e) {
            LOGGER.error("Error retrieving membership and memberAccount for membershipId {}", membershipId);
        }
    }

    private MembershipStore getMembershipStore() {
        return Optional.ofNullable(membershipStore).orElseGet(() -> {
            membershipStore = ConfigurationEngine.getInstance(MembershipStore.class);
            return membershipStore;
        });
    }

    private MembershipRecurringStore getMembershipRecurringStore() {
        return Optional.ofNullable(membershipRecurringStore).orElseGet(() -> {
            membershipRecurringStore = ConfigurationEngine.getInstance(MembershipRecurringStore.class);
            return membershipRecurringStore;
        });
    }

    private MemberStatusActionStore getMemberStatusActionStore() {
        return Optional.ofNullable(memberStatusActionStore).orElseGet(() -> {
            memberStatusActionStore = ConfigurationEngine.getInstance(MemberStatusActionStore.class);
            return memberStatusActionStore;
        });
    }

    private MembershipMessageSendingService getMembershipMessageSendingService() {
        return Optional.ofNullable(membershipMessageSendingService).orElseGet(() -> {
            membershipMessageSendingService = ConfigurationEngine.getInstance(MembershipMessageSendingService.class);
            return membershipMessageSendingService;
        });
    }

    private SubscriptionMessagePublisher getSubscriptionMessagePublisher() {
        return Optional.ofNullable(subscriptionMessagePublisher).orElseGet(() -> {
            subscriptionMessagePublisher = ConfigurationEngine.getInstance(SubscriptionMessagePublisher.class);
            return subscriptionMessagePublisher;
        });
    }

    private MemberStatusToCrmStatusMapper getMemberStatusToCrmStatusMapper() {
        return ConfigurationEngine.getInstance(MemberStatusToCrmStatusMapper.class);
    }
}
