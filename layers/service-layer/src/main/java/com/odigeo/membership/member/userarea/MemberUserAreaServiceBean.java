package com.odigeo.membership.member.userarea;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.google.inject.Inject;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.commons.monitoring.metrics.MetricsBuilder;
import com.odigeo.commons.monitoring.metrics.MetricsNames;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberAccountBooking;
import com.odigeo.membership.MemberStatusAction;
import com.odigeo.membership.MemberSubscriptionDetails;
import com.odigeo.membership.Membership;
import com.odigeo.membership.exception.bookingapi.BookingApiException;
import com.odigeo.membership.member.MemberAccountService;
import com.odigeo.membership.member.MemberService;
import com.odigeo.membership.member.nosql.MembershipNoSQLManager;
import com.odigeo.membership.member.nosql.MembershipNoSQLRepository;
import com.odigeo.membership.member.statusaction.MemberStatusActionService;
import org.apache.log4j.Logger;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang.BooleanUtils.isFalse;

public class MemberUserAreaServiceBean implements MemberUserAreaService {

    private static final Logger LOGGER = Logger.getLogger(MemberUserAreaServiceBean.class);
    private static final int SECONDS_TO_EXPIRE = 43200;

    private final MemberSubscriptionDetailsBean memberSubscriptionDetailsBean;
    private final MemberService memberService;
    private final UserHasAnyMembershipForBrandPredicate userHasAnyMembershipForBrandPredicate;
    private final MemberStatusActionService memberStatusActionService;
    private final MemberAccountService memberAccountService;
    private final MembershipNoSQLManager membershipNoSQLManager;
    private final MembershipNoSQLRepository membershipNoSQLRepository;

    @Inject
    public MemberUserAreaServiceBean(final MemberSubscriptionDetailsBean memberSubscriptionDetailsBean,
                                     final MemberService memberService,
                                     final UserHasAnyMembershipForBrandPredicate userHasAnyMembershipForBrandPredicate,
                                     final MemberStatusActionService memberStatusActionService,
                                     final MemberAccountService memberAccountService,
                                     final MembershipNoSQLManager membershipNoSQLManager,
                                     final MembershipNoSQLRepository membershipNoSQLRepository) {
        this.memberSubscriptionDetailsBean = memberSubscriptionDetailsBean;
        this.memberService = memberService;
        this.userHasAnyMembershipForBrandPredicate = userHasAnyMembershipForBrandPredicate;
        this.memberStatusActionService = memberStatusActionService;
        this.memberAccountService = memberAccountService;
        this.membershipNoSQLManager = membershipNoSQLManager;
        this.membershipNoSQLRepository = membershipNoSQLRepository;
    }

    @Override
    public List<MemberSubscriptionDetails> getAllMemberSubscriptionDetails(long memberAccountId) throws MissingElementException, DataAccessException {
        return memberService.getMembershipsByAccountId(memberAccountId)
                .parallelStream()
                .map(Membership::getId)
                .map(memberSubscriptionDetailsBean::getMemberSubscriptionDetails)
                .collect(toList());
    }

    @Override
    public MemberSubscriptionDetails getMemberSubscriptionDetails(long membershipId) {
        return memberSubscriptionDetailsBean.getMemberSubscriptionDetails(membershipId);
    }

    @Override
    public BookingDetail getBookingDetailSubscription(long membershipId) throws BookingApiException {
        LOGGER.info("MembershipRetrieveBookingsServicesBean::Processing bookings with memberId =" + membershipId);
        return memberSubscriptionDetailsBean.getBookingDetailSubscriptionFromMembershipId(membershipId)
                .orElse(null);
    }

    @Override
    public boolean userHasAnyMembershipForBrand(final long userId, final String brandCode) throws DataAccessException {
        return userHasAnyMembershipForBrandPredicate.testAnyMembershipForBrand(userId, brandCode);
    }

    @Override
    public MemberAccountBooking getMembershipInfo(final long membershipId, final Boolean skipBooking) throws MissingElementException, DataAccessException, BookingApiException {
        Date lastStatusModificationDate = memberStatusActionService.lastStatusActionByMembershipId(membershipId).map(MemberStatusAction::getTimestamp).orElse(null);
        MemberAccount memberAccount = memberAccountService.getMemberAccountByMembershipId(membershipId);
        Long bookingDetailSubscriptionId = getBookingDetailId(membershipId, skipBooking).orElse(null);
        return new MemberAccountBooking(memberAccount, bookingDetailSubscriptionId, lastStatusModificationDate);
    }

    private Optional<Long> getBookingDetailId(final long membershipId, final Boolean skipBooking) throws BookingApiException {
        Optional<Long> bookingId = Optional.empty();
        if (isFalse(skipBooking)) {
            bookingId = getBookingIdForMembershipId(membershipId);
        }
        return bookingId;
    }

    private Optional<Long> getBookingIdForMembershipId(long membershipId) throws BookingApiException {
        MetricsUtils.startTimer(MetricsBuilder.composeResponseTimeMetric(MetricsNames.OPERATION_FETCH_REDIS), MetricsNames.METRICS_REGISTRY_NAME);
        Optional<Long> bookingId = membershipNoSQLManager.getBookingIdFromCache(membershipId);
        MetricsUtils.stopTimer(MetricsBuilder.composeResponseTimeMetric(MetricsNames.OPERATION_FETCH_REDIS), MetricsNames.METRICS_REGISTRY_NAME);
        if (bookingId.isPresent()) {
            MetricsUtils.incrementCounter(MetricsBuilder.buildMetric(MetricsNames.MEMORYSTORE_HIT_CACHE_SUCCESS), MetricsNames.METRICS_REGISTRY_NAME);
        } else {
            bookingId = getBookingIdFromBookingDetailSubscription(membershipId);
            MetricsUtils.incrementCounter(MetricsBuilder.buildMetric(MetricsNames.MEMORYSTORE_HIT_CACHE_FAILURE), MetricsNames.METRICS_REGISTRY_NAME);
        }
        return bookingId;
    }

    private Optional<Long> getBookingIdFromBookingDetailSubscription(long membershipId) throws BookingApiException {
        Optional<Long> bookingId = Optional.empty();
        final BookingDetail bookingDetailSubscription = getBookingDetailSubscription(membershipId);
        if (nonNull(bookingDetailSubscription) && nonNull(bookingDetailSubscription.getBookingBasicInfo())) {
            long bookingDetailId = bookingDetailSubscription.getBookingBasicInfo().getId();
            storeBookingIdByMembership(membershipId, bookingDetailId);
            bookingId = Optional.of(bookingDetailId);
        }
        return bookingId;
    }

    private void storeBookingIdByMembership(long membershipId, long bookingDetailId) {
        MetricsUtils.startTimer(MetricsBuilder.composeResponseTimeMetric(MetricsNames.OPERATION_STORE_REDIS), MetricsNames.METRICS_REGISTRY_NAME);
        try {
            membershipNoSQLRepository.store(String.valueOf(membershipId),
                    String.valueOf(bookingDetailId), SECONDS_TO_EXPIRE);
        } catch (DataAccessException e) {
            LOGGER.error("Error storing BookingId from the Redis cache with membershipId =" + membershipId);
            MetricsUtils.incrementCounter(MetricsBuilder.buildMetric(MetricsNames.MEMORYSTORE_FAILURE), MetricsNames.METRICS_REGISTRY_NAME);
        } finally {
            MetricsUtils.stopTimer(MetricsBuilder.composeResponseTimeMetric(MetricsNames.OPERATION_STORE_REDIS), MetricsNames.METRICS_REGISTRY_NAME);
        }
    }
}
