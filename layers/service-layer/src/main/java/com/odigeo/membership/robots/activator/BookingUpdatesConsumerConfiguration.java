package com.odigeo.membership.robots.activator;

import com.edreams.configuration.ConfiguredInPropertiesFile;
import com.google.inject.Singleton;

@Singleton
@ConfiguredInPropertiesFile
public class BookingUpdatesConsumerConfiguration {

    private String zooKeeperUrl;
    private String topicName;
    private String activatorConsumerGroup;
    private String trackerConsumerGroup;
    private int numConsumerThreads;

    public String getZooKeeperUrl() {
        return zooKeeperUrl;
    }

    public void setZooKeeperUrl(String zooKeeperUrl) {
        this.zooKeeperUrl = zooKeeperUrl;
    }

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public String getActivatorConsumerGroup() {
        return activatorConsumerGroup;
    }

    public void setActivatorConsumerGroup(String activatorConsumerGroup) {
        this.activatorConsumerGroup = activatorConsumerGroup;
    }

    public String getTrackerConsumerGroup() {
        return trackerConsumerGroup;
    }

    public void setTrackerConsumerGroup(String trackerConsumerGroup) {
        this.trackerConsumerGroup = trackerConsumerGroup;
    }

    public int getNumConsumerThreads() {
        return numConsumerThreads;
    }

    public void setNumConsumerThreads(int numConsumerThreads) {
        this.numConsumerThreads = numConsumerThreads;
    }

}
