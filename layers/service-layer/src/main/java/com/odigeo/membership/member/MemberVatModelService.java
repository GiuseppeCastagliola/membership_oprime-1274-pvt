package com.odigeo.membership.member;

import com.edreams.configuration.ConfiguredInPropertiesFile;
import com.google.inject.Singleton;

import java.time.LocalDate;

@Singleton
@ConfiguredInPropertiesFile
public class MemberVatModelService {

    private String cutOverDate;

    public String getCutOverDate() {
        return this.cutOverDate;
    }

    public void setCutOverDate(final String cutOverDate) {
        this.cutOverDate = cutOverDate;
    }

    public boolean isCutOverDateExpired() {
        return LocalDate.now().isBefore(LocalDate.parse(this.getCutOverDate()));
    }
}
