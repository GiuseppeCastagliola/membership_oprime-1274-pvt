package com.odigeo.membership.configuration;

import com.google.inject.AbstractModule;
import com.odigeo.authapi.AuthContract;
import com.odigeo.authapi.AuthenticationApiServiceManager;
import com.odigeo.membership.auth.usecases.AuthenticationLoginUseCase;
import com.odigeo.membership.auth.usecases.AuthenticationLoginUseCaseImpl;
import com.odigeo.membership.auth.usecases.AuthenticationLogoutUseCase;
import com.odigeo.membership.auth.usecases.AuthenticationLogoutUseCaseImpl;
import com.odigeo.membership.auth.usecases.AuthorizationPermissionsUseCase;
import com.odigeo.membership.auth.usecases.AuthorizationPermissionsUseCaseImpl;
import com.odigeo.membership.member.BackOfficeService;
import com.odigeo.membership.member.MemberAccountService;
import com.odigeo.membership.member.MemberService;
import com.odigeo.membership.member.MembershipPostBookingService;
import com.odigeo.membership.member.MembershipProductService;
import com.odigeo.membership.member.MembershipValidationService;
import com.odigeo.membership.member.UpdateMembershipService;
import com.odigeo.membership.member.statusaction.MemberStatusActionService;
import com.odigeo.membership.member.user.UserService;
import com.odigeo.membership.member.user.UserServiceBean;
import com.odigeo.membership.member.userarea.MemberUserAreaService;
import com.odigeo.membership.member.userarea.MemberUserAreaServiceBean;
import com.odigeo.membership.onboarding.OnboardingEventService;
import com.odigeo.membership.product.BlacklistPaymentService;
import com.odigeo.membership.propertiesconfig.PropertiesConfigurationService;
import com.odigeo.membership.robots.BookingUpdatesReaderService;
import com.odigeo.membership.robots.BookingUpdatesReaderServiceBean;
import com.odigeo.membership.search.SearchService;
import com.odigeo.membership.tracking.AutorenewalTrackingService;
import com.odigeo.membership.tracking.BookingTrackingService;
import com.odigeo.messaging.kafka.MembershipKafkaSender;
import com.odigeo.messaging.kafka.MembershipKafkaSenderBean;
import com.odigeo.messaging.kafka.MessageIntegrationService;
import org.apache.log4j.Logger;

@SuppressWarnings("PMD.CouplingBetweenObjects")
public class MemberServiceModule extends AbstractModule {

    private static final Logger logger = Logger.getLogger(MemberServiceModule.class);

    @Override
    protected void configure() {
        logger.info("Starting configure");
        bind(ServiceLocator.class).toInstance(JeeServiceLocator.getInstance());
        logger.info("ServiceLocator instantiated");
        bindJeeServices();
        bind(BookingUpdatesReaderService.class).to(BookingUpdatesReaderServiceBean.class);
        bind(MemberUserAreaService.class).to(MemberUserAreaServiceBean.class);
        bind(MembershipKafkaSender.class).to(MembershipKafkaSenderBean.class);
        bind(UserService.class).to(UserServiceBean.class);
        bind(AuthenticationLoginUseCase.class).to(AuthenticationLoginUseCaseImpl.class);
        bind(AuthenticationLogoutUseCase.class).to(AuthenticationLogoutUseCaseImpl.class);
        bind(AuthorizationPermissionsUseCase.class).to(AuthorizationPermissionsUseCaseImpl.class);
        bind(AuthContract.class).to(AuthenticationApiServiceManager.class);
        logger.info("Services configured");
    }

    private void bindJeeServices() {
        configureJeeService(MemberService.class);
        configureJeeService(MemberAccountService.class);
        configureJeeService(MembershipProductService.class);
        configureJeeService(UpdateMembershipService.class);
        configureJeeService(MembershipValidationService.class);
        configureJeeService(MembershipPostBookingService.class);
        configureJeeService(BackOfficeService.class);
        configureJeeService(BlacklistPaymentService.class);
        configureJeeService(BookingTrackingService.class);
        configureJeeService(MessageIntegrationService.class);
        configureJeeService(MemberStatusActionService.class);
        configureJeeService(SearchService.class);
        configureJeeService(OnboardingEventService.class);
        configureJeeService(PropertiesConfigurationService.class);
        configureJeeService(AutorenewalTrackingService.class);
    }

    private <T> void configureJeeService(Class<T> clazz) {
        configureJeeService(clazz, clazz);
    }

    private <T> void configureJeeService(Class<T> clazz, Class<? extends T> providerClazz) {
        try {
            bind(clazz).toProvider(JeeServiceProvider.getInstance(providerClazz));
        } catch (UnavailableServiceException usex) {
            logger.error("Unable to create provider " + providerClazz + " for class " + clazz, usex);
        }
    }
}
