package com.odigeo.membership.booking;

import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.bookingapi.v12.responses.Fee;
import com.odigeo.bookingapi.v12.responses.FeeContainer;
import com.odigeo.bookingapi.v12.responses.MembershipPerks;
import com.odigeo.membership.BookingTracking;

import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static com.odigeo.membership.booking.MembershipVatModelFee.AVOID_FEES;
import static com.odigeo.membership.booking.MembershipVatModelFee.COST_FEE;
import static com.odigeo.membership.booking.MembershipVatModelFee.PERKS;

public class BookingDetailWrapper {

    private static final String BOOKING_STATUS_CONTRACT = "CONTRACT";
    private final BookingDetail bookingDetail;

    public BookingDetailWrapper(@Nonnull BookingDetail bookingDetail) {
        this.bookingDetail = bookingDetail;
    }

    public long getBookingId() {
        return bookingDetail.getBookingBasicInfo().getId();
    }

    public BookingDetail getBookingDetail() {
        return this.bookingDetail;
    }

    public Optional<FeeContainer> getMembershipPerksFeeContainer() {
        return Optional.ofNullable(bookingDetail.getMembershipPerks())
                .map(MembershipPerks::getFeeContainerId)
                .flatMap(feeId -> bookingDetail.getAllFeeContainers()
                        .stream()
                        .filter(feeContainer -> feeId.compareTo(feeContainer.getId()) == 0)
                        .findFirst());
    }

    public BigDecimal calculateFeeAmount(MembershipVatModelFee membershipVatModelFees) {
        Optional<List<FeeContainer>> allFeeContainers = Optional.ofNullable(bookingDetail.getAllFeeContainers());
        if (allFeeContainers.isPresent()) {
            return allFeeContainers.get()
                    .stream()
                    .flatMap(feeContainer -> feeContainer.getFees().stream())
                    .filter(fee -> matchFee(fee, membershipVatModelFees))
                    .map(Fee::getAmount)
                    .reduce(BigDecimal.ZERO, BigDecimal::add);
        }
        return BigDecimal.ZERO;
    }

    private boolean matchFee(Fee fee, MembershipVatModelFee membershipVatModelFees) {
        if (Objects.nonNull(fee.getSubCode())) {
            return membershipVatModelFees.getFeeCode().equals(fee.getSubCode());
        }
        return membershipVatModelFees.name().equals(fee.getFeeLabel());
    }

    public BookingTracking createBookingTracking(long membershipId) {
        LocalDateTime bookingDate = LocalDateTime.ofInstant(bookingDetail.getBookingBasicInfo().getCreationDate().toInstant(), ZoneId.systemDefault());
        BookingTracking bookingTracking = new BookingTracking();
        bookingTracking.setMembershipId(membershipId);
        bookingTracking.setBookingId(bookingDetail.getBookingBasicInfo().getId());
        bookingTracking.setBookingDate(bookingDate);
        bookingTracking.setAvoidFeeAmount(calculateFeeAmount(AVOID_FEES));
        bookingTracking.setCostFeeAmount(calculateFeeAmount(COST_FEE));
        bookingTracking.setPerksAmount(calculateFeeAmount(PERKS));
        return bookingTracking;
    }

    public String getCurrencyCode() {
        return bookingDetail.getCurrencyCode();
    }

    public boolean isContract() {
        return BOOKING_STATUS_CONTRACT.equalsIgnoreCase(bookingDetail.getBookingStatus());
    }

    public boolean isPrimeBooking() {
        return Optional.ofNullable(bookingDetail.getMembershipPerks()).isPresent();
    }

    public long getMemberId() {
        return bookingDetail.getMembershipPerks().getMemberId();
    }

}
