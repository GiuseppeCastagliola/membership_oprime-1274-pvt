package com.odigeo.membership.robots.activator;

import com.odigeo.messaging.utils.BasicMessage;
import com.odigeo.messaging.utils.ConsumerIterator;
import org.apache.commons.collections.MapUtils;

public class MockBasicMessageConsumerIterator implements ConsumerIterator<BasicMessage> {
    private final String[] msgKeys;
    private int i;

    public MockBasicMessageConsumerIterator(String... msgKeys) {
        this.msgKeys = msgKeys;
        i = 0;
    }

    @Override
    public boolean hasNext() {
        return i < msgKeys.length;
    }

    @Override
    public BasicMessage next() {
        final BasicMessage basicMessage = new BasicMessage();
        basicMessage.setKey(msgKeys[i]);
        basicMessage.setTopicName(basicMessage.toString());
        basicMessage.setExtraParameters(MapUtils.EMPTY_MAP);
        i++;
        return basicMessage;
    }
}
