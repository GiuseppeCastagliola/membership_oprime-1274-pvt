package com.odigeo.membership.member.userarea;

import com.google.inject.Inject;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.bookingapi.v12.responses.CreditCard;
import com.odigeo.membership.MemberSubscriptionDetails;
import com.odigeo.membership.exception.bookingapi.BookingApiException;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Optional;

class MemberSubscriptionDetailsBean {

    private static final Logger LOGGER = Logger.getLogger(MemberSubscriptionDetailsBean.class);

    private final BookingDetailInfoExtractor bookingDetailInfoExtractor;
    private final MembershipBookingDetailsBean membershipBookingDetailsBean;

    @Inject
    MemberSubscriptionDetailsBean(final BookingDetailInfoExtractor bookingDetailInfoExtractor, final MembershipBookingDetailsBean membershipBookingDetailsBean) {
        this.bookingDetailInfoExtractor = bookingDetailInfoExtractor;
        this.membershipBookingDetailsBean = membershipBookingDetailsBean;
    }

    Optional<BookingDetail> getBookingDetailSubscriptionFromMembershipId(long membershipId) throws BookingApiException {
        return membershipBookingDetailsBean.getBookingDetailSubscriptionFromMembershipId(membershipId);
    }

    MemberSubscriptionDetails getMemberSubscriptionDetails(long membershipId) {
        List<BookingDetail> bookingDetails = membershipBookingDetailsBean.getBookingDetailsByMembershipId(membershipId);
        return fillMemberSubscriptionDetails(membershipId, bookingDetails);
    }

    private MemberSubscriptionDetails fillMemberSubscriptionDetails(final long membershipId, final List<BookingDetail> bookingDetails) {
        Optional<BookingDetail> subscriptionBookingDetail = bookingDetailFromMembershipId(membershipId);
        return new MemberSubscriptionDetails()
                .setMembershipId(membershipId)
                .setTotalSavings(bookingDetailInfoExtractor.extractTotalSavingsFromBookingDetails(bookingDetails))
                .setPrimeBookingsInfo(bookingDetailInfoExtractor.extractMemberSubscriptionDetailsFromBookingDetails(bookingDetails))
                .setSubscriptionPaymentMethod(subscriptionBookingDetail.flatMap(bookingDetailInfoExtractor.extractSubscriptionPaymentMethodFromBookingDetail()).orElseGet(CreditCard::new))
                .setSubscriptionPaymentMethodType(subscriptionBookingDetail.map(bookingDetailInfoExtractor.extractPaymentMethodTypeFromBookingDetail()).orElse(StringUtils.EMPTY));
    }

    private Optional<BookingDetail> bookingDetailFromMembershipId(final long membershipId) {
        try {
            return membershipBookingDetailsBean.getBookingDetailSubscriptionFromMembershipId(membershipId);
        } catch (BookingApiException e) {
            LOGGER.error("Cannot retrieve subscription booking for membership id: " + membershipId, e);
        }
        return Optional.empty();
    }
}
