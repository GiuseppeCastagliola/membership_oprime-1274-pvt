package com.odigeo.membership.configuration;

import com.edreams.base.BaseException;

public class UnavailableServiceException extends BaseException {

    public UnavailableServiceException(Class serviceType, Throwable th) {
        super(getMessage(serviceType), th);
    }

    private static String getMessage(Class serviceType) {
        return "Unable to obtain an instance for service " + serviceType.getName();
    }

}

