package com.odigeo.membership.tracking;

import com.odigeo.membership.AutorenewalTracking;

public interface AutorenewalTrackingService {
    void trackAutorenewalOperation(AutorenewalTracking autorenewalTracking);
}
