package com.odigeo.membership.robots;

public interface BookingUpdatesReaderService {
    boolean startTrackerConsumerController();
    void stopTrackerConsumerController();
}
