package com.odigeo.membership.member.userarea;

import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.bookingapi.v12.responses.CollectionAttempt;
import com.odigeo.bookingapi.v12.responses.CollectionSummary;
import com.odigeo.bookingapi.v12.responses.CreditCard;
import com.odigeo.bookingapi.v12.responses.FeeContainer;
import com.odigeo.bookingapi.v12.responses.MembershipPerks;
import com.odigeo.bookingapi.v12.responses.Money;
import com.odigeo.membership.PrimeBookingInformation;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

import static java.math.BigDecimal.ZERO;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toList;

class BookingDetailInfoExtractor {

    private static final String PAID = "PAID";

    Function<BookingDetail, Optional<CreditCard>> extractSubscriptionPaymentMethodFromBookingDetail() {
        return bookingDetail -> {
            Optional<CreditCard> lastCreditCard = lastCreditCardFromBooking(bookingDetail);
            if (lastCreditCard.isPresent()) {
                CreditCard creditCard = lastCreditCard.get();
                creditCard.setCreditCardNumber(CreditCardOperation.maskCreditCardNumber(creditCard.getCreditCardNumber()));
                return Optional.of(creditCard);
            }
            return Optional.empty();
        };
    }

    Function<BookingDetail, String> extractPaymentMethodTypeFromBookingDetail() {
        return bookingDetail -> {
            if (isNull(bookingDetail) || isNull(bookingDetail.getCollectionSummary()) || isNull(bookingDetail.getCollectionSummary().getAttempts())) {
                return StringUtils.EMPTY;
            }
            return bookingDetail.getCollectionSummary().getAttempts().stream()
                    .filter(paidMovementPredicate())
                    .findFirst()
                    .map(this::paymentMethodTypeFromCollectionAttempt)
                    .orElse(StringUtils.EMPTY);
        };
    }

    Money extractTotalSavingsFromBookingDetails(final List<BookingDetail> bookingDetails) {
        return bookingDetails.stream()
                .map(this.extractMembershipPerksAmountFromBookingDetail())
                .reduce(MoneyOperation::moneySum)
                .orElse(MoneyOperation.zero());
    }

    List<PrimeBookingInformation> extractMemberSubscriptionDetailsFromBookingDetails(final List<BookingDetail> bookingDetails) {
        return bookingDetails.parallelStream()
                .map(this::extractPrimeBookingInfoFromBookingDetail)
                .collect(toList());
    }

    private Optional<CreditCard> lastCreditCardFromBooking(final BookingDetail bookingDetail) {
        final CollectionSummary collectionSummary = bookingDetail.getCollectionSummary();
        if (nonNull(collectionSummary)) {
            CreditCard lastCreditCard = collectionSummary.getLastCreditCard();
            if (nonNull(lastCreditCard)) {
                return Optional.of(lastCreditCard);
            }
        }
        return Optional.empty();
    }

    private PrimeBookingInformation extractPrimeBookingInfoFromBookingDetail(final BookingDetail bookingDetail) {
        return new PrimeBookingInformation()
                .setPrice(bookingDetail.getPrice())
                .setBookingId(bookingDetail.getBookingBasicInfo().getId())
                .setDiscount(extractMembershipPerksAmountFromBookingDetail().apply(bookingDetail));
    }

    private Predicate<CollectionAttempt> paidMovementPredicate() {
        return collectionAttempt -> CollectionUtils.isNotEmpty(collectionAttempt.getValidMovements())
                && collectionAttempt.getValidMovements().stream()
                        .anyMatch(movement -> PAID.equalsIgnoreCase(movement.getStatus()));
    }

    private String paymentMethodTypeFromCollectionAttempt(final CollectionAttempt attempt) {
        String type = attempt.getType();
        String gatewayCollectionMethodType = attempt.getGatewayCollectionMethodType();
        return StringUtils.isNotEmpty(type) ? type : gatewayCollectionMethodType;
    }

    private Function<BookingDetail, Money> extractMembershipPerksAmountFromBookingDetail() {
        return bookingDetail -> {
            BigDecimal totalDiscount = findMembershipPerksFeeContainer(bookingDetail)
                    .map(FeeContainer::getTotalAmount)
                    .orElse(ZERO);
            return MoneyOperation.buildMoney(totalDiscount, bookingDetail.getCurrencyCode());
        };
    }

    private Optional<FeeContainer> findMembershipPerksFeeContainer(final BookingDetail bookingDetail) {
        return Optional.ofNullable(bookingDetail.getMembershipPerks())
                .map(MembershipPerks::getFeeContainerId)
                .flatMap(feeId -> bookingDetail.getAllFeeContainers()
                        .stream()
                        .filter(feeContainer -> feeId.compareTo(feeContainer.getId()) == 0)
                        .findFirst());
    }
}
