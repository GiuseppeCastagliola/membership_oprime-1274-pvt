package com.odigeo.membership.member.creation;

import com.edreams.configuration.ConfigurationEngine;
import com.google.common.collect.ImmutableMap;
import com.google.inject.Singleton;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.parameters.MembershipCreation;

import java.util.Map;
import java.util.function.Function;


@Singleton
public class MembershipCreationFactoryProvider {
    private static final String SEPARATOR = "-";
    private static final String PENDING_TO_COLLECT_TYPE_AND_STATUS = MembershipType.BASIC + SEPARATOR + MemberStatus.PENDING_TO_COLLECT;
    private static final String PENDING_TO_COLLECT_BUSINESS_TYPE_AND_STATUS = MembershipType.BUSINESS + SEPARATOR + MemberStatus.PENDING_TO_COLLECT;
    private static final String BASIC_FREE_TYPE_AND_STATUS = MembershipType.BASIC_FREE + SEPARATOR + MemberStatus.ACTIVATED;

    private static final Function<MembershipCreation, String> TYPE_AND_STATUS = membershipCreation -> membershipCreation.getMembershipType() + SEPARATOR + membershipCreation.getMemberStatus();
    private static Map<String, Class<? extends MembershipCreationFactory>> creationInstance = ImmutableMap.of(
            PENDING_TO_COLLECT_TYPE_AND_STATUS, PendingToCollectMembershipCreation.class,
            PENDING_TO_COLLECT_BUSINESS_TYPE_AND_STATUS, PendingToCollectMembershipCreation.class,
            BASIC_FREE_TYPE_AND_STATUS, BasicFreeMembershipCreation.class);

    public MembershipCreationFactory getInstance(MembershipCreation membershipCreation) {
        return ConfigurationEngine.getInstance(creationInstance.getOrDefault(TYPE_AND_STATUS.apply(membershipCreation), NewMembershipSubscriptionCreation.class));
    }

}
