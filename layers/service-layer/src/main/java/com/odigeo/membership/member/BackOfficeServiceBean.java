package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.crm.MemberStatusToCrmStatusMapper;
import com.odigeo.membership.Membership;
import com.odigeo.membership.v4.messages.MembershipSubscriptionMessage;
import com.odigeo.membership.v4.messages.SubscriptionStatus;
import com.odigeo.messaging.MembershipMessageSendingService;
import com.odigeo.messaging.MembershipSubscriptionMessageService;
import com.odigeo.messaging.SubscriptionMessagePublisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import java.util.Optional;

import static java.util.Objects.isNull;

@Stateless
@Local(BackOfficeService.class)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class BackOfficeServiceBean implements BackOfficeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BackOfficeServiceBean.class);

    @EJB
    private MemberService memberService;

    private MembershipMessageSendingService membershipMessageSendingService;
    private MemberStatusToCrmStatusMapper memberStatusToCrmStatusMapper;
    private MembershipSubscriptionMessageService membershipSubscriptionMessageService;
    private SubscriptionMessagePublisher subscriptionMessagePublisher;

    @Override
    public boolean updateMembershipMarketingInfo(Long membershipId) throws MissingElementException, DataAccessException {
        Membership membership = getMembershipWithMemberAccount(membershipId);
        SubscriptionStatus subscriptionStatus = getMemberStatusToCrmStatusMapper().map(membership.getStatus());
        MembershipSubscriptionMessage message = getMembershipSubscriptionMessageService().getMembershipSubscriptionMessage(membership, subscriptionStatus);
        return getMembershipMessageSendingService().sendCompletedMembershipSubscriptionMessage(message);
    }

    @Override
    public boolean sendWelcomeEmail(Long membershipId, Long bookingId) throws MissingElementException, DataAccessException {
        Membership membership = getMembershipWithMemberAccount(membershipId);
        LOGGER.info("sending welcome email related to bookingId {} membershipId {} userId {}", bookingId, membershipId, membership.getMemberAccount().getUserId());
        getSubscriptionMessagePublisher().sendWelcomeToPrimeMessageToMembershipTransactionalTopic(membership, bookingId, true);
        return true;
    }

    private Membership getMembershipWithMemberAccount(Long membershipId) throws MissingElementException, DataAccessException {
        return Optional.ofNullable(memberService.getMembershipByIdWithMemberAccount(membershipId))
            .orElseThrow(() -> new MissingElementException("Membership not found by id " + membershipId));
    }

    private MembershipSubscriptionMessageService getMembershipSubscriptionMessageService() {
        if (isNull(membershipSubscriptionMessageService)) {
            membershipSubscriptionMessageService = ConfigurationEngine.getInstance(MembershipSubscriptionMessageService.class);
        }
        return membershipSubscriptionMessageService;
    }

    private MembershipMessageSendingService getMembershipMessageSendingService() {
        if (isNull(membershipMessageSendingService)) {
            membershipMessageSendingService = ConfigurationEngine.getInstance(MembershipMessageSendingService.class);
        }
        return membershipMessageSendingService;
    }

    private MemberStatusToCrmStatusMapper getMemberStatusToCrmStatusMapper() {
        if (isNull(memberStatusToCrmStatusMapper)) {
            memberStatusToCrmStatusMapper = ConfigurationEngine.getInstance(MemberStatusToCrmStatusMapper.class);
        }
        return memberStatusToCrmStatusMapper;
    }

    private SubscriptionMessagePublisher getSubscriptionMessagePublisher() {
        if (isNull(subscriptionMessagePublisher)) {
            subscriptionMessagePublisher = ConfigurationEngine.getInstance(SubscriptionMessagePublisher.class);
        }
        return subscriptionMessagePublisher;
    }
}
