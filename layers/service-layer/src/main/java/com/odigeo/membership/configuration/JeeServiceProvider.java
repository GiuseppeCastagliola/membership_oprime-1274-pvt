package com.odigeo.membership.configuration;

import com.edreams.base.BaseRuntimeException;
import com.google.inject.Provider;

/**
 * This class enables the instantiation of EJBs via the <code>ConfigurationEngine</code>. It only
 * works for this kind of objects.
 */
public final class JeeServiceProvider<T> implements Provider<T> {

    private final Class<T> type;

    private JeeServiceProvider(Class<T> type) {
        this.type = type;
    }

    private static <T> void checkThatServiceExists(Class<T> type) throws UnavailableServiceException {
        JeeServiceLocator.getInstance().getService(type);
    }

    public static <T> JeeServiceProvider<T> getInstance(Class<T> type) throws UnavailableServiceException {
        checkThatServiceExists(type);
        return new JeeServiceProvider<T>(type);
    }

    public T get() {
        try {
            return JeeServiceLocator.getInstance().getService(type);
        } catch (UnavailableServiceException usex) {
            throw new BaseRuntimeException("Unable to obtain an instance of service " + type, usex);
        }
    }

}

