package com.odigeo.membership.member.creation;

import com.edreams.base.DataAccessException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Inject;
import com.odigeo.commons.monitoring.metrics.MetricsNames;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.exception.ActivatedMembershipException;
import com.odigeo.membership.exception.DataAccessRollbackException;
import com.odigeo.membership.member.MemberManager;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.parameters.search.MemberAccountSearch;
import com.odigeo.membership.parameters.search.MembershipSearch;
import com.odigeo.membership.search.SearchService;
import com.odigeo.membership.v4.messages.MembershipSubscriptionMessage;
import com.odigeo.membership.v4.messages.SubscriptionStatus;
import com.odigeo.messaging.MembershipMessageSendingService;
import com.odigeo.userapi.UserApiManager;
import com.odigeo.userapi.UserInfo;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static java.util.Objects.nonNull;
import static org.apache.commons.lang.StringUtils.isBlank;
import static org.apache.commons.lang.StringUtils.isNotBlank;

public class BasicFreeMembershipCreation extends MembershipCreationFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(BasicFreeMembershipCreation.class);
    private static final int BASIC_FREE_MONTHS_DURATION = 6;
    private final UserApiManager userApiManager;
    private final SearchService searchService;
    private final MembershipMessageSendingService membershipMessageSendingService;


    @Inject
    public BasicFreeMembershipCreation(UserApiManager userApiManager, SearchService searchService, MembershipMessageSendingService membershipMessageSendingService) {
        this.userApiManager = userApiManager;
        this.searchService = searchService;
        this.membershipMessageSendingService = membershipMessageSendingService;
    }

    @Override
    public Long createMembership(DataSource dataSource, MembershipCreation membershipCreation) throws DataAccessException {
        try {
            membershipCreation.setActivationDate(LocalDate.now());
            membershipCreation.setMonthsDuration(BASIC_FREE_MONTHS_DURATION);
            membershipCreation.setExpirationDate(LocalDate.now().plusMonths(BASIC_FREE_MONTHS_DURATION));
            membershipCreation.setBalance(BigDecimal.ZERO);
            Long memberId = createMembershipIfNotExistActive(membershipCreation, membershipCreation.getStatusAction(), dataSource);
            sendSubscriptionMessage(membershipCreation);
            incrementCreationMetricsCounter(MetricsNames.BASIC_FREE_CREATION);
            return memberId;
        } catch (DataAccessException | ActivatedMembershipException e) {
            throw new DataAccessRollbackException("Error creating membership for userId " + membershipCreation.getUserId(), e);
        }
    }

    private Long createMembershipIfNotExistActive(MembershipCreation membershipCreation, StatusAction statusAction, DataSource dataSource) throws ActivatedMembershipException, DataAccessException {
        List<Membership> memberships = searchService.searchMemberships(new MembershipSearch.Builder()
                .status(MemberStatus.ACTIVATED.name())
                .website(membershipCreation.getWebsite())
                .withMemberAccount(Boolean.TRUE)
                .memberAccountSearch(new MemberAccountSearch.Builder()
                        .userId(membershipCreation.getUserId())
                        .build())
                .build());
        if (CollectionUtils.isNotEmpty(memberships)) {
            throw new ActivatedMembershipException("An activated membership exists for userId " + membershipCreation.getUserId() + " and website " + membershipCreation.getWebsite());
        }
        Long memberId = getMemberManager().createMember(dataSource, membershipCreation);
        storeMemberStatusAction(dataSource, memberId, statusAction);
        membershipMessageSendingService.sendMembershipIdToMembershipReporter(memberId);
        return memberId;
    }

    private void sendSubscriptionMessage(MembershipCreation membershipCreation) {
        String email = retrieveEmail(membershipCreation);
        Optional.ofNullable(email).ifPresent(mail ->
                membershipMessageSendingService.sendMembershipSubscriptionMessage(new MembershipSubscriptionMessage
                        .Builder(mail, membershipCreation.getWebsite())
                        .withDates(membershipCreation.getActivationDate(), membershipCreation.getExpirationDate())
                        .withSubscriptionStatus(SubscriptionStatus.SUBSCRIBED).build())
        );
    }

    private String retrieveEmail(MembershipCreation membershipCreation) {
        String email;
        if (nonNull(membershipCreation.getUserCreation()) && isNotBlank(membershipCreation.getUserCreation().getEmail())) {
            email = membershipCreation.getUserCreation().getEmail();
        } else {
            UserInfo userInfo = userApiManager.getUserInfo(membershipCreation.getUserId());
            if (isBlank(userInfo.getEmail())) {
                LOGGER.warn("Email not retrieved related to user id received for basic_free creation ({}), no mkt email will be sent", membershipCreation.getUserId());
            }
            email = userInfo.getEmail();
        }
        return email;
    }

    private MemberManager getMemberManager() {
        return ConfigurationEngine.getInstance(MemberManager.class);
    }

}
