package com.odigeo.membership.tracking;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.odigeo.membership.BookingTracking;
import com.odigeo.membership.exception.RetryableException;

public interface BookingTrackingService {

    void processBooking(long bookingId) throws MissingElementException, RetryableException;

    boolean isBookingLimitReached(Long memberId) throws DataAccessException;

    BookingTracking getMembershipBookingTracking(long bookingId) throws DataAccessException;

    BookingTracking insertMembershipBookingTracking(BookingTracking bookingTracking) throws DataAccessException;

    void deleteMembershipBookingTracking(BookingTracking bookingTracking) throws DataAccessException;
}
