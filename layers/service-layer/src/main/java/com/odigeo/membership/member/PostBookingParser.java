package com.odigeo.membership.member;

import com.edreams.base.MissingElementException;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.enums.SourceType;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.parameters.MembershipCreationBuilder;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

public final class PostBookingParser {

    public static final String TOKEN = ";";
    private static final String END_LINE = "\r?\n";
    public static final int MAX_MONTHS_TO_RENEWAL = 12;
    public static final int DEFAULT_MONTHS_TO_RENEWAL = 6;
    public static final int FILE_ROW_OFFSET = 2;
    private static final String DEFAULT_MEMBERSHIP_TYPE = MembershipType.BASIC.toString();

    private static final int USER_ID_INDEX = 0;
    private static final int NAME_INDEX = 1;
    private static final int LASTNAMES_INDEX = 2;
    private static final int WEBSITE_INDEX = 3;
    private static final int EMAIL_INDEX = 4;
    private static final int MONTHS_TO_RENEWAL_INDEX = 5;
    private static final int MEMBERSHIP_TYPE_INDEX = 6;
    private static final String[] PB_MANDATORY_ATTRIBUTES = {"userId", "Name", "LastNames", "Website", "Email"};
    public static final int INTERNAL_EMPLOYEES_MIN_ATTRIBUTES = 4;
    public static final String CHANNEL_EMAIL = "EMAIL";
    public static final String WEBSITE_GB = "GB";

    private final List<String> lines;
    private final List<String> errors;
    private final boolean internalEmployees;
    private final String channel;


    public PostBookingParser(String csv, boolean internalEmployees, String channel) {
        this.internalEmployees = internalEmployees;
        this.channel = channel;
        this.errors = new ArrayList<>();
        lines = getLinesFromCsvAsString(csv);
    }

    private static List<String> getLinesFromCsvAsString(String csv) {
        if (Objects.nonNull(csv) && !csv.isEmpty()) {
            List<String> lines = Arrays.asList(csv.split(END_LINE));
            return lines.subList(1, lines.size());
        }
        return new ArrayList<>();
    }

    public String[] parseLine(String line) throws MissingElementException {
        String[] userInfo = line.split(TOKEN);
        int minLength = internalEmployees ? INTERNAL_EMPLOYEES_MIN_ATTRIBUTES : PB_MANDATORY_ATTRIBUTES.length;
        if (Objects.isNull(userInfo) || userInfo.length < minLength) {
            throw new MissingElementException("Wrong format in the processed line");
        }
        return userInfo;
    }

    private static int getMonthsToRenewal(String... userInfo) throws MissingElementException {
        if (userInfo.length > MONTHS_TO_RENEWAL_INDEX) {
            if (!StringUtils.EMPTY.equals(userInfo[MONTHS_TO_RENEWAL_INDEX]) && StringUtils.isNumeric(userInfo[MONTHS_TO_RENEWAL_INDEX])) {
                int numMonths = Integer.parseInt(userInfo[MONTHS_TO_RENEWAL_INDEX]);
                if (numMonths > MAX_MONTHS_TO_RENEWAL) {
                    throw new MissingElementException("Months to renewal exceeds the maximum of " + MAX_MONTHS_TO_RENEWAL);
                } else if (numMonths <= 0) {
                    throw new MissingElementException("Months to renewal cannot be 0 or negative");
                }
                return numMonths;
            } else if (StringUtils.isNotBlank(userInfo[MONTHS_TO_RENEWAL_INDEX])) {
                throw new MissingElementException("Wrong format for months to renewal");
            }
        }
        return DEFAULT_MONTHS_TO_RENEWAL;
    }

    private static long getUserId(String... userInfo) throws MissingElementException {
        try {
            return Long.parseLong(userInfo[USER_ID_INDEX].trim());
        } catch (NumberFormatException e) {
            throw new MissingElementException("Error trying to parse userId", e);
        }
    }

    private static String getNonNumericalAttribute(int attribute, String... userInfo) throws MissingElementException {
        String value = userInfo[attribute].trim();
        if (value.isEmpty()) {
            throw new MissingElementException(PB_MANDATORY_ATTRIBUTES[attribute] + " attribute empty");
        }
        return value;
    }

    public String getEmail(String... userInfo) throws MissingElementException {
        String email = getNonNumericalAttribute(EMAIL_INDEX, userInfo);
        if (Pattern.matches("(.+)@(.+)(\\.)(.+)", email)) {
            return email;
        }
        throw new MissingElementException("Wrong email format");
    }

    private static MembershipType getMembershipType(int attribute, String... userInfo) throws MissingElementException {
        String memberShipType = DEFAULT_MEMBERSHIP_TYPE;
        if (userInfo.length > MEMBERSHIP_TYPE_INDEX) {
            memberShipType = userInfo[attribute].trim();
            if (memberShipType.isEmpty()) {
                throw new MissingElementException(userInfo[attribute] + " attribute empty");
            }
        }
        return MembershipType.valueOf(memberShipType);
    }

    public MembershipCreation getMembershipCreation(String... userInfo) throws MissingElementException {
        MembershipCreation membershipCreation = new MembershipCreationBuilder()
                .withUserId(getUserId(userInfo))
                .withName(getNonNumericalAttribute(NAME_INDEX, userInfo))
                .withLastNames(getNonNumericalAttribute(LASTNAMES_INDEX, userInfo))
                .withWebsite(internalEmployees ? WEBSITE_GB : getNonNumericalAttribute(WEBSITE_INDEX, userInfo))
                .withMembershipType(getMembershipType(MEMBERSHIP_TYPE_INDEX, userInfo))
                .withSourceType(SourceType.POST_BOOKING)
                .withMemberStatus(MemberStatus.ACTIVATED)
                .withSubscriptionPrice(BigDecimal.ZERO)
                .build();
        if (internalEmployees) {
            membershipCreation.setStatusAction(StatusAction.INTERNAL_CREATION);
        } else {
            LocalDate activationDate = LocalDate.now();
            membershipCreation.setActivationDate(activationDate);
            membershipCreation.setExpirationDate(activationDate.plusMonths(getMonthsToRenewal(userInfo)));
            membershipCreation.setStatusAction(
                    CHANNEL_EMAIL.equals(channel) ? StatusAction.PB_EMAIL_CREATION : StatusAction.PB_PHONE_CREATION);
        }
        return membershipCreation;
    }

    public boolean isInternalEmployees() {
        return internalEmployees;
    }

    public List<String> getLines() {
        return lines;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void addError(String error) {
        errors.add(error);
    }
}
