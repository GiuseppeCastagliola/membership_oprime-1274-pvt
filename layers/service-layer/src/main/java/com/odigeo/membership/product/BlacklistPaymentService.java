package com.odigeo.membership.product;

import com.edreams.base.DataAccessException;
import com.odigeo.membership.BlacklistedPaymentMethod;

import java.util.List;

public interface BlacklistPaymentService {

    List<BlacklistedPaymentMethod> getBlacklistedPaymentMethods(Long membershipId) throws DataAccessException;

    Boolean addToBlackList(List<BlacklistedPaymentMethod> blacklistedPaymentMethods) throws DataAccessException;

}
