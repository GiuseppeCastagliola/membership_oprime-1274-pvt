package com.odigeo.membership.booking;

public enum MembershipVatModelFee {
    AVOID_FEES("AB35"),
    COST_FEE("AB32"),
    PERKS("AB34");

    private String feeCode;

    MembershipVatModelFee(String feeCode) {
        this.feeCode = feeCode;
    }

    public String getFeeCode() {
        return feeCode;
    }
}
