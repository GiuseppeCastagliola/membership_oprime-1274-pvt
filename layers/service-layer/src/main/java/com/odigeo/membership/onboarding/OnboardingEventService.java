package com.odigeo.membership.onboarding;

import com.edreams.base.DataAccessException;

public interface OnboardingEventService {

    long createOnboarding(Onboarding onboarding) throws DataAccessException;
}
