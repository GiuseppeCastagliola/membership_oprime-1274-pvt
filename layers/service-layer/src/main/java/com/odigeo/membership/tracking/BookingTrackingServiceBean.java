package com.odigeo.membership.tracking;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.bookingapi.BookingApiDAO;
import com.odigeo.bookingapi.BookingApiManager;
import com.odigeo.bookingapi.v12.requests.CreateBookingProductCategoryRequest;
import com.odigeo.bookingapi.v12.requests.FeeRequest;
import com.odigeo.bookingapi.v12.requests.PaymentType;
import com.odigeo.bookingapi.v12.requests.ProductCategoryBookingItemType;
import com.odigeo.bookingapi.v12.requests.Status;
import com.odigeo.bookingapi.v12.requests.UpdateBookingRequest;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.commons.monitoring.metrics.MetricsBuilder;
import com.odigeo.commons.monitoring.metrics.MetricsNames;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.membership.AutoRenewalOperation;
import com.odigeo.membership.BookingTracking;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.booking.BookingDetailWrapper;
import com.odigeo.membership.booking.UpdateBookingRequestBuilder;
import com.odigeo.membership.booking.UpdateBookingRequestBuilder.CreateOtherProductProviderBookingBuilder;
import com.odigeo.membership.booking.UpdateBookingRequestBuilder.CreateProviderPaymentDetailRequestBuilder;
import com.odigeo.membership.exception.DataAccessRollbackException;
import com.odigeo.membership.exception.DataNotFoundException;
import com.odigeo.membership.exception.RetryableException;
import com.odigeo.membership.exception.bookingapi.BookingApiException;
import com.odigeo.membership.member.AbstractServiceBean;
import com.odigeo.membership.member.MembershipStore;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Optional;
import java.util.function.Predicate;

import static com.odigeo.membership.MembershipRenewal.DISABLED;
import static com.odigeo.membership.enums.MembershipType.BASIC_FREE;
import static java.util.Objects.isNull;

@Stateless
@Local(BookingTrackingService.class)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class BookingTrackingServiceBean extends AbstractServiceBean implements BookingTrackingService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookingTrackingServiceBean.class);
    private static final Predicate<Membership> BASIC_FREE_MEMBERSHIP_TO_UPDATE = membership -> BASIC_FREE.equals(membership.getMembershipType())
            && DISABLED.equals(membership.getAutoRenewal());
    private static final Predicate<BookingDetailWrapper> BOOKING_CONTRACT_AND_MEMBERSHIP_PRODUCT_MISSING = bookingDetailWrapper -> bookingDetailWrapper.isContract()
            && bookingDetailWrapper.getBookingDetail().getAllFeeContainers().stream()
            .noneMatch(feeContainer -> "MEMBERSHIP_SUBSCRIPTION".equalsIgnoreCase(feeContainer.getFeeContainerType()));

    @Override
    public void processBooking(long bookingId) throws MissingElementException, RetryableException {
        try {
            BookingDetail bookingDetail = Optional.ofNullable(getBookingApiManager().getBooking(bookingId))
                    .orElseThrow(() -> new RetryableException("Error retrieving booking detail " + bookingId));
            BookingDetailWrapper bookingDetailWrapper = new BookingDetailWrapper(bookingDetail);
            if (bookingDetailWrapper.isPrimeBooking()) {
                Membership membership = getActivatedMembershipBlocking(bookingDetailWrapper);
                Optional<BookingDetail> optionalUpdatedBooking = Optional.empty();
                if (BASIC_FREE_MEMBERSHIP_TO_UPDATE.test(membership)) {
                    optionalUpdatedBooking = updateIfFirstBooking(bookingDetailWrapper, membership);
                }
                if (optionalUpdatedBooking.isPresent()) {
                    getMembershipStore().updateAutoRenewal(dataSource, membership.getId(), AutoRenewalOperation.ENABLE_AUTO_RENEW);
                    LOGGER.info("Auto renewal enabled for membership {}", membership.getId());
                } else {
                    boolean alreadyTracked = getBookingTrackingStore().isBookingTracked(dataSource, bookingId);
                    AbstractBookingTracking bookingTracking = getBookingTrackingInstance(bookingDetailWrapper, membership);
                    LOGGER.info("Going to process booking {} for tracking, was already tracked? : {}", bookingId, alreadyTracked);
                    if (bookingDetailWrapper.isContract()) {
                        LOGGER.info("Tracking booking {} status CONTRACT and membership id {} ", bookingId, membership.getId());
                        bookingTracking.trackBookingAndUpdateBalance(dataSource);
                    } else if (alreadyTracked) {
                        LOGGER.info("Deleting already tracked booking {} not CONTRACT, membership id {} ", bookingId, membership.getId());
                        bookingTracking.deleteTrackedBookingAndRestoreBalance(dataSource);
                    }
                    LOGGER.info("Process finished with bookingId {}, memberId {} released", bookingId, membership.getId());
                }
            }
        } catch (BookingApiException | DataAccessException e) {
            throw new RetryableException(new MissingElementException("Cannot get/update booking for bookingId: " + bookingId, e));
        } catch (SQLException e) {
            LOGGER.info("Error processing bookingId {}: {}", bookingId, e.getMessage());
            throw new RetryableException(new DataAccessException("Cannot access to the DB resource for tracking of booking " + bookingId, e));
        }
    }

    private Optional<BookingDetail> updateIfFirstBooking(BookingDetailWrapper bookingDetailWrapper, Membership membership) throws DataAccessException, RetryableException {
        BookingApiDAO bookingApiDAO = ConfigurationEngine.getInstance(BookingApiDAO.class);
        Long membershipId = membership.getId();
        if (BOOKING_CONTRACT_AND_MEMBERSHIP_PRODUCT_MISSING.test(bookingDetailWrapper) && CollectionUtils.isEmpty(bookingApiDAO.searchPrimeBookingsByMembershipId(membershipId))) {
            String currency = StringUtils.defaultIfEmpty(membership.getCurrencyCode(), bookingDetailWrapper.getCurrencyCode());
            UpdateBookingRequest updateBookingRequest = buildUpdateRequest(membershipId, currency);
            BookingDetail bookingDetail = getBookingApiManager().updateBooking(bookingDetailWrapper.getBookingId(), updateBookingRequest);
            if (isNull(bookingDetail) || StringUtils.isNotEmpty(bookingDetail.getErrorMessage())) {
                String message = isNull(bookingDetail) ? "Null bookingDetail after trying update" : bookingDetail.getErrorMessage();
                throw new RetryableException(message);
            }
            LOGGER.info("Booking {} updated, subscription product added", bookingDetailWrapper.getBookingId());
            return Optional.of(bookingDetail);
        }
        return Optional.empty();
    }

    private UpdateBookingRequest buildUpdateRequest(Long membershipId, String currency) {
        FeeRequest feeRequest = new FeeRequest();
        feeRequest.setCurrency(currency);
        feeRequest.setAmount(BigDecimal.ZERO);
        CreateProviderPaymentDetailRequestBuilder createProviderPaymentDetailRequestBuilder = new CreateProviderPaymentDetailRequestBuilder(PaymentType.CASH, BigDecimal.ZERO, currency);
        CreateOtherProductProviderBookingBuilder createOtherProductProviderBookingBuilder = new CreateOtherProductProviderBookingBuilder(Status.CONTRACT, BigDecimal.ZERO, currency, membershipId)
                .createProviderPaymentDetailRequestBuilder(createProviderPaymentDetailRequestBuilder);
        CreateBookingProductCategoryRequest createProduct = new UpdateBookingRequestBuilder
                .CreateBookingProductCategoryRequestBuilder(ProductCategoryBookingItemType.MEMBERSHIP_SUBSCRIPTION)
                .createOtherProductProviderBookingBuilder(createOtherProductProviderBookingBuilder)
                .feeRequests(feeRequest)
                .build();
        return new UpdateBookingRequestBuilder().createBookingProductCategoryRequests(createProduct).buildAddProduct();
    }

    private AbstractBookingTracking getBookingTrackingInstance(BookingDetailWrapper bookingDetailWrapper, Membership membership) {
        return new BookingTrackingFactory().create(bookingDetailWrapper, membership);
    }

    Membership getActivatedMembershipBlocking(BookingDetailWrapper bookingDetailWrapper)
            throws MissingElementException, SQLException, DataNotFoundException, RetryableException {
        long bookingId = bookingDetailWrapper.getBookingId();
        LOGGER.info("Trying to read and lock memberId {} for bookingId {}", bookingDetailWrapper.getMemberId(), bookingId);
        Membership membership = Optional
                .ofNullable(getMembershipStore().fetchMembersByIdBlocking(dataSource, bookingDetailWrapper.getMemberId()))
                .orElseThrow(() -> new MissingElementException("Error retrieving membership from booking detail " + bookingId));
        if (MemberStatus.PENDING_TO_ACTIVATE == membership.getStatus()) {
            throw new RetryableException("Membership " + membership.getId() + " still pending, booking " + bookingId
                    + " status " + bookingDetailWrapper.getBookingDetail().getBookingStatus() + ". Will be retried waiting for activation");
        }
        LOGGER.info("MemberId {} lock acquired", membership.getId());
        return membership;
    }

    @Override
    public boolean isBookingLimitReached(Long memberId) throws DataAccessException {
        try {
            return getBookingTrackingStore().isBookingLimitReached(dataSource, memberId);
        } catch (SQLException e) {
            throw new DataAccessException("Cannot search for tracked bookings for member " + memberId, e);
        }
    }

    @Override
    public BookingTracking getMembershipBookingTracking(long bookingId) throws DataAccessException {
        try {
            return getBookingTrackingStore().getBookingTracked(dataSource, bookingId);
        } catch (SQLException e) {
            throw new DataAccessException("Exception retrieving booking tracking: " + bookingId, e);
        }
    }

    @Override
    public BookingTracking insertMembershipBookingTracking(BookingTracking bookingTracking) throws DataAccessException {
        try {
            getBookingTrackingStore().trackBooking(dataSource, bookingTracking);
            Membership membership = getMembershipStore().fetchMembershipById(dataSource, bookingTracking.getMembershipId());
            BigDecimal newBalance = membership.getBalance().add(bookingTracking.getAvoidFeeAmount());
            getMembershipStore().updateMembershipBalance(dataSource, membership.getId(), newBalance);
            MetricsUtils.incrementCounter(MetricsBuilder.buildMetric(MetricsNames.TRACKED_BOOKINGS_NUMBER), MetricsNames.METRICS_REGISTRY_NAME);
        } catch (SQLException | DataNotFoundException e) {
            throw new DataAccessRollbackException("Exception inserting tracking of: membership " + bookingTracking.getMembershipId()
                    + " booking " + bookingTracking.getBookingId(), e);
        }
        return getMembershipBookingTracking(bookingTracking.getBookingId());
    }

    @Override
    public void deleteMembershipBookingTracking(BookingTracking bookingTracking) throws DataAccessException {
        try {
            BookingTracking bookingTracked = getBookingTrackingStore().getBookingTracked(dataSource, bookingTracking.getBookingId());
            getBookingTrackingStore().removeTrackedBooking(dataSource, bookingTracking.getBookingId());
            Membership membership = getMembershipStore().fetchMembershipById(dataSource, bookingTracking.getMembershipId());
            BigDecimal newBalance = membership.getBalance().subtract(bookingTracked.getAvoidFeeAmount());
            getMembershipStore().updateMembershipBalance(dataSource, membership.getId(), newBalance);
            MetricsUtils.incrementCounter(MetricsBuilder.buildMetric(MetricsNames.TRACKED_BOOKINGS_NUMBER), MetricsNames.METRICS_REGISTRY_NAME, -1L);
        } catch (SQLException e) {
            throw new DataAccessRollbackException("Exception deleting tracking of: membership " + bookingTracking.getMembershipId()
                    + " booking " + bookingTracking.getBookingId(), e);
        }
    }

    private BookingTrackingStore getBookingTrackingStore() {
        return ConfigurationEngine.getInstance(BookingTrackingStore.class);
    }

    private BookingApiManager getBookingApiManager() {
        return ConfigurationEngine.getInstance(BookingApiManager.class);
    }

    private MembershipStore getMembershipStore() {
        return ConfigurationEngine.getInstance(MembershipStore.class);
    }
}
