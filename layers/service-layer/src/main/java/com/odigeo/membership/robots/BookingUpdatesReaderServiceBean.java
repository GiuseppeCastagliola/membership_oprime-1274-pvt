package com.odigeo.membership.robots;

import com.google.inject.Inject;
import com.odigeo.membership.robots.tracker.MembershipTrackerConsumerController;
import org.I0Itec.zkclient.exception.ZkTimeoutException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BookingUpdatesReaderServiceBean implements BookingUpdatesReaderService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookingUpdatesReaderServiceBean.class);

    @Inject
    private MembershipTrackerConsumerController membershipTrackerConsumerController;

    @Override
    public boolean startTrackerConsumerController() {
        return startConsumerController(membershipTrackerConsumerController);
    }

    @Override
    public void stopTrackerConsumerController() {
        membershipTrackerConsumerController.stopMultiThreadConsumer();
    }

    private boolean startConsumerController(ConsumerController consumerController) {
        boolean success = false;
        boolean connected = false;
        try {
            while (!connected) {
                connected = startMultiThreadConsumer(consumerController);
            }
            consumerController.join();
            success = true;
        } catch (InterruptedException e) {
            LOGGER.error("Timeout for search results consumer threads", e);
        }
        return success;
    }

    private boolean startMultiThreadConsumer(ConsumerController consumerController) {
        try {
            consumerController.startMultiThreadConsumer();
        } catch (ZkTimeoutException e) {
            LOGGER.error("Timeout for search results consumer threads", e);
        }
        return true;
    }

}
