package com.odigeo.membership.robots.activator;

import com.edreams.configuration.ConfiguredInPropertiesFile;
import com.google.inject.Singleton;

/**
 * Created by octavi.ripolles on 28/08/2017.
 */
@Singleton
@ConfiguredInPropertiesFile
public class MembershipActivationProducerConfiguration {

    private String brokerList;
    private String topicName;

    public String getBrokerList() {
        return brokerList;
    }

    public void setBrokerList(String brokerList) {
        this.brokerList = brokerList;
    }

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

}
