package com.odigeo.messaging.kafka;

import com.edreams.configuration.ConfigurationEngine;
import com.google.common.annotations.VisibleForTesting;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.commons.monitoring.metrics.MetricsBuilder;
import com.odigeo.commons.monitoring.metrics.MetricsNames;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.membership.Membership;
import com.odigeo.membership.message.MembershipMailerMessage;
import com.odigeo.membership.propertiesconfig.PropertiesCacheService;
import com.odigeo.membership.robots.activator.MembershipActivationProducerConfiguration;
import com.odigeo.membership.robots.reporter.MembershipUpdateProducerConfiguration;
import com.odigeo.membership.v1.messages.MembershipUpdateMessage;
import com.odigeo.membership.v4.messages.MembershipSubscriptionMessage;
import com.odigeo.membership.v4.messages.SubscriptionStatus;
import com.odigeo.messaging.kafka.messagepublishers.WelcomeToPrimePublisher;
import com.odigeo.messaging.utils.Message;
import com.odigeo.messaging.utils.MessageDataAccessException;
import com.odigeo.messaging.utils.Publisher;
import org.apache.kafka.clients.producer.Callback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@Singleton
public class MembershipKafkaSenderBean implements MembershipKafkaSender {

    @VisibleForTesting
    static final String SUBSCRIPTION_MESSAGE_SENT_OK = "MembershipKafkaSenderBean: MembershipSubscriptionMessage sent to marketing with email = {}";
    private static final String UPDATE_MESSAGE_SENT_OK = "MembershipKafkaSenderBean: MembershipUpdateMessage sent with membershipId = {}";
    private static final String WELCOME_TO_PRIME_MESSAGE_SENT_OK = "MembershipKafkaSenderBean: WelcomeToPrime MembershipMailerMessage sent with membershipId = {}";
    private static final Logger LOGGER = LoggerFactory.getLogger(MembershipKafkaSenderBean.class);

    private final Publisher<MembershipSubscriptionMessage> kafkaMembershipSubscriptionPublisher;
    private final Publisher<MembershipUpdateMessage> kafkaMembershipUpdatePublisher;
    private final PropertiesCacheService propertiesCacheService;

    @Inject
    public MembershipKafkaSenderBean(MembershipActivationProducerConfiguration membershipActivationProducerConfiguration, MembershipUpdateProducerConfiguration membershipUpdateProducerConfiguration,
                                     MessageIntegrationServiceBean<MembershipSubscriptionMessage> subscriptionMessageIntegrationServiceBean, MessageIntegrationServiceBean<MembershipUpdateMessage> updateMessageIntegrationServiceBean,
                                     PropertiesCacheService propertiesCacheService) {
        this.kafkaMembershipSubscriptionPublisher = subscriptionMessageIntegrationServiceBean.createKafkaPublisher(membershipActivationProducerConfiguration.getBrokerList());
        this.kafkaMembershipUpdatePublisher = updateMessageIntegrationServiceBean.createKafkaPublisher(membershipUpdateProducerConfiguration.getBrokerList());
        this.propertiesCacheService = propertiesCacheService;
    }

    @Override
    public void sendMembershipSubscriptionMessageToKafka(MembershipSubscriptionMessage membershipSubscriptionMessage) throws MessageDataAccessException {
        sendMessageToKafka(kafkaMembershipSubscriptionPublisher, membershipSubscriptionMessage,
            (recordMetadata, e) -> publishMessageCallbackProcessor(SUBSCRIPTION_MESSAGE_SENT_OK, membershipSubscriptionMessage.getEmail(),
                isNull(e) ? MetricsNames.SUCCESS_SUBSCRIPTION_MSG : MetricsNames.FAILED_SUBSCRIPTION_MSG, e));
    }

    @Override
    public void sendMembershipUpdateMessageToKafka(MembershipUpdateMessage membershipUpdateMessage) throws MessageDataAccessException {
        if (propertiesCacheService.isSendingIdsToKafkaActive()) {
            sendMessageToKafka(kafkaMembershipUpdatePublisher, membershipUpdateMessage,
                (recordMetadata, e) -> publishMessageCallbackProcessor(UPDATE_MESSAGE_SENT_OK, membershipUpdateMessage.getKey(),
                    isNull(e) ? MetricsNames.SUCCESS_UPDATE_MSG : MetricsNames.FAILED_UPDATE_MSG, e));
        }
    }

    // using commons-messaging
    @Override
    public void sendMembershipMailerMessageToKafka(MembershipMailerMessage membershipMailerMessage, boolean force) {
        if (force || propertiesCacheService.isTransactionalWelcomeEmailActive()) {
            Callback welcomeToPrimeCallback = (recordMetadata, e) -> publishMessageCallbackProcessor(WELCOME_TO_PRIME_MESSAGE_SENT_OK,
                    membershipMailerMessage.getMembershipId().toString(),
                    isNull(e) ? MetricsNames.SUCCESS_WELCOME_TO_PRIME_MSG : MetricsNames.FAILED_WELCOME_TO_PRIME_MSG, e);

            ConfigurationEngine.getInstance(WelcomeToPrimePublisher.class)
                    .publishMembershipMailerMessage(membershipMailerMessage, welcomeToPrimeCallback);
        }
    }

    // using messaging-utils
    private void sendMessageToKafka(Publisher publisher, Message message, Callback callback) throws MessageDataAccessException {
        publisher.publishMessage(message, callback);
    }

    void publishMessageCallbackProcessor(String successMessage, String key, String metricName, Exception e) {
        if (nonNull(e)) {
            LOGGER.error(e.getMessage());
        } else {
            LOGGER.info(successMessage, key);
        }
        MetricsUtils.incrementCounter(MetricsBuilder.buildMetric(metricName), MetricsNames.METRICS_REGISTRY_NAME);
    }

    static MembershipSubscriptionMessage getMembershipSubscriptionMessage(
            BookingDetail bookingDetail, Membership membership, SubscriptionStatus subscriptionStatus) {
        return new MembershipSubscriptionMessage
            .Builder(bookingDetail.getBuyer().getMail(), bookingDetail.getBookingBasicInfo().getWebsite().getCode())
            .withDates(membership.getActivationDate().toLocalDate(), membership.getExpirationDate().toLocalDate())
            .withSubscriptionStatus(subscriptionStatus)
            .build();
    }
}
