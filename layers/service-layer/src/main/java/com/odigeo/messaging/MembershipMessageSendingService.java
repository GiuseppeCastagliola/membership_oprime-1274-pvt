package com.odigeo.messaging;

import com.google.common.annotations.VisibleForTesting;
import com.google.inject.Inject;
import com.odigeo.membership.message.MembershipMailerMessage;
import com.odigeo.membership.v1.messages.MembershipUpdateMessage;
import com.odigeo.membership.v4.messages.MembershipSubscriptionMessage;
import com.odigeo.messaging.kafka.MembershipKafkaSender;
import com.odigeo.messaging.utils.MessageDataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MembershipMessageSendingService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MembershipMessageSendingService.class);

    private final MembershipKafkaSender membershipKafkaSender;
    private final MembershipSubscriptionMessageService membershipSubscriptionMessageService;

    @Inject
    public MembershipMessageSendingService(MembershipKafkaSender membershipKafkaSender, MembershipSubscriptionMessageService membershipSubscriptionMessageService) {
        this.membershipKafkaSender = membershipKafkaSender;
        this.membershipSubscriptionMessageService = membershipSubscriptionMessageService;
    }

    public void sendMembershipIdToMembershipReporter(long membershipId) {
        try {
            membershipKafkaSender.sendMembershipUpdateMessageToKafka(MembershipUpdateMessage.Builder.create().membershipId(String.valueOf(membershipId)).build());
        } catch (MessageDataAccessException e) {
            LOGGER.error("Error sending ID {} to Kafka topic", membershipId);
        }
    }

    public boolean sendCompletedMembershipSubscriptionMessage(MembershipSubscriptionMessage subscriptionMessage) {
        LOGGER.info("Membership subscription Message to Kafka topic for email = {}, website = {}", subscriptionMessage.getEmail(), subscriptionMessage.getWebsite());
        try {
            membershipKafkaSender.sendMembershipSubscriptionMessageToKafka(subscriptionMessage);
            return true;
        } catch (MessageDataAccessException e) {
            LOGGER.error("Error - Membership subscription Message to Kafka topic for email = {}, website = {}", subscriptionMessage.getEmail(), subscriptionMessage.getWebsite());
            return false;
        }
    }

    @VisibleForTesting
    void sendMembershipMailerMessage(MembershipMailerMessage membershipMailerMessage, boolean force) {
        membershipKafkaSender.sendMembershipMailerMessageToKafka(membershipMailerMessage, force);
    }

    public boolean sendMembershipSubscriptionMessage(MembershipSubscriptionMessage subscriptionMessage) {
        MembershipSubscriptionMessage completedMessage = membershipSubscriptionMessageService.completeSubscriptionMessage(subscriptionMessage);
        return sendCompletedMembershipSubscriptionMessage(completedMessage);
    }
}
