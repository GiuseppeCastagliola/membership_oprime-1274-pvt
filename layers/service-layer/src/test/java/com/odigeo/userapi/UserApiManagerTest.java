package com.odigeo.userapi;

import com.odigeo.membership.exception.DataNotFoundException;
import com.odigeo.userprofiles.api.v1.UserServiceInternalManager;
import com.odigeo.userprofiles.api.v1.model.Brand;
import com.odigeo.userprofiles.api.v1.model.HashCode;
import com.odigeo.userprofiles.api.v1.model.HashType;
import com.odigeo.userprofiles.api.v1.model.InternalServerException;
import com.odigeo.userprofiles.api.v1.model.InvalidCredentialsException;
import com.odigeo.userprofiles.api.v1.model.InvalidFindException;
import com.odigeo.userprofiles.api.v1.model.InvalidValidationException;
import com.odigeo.userprofiles.api.v1.model.RemoteException;
import com.odigeo.userprofiles.api.v1.model.UserBasicInfo;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Optional;

import static com.odigeo.userprofiles.api.v1.model.Status.ACTIVE;
import static com.odigeo.userprofiles.api.v1.model.Status.PENDING_LOGIN;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class UserApiManagerTest {

    private static final List<String> TOKENS = asList("firstToken", "secondToken");
    private static final Long USER_ID = 8373L;
    private static final String EMAIL = "test@test.com";
    private static final String INVALID_WEBSITE = "ed.es";
    private static final String WEBSITE_ES = "ES";
    private static final Brand BRAND = Brand.ED;

    @Mock
    private UserServiceInternalManager userServiceInternalManagerMock;

    private UserApiManager userApiManager;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        userApiManager = new UserApiManager(userServiceInternalManagerMock);
    }

    @Test
    public void testForgetPasswordTokenReturnsValidToken() throws RemoteException, InternalServerException, InvalidCredentialsException, InvalidFindException {
        //Given
        given(userServiceInternalManagerMock.getHashCodes(USER_ID, HashType.REQUEST_PASSWORD)).willReturn(sampleHashCodes());
        //When
        Optional<String> forgetPasswordToken = userApiManager.getForgetPasswordToken(USER_ID);
        //Then
        assertTrue(forgetPasswordToken.isPresent());
        assertTrue(TOKENS.contains(forgetPasswordToken.get()));
    }

    @Test
    public void testForgetPasswordTokenReturnsNoToken() throws RemoteException, InternalServerException, InvalidCredentialsException, InvalidFindException {
        //Given
        given(userServiceInternalManagerMock.getHashCodes(USER_ID, HashType.REQUEST_PASSWORD)).willReturn(emptyList());
        //When
        Optional<String> forgetPasswordToken = userApiManager.getForgetPasswordToken(USER_ID);
        //Then
        assertFalse(forgetPasswordToken.isPresent());
    }

    @Test
    public void testForgetPasswordTokenReturnsNoTokenWhenException() throws RemoteException, InternalServerException, InvalidCredentialsException, InvalidFindException {
        //Given
        given(userServiceInternalManagerMock.getHashCodes(USER_ID, HashType.REQUEST_PASSWORD)).willThrow(new InternalServerException("expected exception"));
        //When
        Optional<String> forgetPasswordToken = userApiManager.getForgetPasswordToken(USER_ID);
        //Then
        assertFalse(forgetPasswordToken.isPresent());
    }

    @Test
    public void testGetUserInfoHasDefaultValuesWhenIncorrectWebsite() {
        //When
        UserInfo userInfo = userApiManager.getUserInfo(EMAIL, INVALID_WEBSITE);
        //Then
        assertEquals(userInfo.getUserId(), Optional.empty());
        assertFalse(userInfo.shouldSetPassword());
    }

    @Test
    public void testGetUserInfoHasDefaultValuesWhenException() throws InvalidValidationException, InternalServerException, RemoteException, InvalidCredentialsException, InvalidFindException {
        //Given
        given(userServiceInternalManagerMock.getUserBasicInfo(EMAIL, BRAND)).willThrow(new InternalServerException("expected exception"));
        //When
        UserInfo userInfo = userApiManager.getUserInfo(EMAIL, WEBSITE_ES);
        //Then
        assertEquals(userInfo.getUserId(), Optional.empty());
        assertFalse(userInfo.shouldSetPassword());
    }

    @Test
    public void testGetUserInfoHasDefaultValuesWhenNullUserBasicInfo() throws InvalidValidationException, InternalServerException, RemoteException, InvalidCredentialsException, InvalidFindException {
        //Given
        given(userServiceInternalManagerMock.getUserBasicInfo(EMAIL, BRAND)).willReturn(null);
        //When
        UserInfo userInfo = userApiManager.getUserInfo(EMAIL, WEBSITE_ES);
        //Then
        assertEquals(userInfo.getUserId(), Optional.empty());
        assertFalse(userInfo.shouldSetPassword());
    }

    @Test
    public void testGetUserInfoHasUserBasicInfoValuesWithPendingLogin() throws InvalidValidationException, InternalServerException, RemoteException, InvalidCredentialsException, InvalidFindException {
        //Given
        given(userServiceInternalManagerMock.getUserBasicInfo(EMAIL, BRAND)).willReturn(sampleUserBasicInfo(true));
        //When
        UserInfo userInfo = userApiManager.getUserInfo(EMAIL, WEBSITE_ES);
        //Then
        assertTrue(userInfo.getUserId().isPresent());
        assertEquals(userInfo.getUserId().get(), USER_ID);
        assertTrue(userInfo.shouldSetPassword());
    }

    @Test
    public void testGetUserInfoHasUserBasicInfoValuesWithNoPendingLogin() throws InvalidValidationException, InternalServerException, RemoteException, InvalidCredentialsException, InvalidFindException {
        //Given
        given(userServiceInternalManagerMock.getUserBasicInfo(EMAIL, BRAND)).willReturn(sampleUserBasicInfo(false));
        //When
        UserInfo userInfo = userApiManager.getUserInfo(EMAIL, WEBSITE_ES);
        //Then
        assertTrue(userInfo.getUserId().isPresent());
        assertEquals(userInfo.getUserId().get(), USER_ID);
        assertFalse(userInfo.shouldSetPassword());
    }

    @Test
    public void testGetUserInfoByUserId() throws InternalServerException, RemoteException, InvalidCredentialsException, InvalidFindException {
        //Given
        given(userServiceInternalManagerMock.getUserBasicInfo(USER_ID)).willReturn(sampleUserBasicInfo(false));
        //When
        UserInfo userInfo = userApiManager.getUserInfo(USER_ID);
        //Then
        assertEquals(userInfo.getEmail(), EMAIL);
    }

    @Test
    public void testGetUserInfoByUserIdThrowsException() throws InternalServerException, RemoteException, InvalidCredentialsException, InvalidFindException {
        //Given
        given(userServiceInternalManagerMock.getUserBasicInfo(USER_ID)).willThrow(new InternalServerException("expected exception"));
        //When
        UserInfo userInfo = userApiManager.getUserInfo(USER_ID);
        //Then
        assertNull(userInfo.getEmail());
    }

    @Test
    public void testGetEmail() throws RemoteException, InternalServerException, InvalidCredentialsException, InvalidFindException, DataNotFoundException {
        given(userServiceInternalManagerMock.getUserBasicInfo(USER_ID)).willReturn(sampleUserBasicInfo(false));
        //When
        String email = userApiManager.getEmail(USER_ID);
        //Then
        assertEquals(email, EMAIL);
    }

    @Test(expectedExceptions = DataNotFoundException.class)
    public void testGetEmail_nullEmail() throws RemoteException, InternalServerException, InvalidCredentialsException, InvalidFindException, DataNotFoundException {
        given(userServiceInternalManagerMock.getUserBasicInfo(USER_ID)).willReturn(new UserBasicInfo());

        //When
        userApiManager.getEmail(USER_ID);
    }

    private UserBasicInfo sampleUserBasicInfo(boolean isPendingLogin) {
        UserBasicInfo userBasicInfo = new UserBasicInfo();
        userBasicInfo.setId(USER_ID);
        userBasicInfo.setEmail(EMAIL);
        userBasicInfo.setStatus(isPendingLogin ? PENDING_LOGIN : ACTIVE);
        return userBasicInfo;
    }

    private List<HashCode> sampleHashCodes() {
        HashCode firstHashCode = new HashCode();
        firstHashCode.setCode(TOKENS.get(0));
        HashCode secondHashCode = new HashCode();
        secondHashCode.setCode(TOKENS.get(1));
        return asList(firstHashCode, secondHashCode);
    }
}