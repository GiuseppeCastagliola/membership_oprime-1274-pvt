package com.odigeo.membership.tracking;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.membership.Membership;
import com.odigeo.membership.booking.BookingDetailWrapper;
import com.odigeo.membership.member.MembershipValidationService;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.assertFalse;

public class BookingTrackingFactoryTest {

    private static final String CUTOVER_DATE = "2019-03-31";
    private static final String DATE_PATTERN = "yyyy-MM-dd";

    @Mock
    private Membership membershipMock;
    @Mock
    private BookingDetail bookingDetail;
    @Mock
    private MembershipValidationService membershipValidationService;

    private BookingDetailWrapper bookingDetailWrapper;
    private BookingTrackingFactory bookingTrackingFactory;

    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        ConfigurationEngine.init(this::configure);
        bookingTrackingFactory = new BookingTrackingFactory();
        bookingDetailWrapper = new BookingDetailWrapper(bookingDetail);
        when(membershipValidationService.getNewVatModelDate()).thenReturn(CUTOVER_DATE);
    }

    private void configure(Binder binder) {
        binder.bind(MembershipValidationService.class).toInstance(membershipValidationService);
    }

    @Test
    public void testCreateNewVatModelTracking() {
        mockMembershipAfterCutover();
        AbstractBookingTracking bookingTracking = bookingTrackingFactory
            .create(bookingDetailWrapper, membershipMock);
        assertTrue(bookingTracking instanceof BookingTrackingNewVatModel);
    }

    @Test
    public void testCreateOldModelTracking() {
        mockMembershipBeforeCutover();
        AbstractBookingTracking bookingTracking = bookingTrackingFactory
            .create(bookingDetailWrapper, membershipMock);
        assertTrue(bookingTracking instanceof BookingTrackingOldModel);
    }

    @Test
    public void testApplyNewVatModel() {
        mockMembershipAfterCutover();
        assertTrue(bookingTrackingFactory.applyNewVatModel(membershipMock));
        mockMembershipBeforeCutover();
        assertFalse(bookingTrackingFactory.applyNewVatModel(membershipMock));
        when(membershipMock.getActivationDate()).thenReturn(null);
        assertFalse(bookingTrackingFactory.applyNewVatModel(membershipMock));
    }

    private void mockMembershipAfterCutover() {
        LocalDateTime activationDateAfterCutover = LocalDate.parse(CUTOVER_DATE,
            DateTimeFormatter.ofPattern(DATE_PATTERN)).plusDays(1).atStartOfDay();
        when(membershipMock.getActivationDate()).thenReturn(activationDateAfterCutover);
    }

    private void mockMembershipBeforeCutover() {
        LocalDateTime activationDateBeforeCutover = LocalDate.parse(CUTOVER_DATE,
            DateTimeFormatter.ofPattern(DATE_PATTERN)).atStartOfDay();
        when(membershipMock.getActivationDate()).thenReturn(activationDateBeforeCutover);
    }
}