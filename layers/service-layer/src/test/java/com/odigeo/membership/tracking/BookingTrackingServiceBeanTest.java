package com.odigeo.membership.tracking;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.bookingapi.BookingApiDAO;
import com.odigeo.bookingapi.BookingApiManager;
import com.odigeo.bookingapi.mock.v12.response.BookingBasicInfoBuilder;
import com.odigeo.bookingapi.mock.v12.response.BookingDetailBuilder;
import com.odigeo.bookingapi.mock.v12.response.BuilderException;
import com.odigeo.bookingapi.mock.v12.response.FeeBuilder;
import com.odigeo.bookingapi.mock.v12.response.FeeContainerBuilder;
import com.odigeo.bookingapi.mock.v12.response.MembershipPerksBuilder;
import com.odigeo.bookingapi.mock.v12.response.WebsiteBuilder;
import com.odigeo.bookingapi.v12.requests.UpdateBookingRequest;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.bookingapi.v12.responses.Fee;
import com.odigeo.bookingapi.v12.responses.FeeContainer;
import com.odigeo.bookingsearchapi.v1.responses.BookingSummary;
import com.odigeo.membership.AutoRenewalOperation;
import com.odigeo.membership.BookingTracking;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.booking.BookingDetailWrapper;
import com.odigeo.membership.booking.MembershipVatModelFee;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.exception.DataNotFoundException;
import com.odigeo.membership.exception.RetryableException;
import com.odigeo.membership.exception.bookingapi.BookingApiException;
import com.odigeo.membership.member.MembershipStore;
import com.odigeo.membership.member.MembershipValidationService;
import org.apache.commons.lang.StringUtils;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.verification.VerificationMode;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Objects;
import java.util.Random;

import static com.odigeo.membership.booking.MembershipVatModelFee.AVOID_FEES;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class BookingTrackingServiceBeanTest {

    private static final String SITE_ES = "ES";
    private static final String BRAND_ED = "ED";
    private static final long BOOKING_ID = 123L;
    private static final long MEMBERSHIP_ID = 101L;
    private static final long FEE_CONTAINER_ID = 999L;
    private static final Random RANDOM = new Random();
    private static final String CONTRACT = "CONTRACT";
    private static final String CURRENCY_GBP = "GBP";
    private static final String CUTOVER_DATE = "2019-03-31";
    private static final String DATE_PATTERN = "yyyy-MM-dd";
    private static final SQLException SQL_EXCEPTION = new SQLException();
    private static final LocalDateTime activationDateBeforeCutover = LocalDate.parse(CUTOVER_DATE,
            DateTimeFormatter.ofPattern(DATE_PATTERN)).atStartOfDay();
    private static final LocalDateTime activationDateAfterCutover = LocalDate.parse(CUTOVER_DATE,
            DateTimeFormatter.ofPattern(DATE_PATTERN)).plusDays(1).atStartOfDay();
    private static final VerificationMode TWICE = times(2);

    @Mock
    private DataSource dataSource;
    @Mock
    private Connection connection;
    @Mock
    private Membership membershipMock;
    @Mock
    private MembershipStore membershipStore;
    @Mock
    private BookingApiManager bookingApiManager;
    @Mock
    private BookingTrackingStore bookingTrackingStore;
    @Mock
    private MembershipValidationService membershipValidationService;
    @Mock
    private BookingApiDAO bookingApiDao;
    @Captor
    private ArgumentCaptor<BookingTracking> bookingTrackingArgumentCaptor;
    @Captor
    private ArgumentCaptor<BigDecimal> balanceArgumentCaptor;
    @Captor
    private ArgumentCaptor<UpdateBookingRequest> updateBookingArgumentCaptor;

    @InjectMocks
    private BookingTrackingServiceBean bookingTrackingServiceBean = new BookingTrackingServiceBean();

    private BookingDetail bookingDetail;
    private BookingDetailWrapper bookingDetailWrapper;
    private BookingTrackingFactory bookingTrackingFactory;


    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        bookingDetail = buildBookingDetail();
        bookingDetailWrapper = new BookingDetailWrapper(bookingDetail);
        bookingTrackingFactory = new BookingTrackingFactory();
        ConfigurationEngine.init(this::configure);
        setupMocks();
    }

    private void configure(Binder binder) {
        binder.bind(BookingTrackingStore.class).toInstance(bookingTrackingStore);
        binder.bind(MembershipValidationService.class).toInstance(membershipValidationService);
        binder.bind(BookingApiManager.class).toInstance(bookingApiManager);
        binder.bind(MembershipStore.class).toInstance(membershipStore);
        binder.bind(BookingApiDAO.class).toInstance(bookingApiDao);
    }

    private void setupMocks() throws SQLException {
        when(dataSource.getConnection()).thenReturn(connection);
        when(membershipValidationService.getNewVatModelDate()).thenReturn(CUTOVER_DATE);
        setUpMembershipMock();
    }

    private void setUpMembershipMock() {
        when(membershipMock.getId()).thenReturn(MEMBERSHIP_ID);
        when(membershipMock.getBalance()).thenReturn(BigDecimal.TEN);
        when(membershipMock.getStatus()).thenReturn(MemberStatus.ACTIVATED);
        when(membershipMock.getAutoRenewal()).thenReturn(MembershipRenewal.ENABLED);
        when(membershipMock.getActivationDate()).thenReturn(LocalDateTime.now());
        when(membershipMock.getExpirationDate()).thenReturn(LocalDateTime.now().plusYears(1L));
        when(membershipMock.getWebsite()).thenReturn(SITE_ES);
    }

    @Test
    public void testGetBookingTrackerMemberActivatedBeforeCutover() {
        // Given
        when(membershipMock.getActivationDate()).thenReturn(activationDateBeforeCutover);
        // When
        AbstractBookingTracking bookingTracker = bookingTrackingFactory.create(bookingDetailWrapper, membershipMock);
        // Then
        assertTrue(bookingTracker instanceof BookingTrackingOldModel);
    }

    @Test
    public void testGetBookingTrackerMemberActivatedAfterCutover() {
        // Given
        when(membershipMock.getActivationDate()).thenReturn(activationDateAfterCutover);
        // When
        AbstractBookingTracking bookingTracker = bookingTrackingFactory.create(bookingDetailWrapper, membershipMock);
        // Then
        assertTrue(bookingTracker instanceof BookingTrackingNewVatModel);
    }

    @Test(expectedExceptions = RetryableException.class)
    public void testProcessBookingSqlException()
            throws DataAccessException, SQLException, MissingElementException, RetryableException, BookingApiException {
        // Given
        when(bookingApiManager.getBooking(eq(BOOKING_ID))).thenReturn(bookingDetail);
        when(membershipStore.fetchMembersByIdBlocking(eq(dataSource), eq(MEMBERSHIP_ID))).thenThrow(new SQLException());
        // When
        bookingTrackingServiceBean.processBooking(BOOKING_ID);
    }

    @Test
    public void testProcessBookingNewVatModel()
            throws DataAccessException, MissingElementException, SQLException, RetryableException, BookingApiException {
        // Given
        mockBookingApiCallAndFetchMember(bookingDetail, membershipMock);
        when(membershipMock.getActivationDate()).thenReturn(activationDateAfterCutover);
        // When
        bookingTrackingServiceBean.processBooking(BOOKING_ID);
        // Then
        verify(bookingTrackingStore).trackBooking(eq(dataSource), any(BookingTracking.class));
        verify(bookingTrackingStore, never()).removeTrackedBooking(eq(dataSource), anyLong());
    }

    @Test
    public void testProcessBookingCurrencyFromBooking() throws DataAccessException, SQLException, BookingApiException, BuilderException, MissingElementException, RetryableException {
        // Given
        BookingDetail booking = buildBookingDetail();
        booking.setCurrencyCode(CURRENCY_GBP);
        when(membershipMock.getActivationDate()).thenReturn(activationDateAfterCutover);
        when(membershipMock.getMembershipType()).thenReturn(MembershipType.BASIC_FREE);
        when(membershipMock.getAutoRenewal()).thenReturn(MembershipRenewal.DISABLED);

        mockBookingApiCallAndFetchMember(booking, membershipMock);
        when(bookingApiManager.updateBooking(anyLong(), any(UpdateBookingRequest.class))).thenReturn(new BookingDetail());
        when(membershipStore.updateAutoRenewal(any(DataSource.class), eq(MEMBERSHIP_ID), eq(AutoRenewalOperation.ENABLE_AUTO_RENEW))).thenReturn(true);
        // When
        bookingTrackingServiceBean.processBooking(BOOKING_ID);
        // Then
        verify(bookingApiManager).updateBooking(eq(BOOKING_ID), updateBookingArgumentCaptor.capture());
        assertEquals(updateBookingArgumentCaptor.getValue().getCreateBookingProductCategoryList().get(0).getProductCategoryFees().get(0).getCurrency(), CURRENCY_GBP);
    }

    @Test
    public void testProcessBookingAlreadyTracked()
            throws DataAccessException, MissingElementException, SQLException, RetryableException, BookingApiException {
        // Given
        mockBookingApiCallAndFetchMember(bookingDetail, membershipMock);
        when(bookingTrackingStore.isBookingTracked(eq(dataSource), eq(BOOKING_ID))).thenReturn(true);
        when(bookingTrackingStore.getBookingTracked(eq(dataSource), eq(BOOKING_ID))).thenReturn(createBookingTracking(BigDecimal.TEN, BigDecimal.ONE));
        // When
        bookingTrackingServiceBean.processBooking(BOOKING_ID);
        // Then
        verify(bookingTrackingStore).trackBooking(eq(dataSource), any(BookingTracking.class));
        verify(bookingTrackingStore).removeTrackedBooking(eq(dataSource), anyLong());
    }

    @Test
    public void testProcessBookingAlreadyTrackedNotContractNoExpDate()
            throws DataAccessException, MissingElementException, SQLException, RetryableException, BookingApiException {
        // Given
        bookingDetail.setBookingStatus(StringUtils.EMPTY);
        when(membershipMock.getExpirationDate()).thenReturn(null);
        mockBookingApiCallAndFetchMember(bookingDetail, membershipMock);
        when(bookingTrackingStore.isBookingTracked(eq(dataSource), eq(BOOKING_ID))).thenReturn(true);
        when(bookingTrackingStore.getBookingTracked(eq(dataSource), eq(BOOKING_ID))).thenReturn(createBookingTracking(BigDecimal.TEN, BigDecimal.ONE));
        // When
        bookingTrackingServiceBean.processBooking(BOOKING_ID);
        // Then
        verify(bookingTrackingStore, never()).trackBooking(eq(dataSource), any(BookingTracking.class));
        verify(bookingTrackingStore).removeTrackedBooking(eq(dataSource), anyLong());
    }

    @Test(expectedExceptions = RetryableException.class)
    public void testProcessBookingBookingApiException()
            throws MissingElementException, RetryableException, BookingApiException {
        // Given
        when(bookingApiManager.getBooking(eq(BOOKING_ID))).thenThrow(new BookingApiException(StringUtils.EMPTY));
        // When
        bookingTrackingServiceBean.processBooking(BOOKING_ID);
    }

    @Test(expectedExceptions = MissingElementException.class)
    public void testProcessBookingNotFoundMembership()
            throws DataAccessException, MissingElementException, RetryableException, SQLException, BookingApiException {
        // Given
        mockBookingApiCallAndFetchMember(bookingDetail, null);
        // When
        bookingTrackingServiceBean.processBooking(BOOKING_ID);
    }

    @Test(expectedExceptions = RetryableException.class)
    public void testBookingTrackingException() throws Exception {
        // Given
        mockBookingApiCallAndFetchMember(bookingDetail, membershipMock);
        when(bookingTrackingStore.isBookingTracked(eq(dataSource), eq(BOOKING_ID))).thenReturn(Boolean.FALSE);
        when(bookingTrackingStore.getBookingTracked(eq(dataSource), eq(BOOKING_ID))).thenThrow(SQL_EXCEPTION);
        // When
        bookingTrackingServiceBean.processBooking(BOOKING_ID);
    }

    @Test(expectedExceptions = RetryableException.class)
    public void testRemoveBookingTrackingNewModelException() throws Exception {
        // Given
        when(membershipMock.getActivationDate()).thenReturn(activationDateAfterCutover);
        mockBookingApiCallAndFetchMember(bookingDetail, membershipMock);
        bookingDetail.setBookingStatus(StringUtils.EMPTY);
        when(bookingTrackingStore.isBookingTracked(eq(dataSource), eq(BOOKING_ID))).thenReturn(Boolean.TRUE);
        when(bookingTrackingStore.removeTrackedBooking(eq(dataSource), eq(BOOKING_ID))).thenThrow(SQL_EXCEPTION);
        when(bookingTrackingStore.getBookingTracked(eq(dataSource), eq(BOOKING_ID))).thenReturn(createBookingTracking(BigDecimal.TEN, BigDecimal.ZERO));
        // When
        bookingTrackingServiceBean.processBooking(BOOKING_ID);
    }

    @Test(expectedExceptions = RetryableException.class)
    public void testProcessBookingNotBookingFound()
            throws MissingElementException, RetryableException, BookingApiException {
        // Given
        when(bookingApiManager.getBooking(anyLong())).thenReturn(null);
        // When
        bookingTrackingServiceBean.processBooking(BOOKING_ID);
    }

    @Test(expectedExceptions = RetryableException.class)
    public void testProcessBookingIsBookingTrackedException() throws Exception {
        // Given
        setExceptionsTrackingStore();
        mockBookingApiCallAndFetchMember(bookingDetail, membershipMock);
        configureMembershipMock(BigDecimal.TEN);
        // When
        bookingTrackingServiceBean.processBooking(BOOKING_ID);
    }

    private void mockBookingApiCallAndFetchMember(BookingDetail expectedBookingDetail, Membership expectedMembership)
            throws DataAccessException, SQLException, BookingApiException {
        when(bookingApiManager.getBooking(eq(BOOKING_ID))).thenReturn(expectedBookingDetail);
        mockFetchMembership(expectedMembership);
    }

    private void mockFetchMembership(Membership expectedMembership) throws SQLException, DataNotFoundException {
        when(membershipStore.fetchMembersByIdBlocking(eq(dataSource), eq(MEMBERSHIP_ID))).thenReturn(expectedMembership);
        when(membershipStore.fetchMembershipById(eq(dataSource), eq(MEMBERSHIP_ID))).thenReturn(expectedMembership);
    }

    @Test
    public void testTrackBookingAlreadyTrackedAvoidFeeAmountChanged()
            throws SQLException, BuilderException, RetryableException, MissingElementException, DataAccessException,
            BookingApiException {
        configureMembershipMock(new BigDecimal("18.65"), activationDateAfterCutover);
        BigDecimal avoidFeeAmount = new BigDecimal("-15.15");
        when(bookingTrackingStore.getBookingTracked(eq(dataSource), eq(BOOKING_ID))).thenReturn(createBookingTracking(BigDecimal.TEN, BigDecimal.ZERO));
        testTrackBookingAlreadyTrackedFeeChanged(avoidFeeAmount);
    }

    @Test
    public void testTrackBookingAlreadyTrackedPerksAmountChanged()
            throws SQLException, BuilderException, RetryableException, MissingElementException, DataAccessException,
            BookingApiException {
        configureMembershipMock(BigDecimal.ONE, activationDateAfterCutover);
        BigDecimal avoidFeeAmount = BigDecimal.ONE;
        BookingTracking bookingTracking = createBookingTracking(avoidFeeAmount, BigDecimal.ZERO);
        bookingTracking.setPerksAmount(BigDecimal.TEN);
        when(bookingTrackingStore.getBookingTracked(eq(dataSource), eq(BOOKING_ID))).thenReturn(bookingTracking);
        testTrackBookingAlreadyTrackedFeeChanged(avoidFeeAmount);
    }

    private BookingTracking createBookingTracking(BigDecimal avoidFeeAmount, BigDecimal costFeeAmount) {
        BookingTracking bookingTracking = new BookingTracking();
        bookingTracking.setMembershipId(MEMBERSHIP_ID);
        bookingTracking.setBookingId(BOOKING_ID);
        bookingTracking.setCostFeeAmount(costFeeAmount);
        bookingTracking.setAvoidFeeAmount(avoidFeeAmount);
        bookingTracking.setPerksAmount(BigDecimal.ZERO);
        return bookingTracking;
    }

    private void configureMembershipMock(BigDecimal balance) {
        configureMembershipMock(balance, null);
    }

    private void configureMembershipMock(BigDecimal balance, LocalDateTime activationDate) {
        when(membershipMock.getBalance()).thenReturn(balance);
        if (Objects.nonNull(activationDate)) {
            when(membershipMock.getActivationDate()).thenReturn(activationDate);
        }
    }

    private void testTrackBookingAlreadyTrackedFeeChanged(BigDecimal avoidFeeAmount)
            throws SQLException, RetryableException, BuilderException, MissingElementException, DataAccessException,
            BookingApiException {
        when(bookingTrackingStore.removeTrackedBooking(eq(dataSource), eq(BOOKING_ID))).thenReturn(true);
        bookingDetail.setAllFeeContainers(
                Collections.singletonList(buildMembershipFeeContainer(buildFee(AVOID_FEES, avoidFeeAmount))));
        when(bookingApiManager.getBooking(BOOKING_ID)).thenReturn(bookingDetail);
        when(membershipStore.fetchMembersByIdBlocking(eq(dataSource), eq(MEMBERSHIP_ID))).thenReturn(membershipMock);
        bookingTrackingServiceBean.processBooking(BOOKING_ID);
        verifyDeleteAndTrackBalancedBooking(avoidFeeAmount, BigDecimal.ZERO);
    }

    private void verifyDeleteAndTrackBalancedBooking(BigDecimal avoidFeeAmount, BigDecimal costFeeAmount) throws SQLException, DataAccessException {
        verify(bookingTrackingStore, atLeastOnce()).getBookingTracked(dataSource, BOOKING_ID);
        verify(bookingTrackingStore).trackBooking(eq(dataSource), bookingTrackingArgumentCaptor.capture());
        verify(bookingApiManager, never()).updateBooking(anyLong(), any(UpdateBookingRequest.class));
        verify(membershipStore, TWICE).updateMembershipBalance(eq(dataSource), eq(MEMBERSHIP_ID), balanceArgumentCaptor.capture());
        BookingTracking bookingTracking = bookingTrackingArgumentCaptor.getValue();
        assertEquals(bookingTracking.getCostFeeAmount(), costFeeAmount);
        assertEquals(bookingTracking.getAvoidFeeAmount(), avoidFeeAmount);
        assertEquals(membershipMock.getBalance().add(avoidFeeAmount), balanceArgumentCaptor.getValue());
        verify(bookingTrackingStore).removeTrackedBooking(eq(dataSource), eq(BOOKING_ID));
    }

    private Fee buildFee(MembershipVatModelFee fee, BigDecimal amount) throws BuilderException {
        return new FeeBuilder().amount(amount).subCode(fee.getFeeCode()).build(RANDOM);
    }

    private FeeContainer buildMembershipFeeContainer(Fee... fees) {
        FeeContainer feeContainer = new FeeContainer();
        feeContainer.setId(FEE_CONTAINER_ID);
        feeContainer.setFees(Arrays.asList(fees.clone()));
        return feeContainer;
    }

    @Test
    public void testIsBookingLimitReachedTrue() throws DataAccessException, SQLException {
        setIsBookingLimitReached(true);
        assertTrue(bookingTrackingServiceBean.isBookingLimitReached(membershipMock.getId()));
        verify(bookingTrackingStore).isBookingLimitReached(dataSource, membershipMock.getId());
    }

    @Test
    public void testIsBookingLimitReachedFalse() throws DataAccessException, SQLException {
        setIsBookingLimitReached(false);
        assertFalse(bookingTrackingServiceBean.isBookingLimitReached(membershipMock.getId()));
        verify(bookingTrackingStore).isBookingLimitReached(dataSource, membershipMock.getId());
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testIsBookingLimitReachedException() throws Exception {
        setExceptionsTrackingStore();
        bookingTrackingServiceBean.isBookingLimitReached(membershipMock.getId());
    }

    private void setIsBookingLimitReached(final boolean limitReached) throws SQLException {
        when(bookingTrackingStore.isBookingLimitReached(any(), anyLong())).thenReturn(limitReached);
    }

    private void setExceptionsTrackingStore() throws SQLException {
        when(bookingTrackingStore.trackBooking(any(DataSource.class), any(BookingTracking.class))).thenThrow(SQL_EXCEPTION);
        when(bookingTrackingStore.removeTrackedBooking(any(DataSource.class), anyLong())).thenThrow(SQL_EXCEPTION);
        when(bookingTrackingStore.getBookingTracked(any(DataSource.class), anyLong())).thenThrow(SQL_EXCEPTION);
        when(bookingTrackingStore.isBookingTracked(any(DataSource.class), anyLong())).thenThrow(SQL_EXCEPTION);
        when(bookingTrackingStore.isBookingLimitReached(any(DataSource.class), anyLong())).thenThrow(SQL_EXCEPTION);
    }

    @Test(expectedExceptions = MissingElementException.class)
    public void testGetActivatedMembershipBlockingNullMembership()
            throws DataNotFoundException, MissingElementException, SQLException, RetryableException {
        // Given
        when(membershipStore.fetchMembersByIdBlocking(eq(dataSource), eq(MEMBERSHIP_ID))).thenReturn(null);

        // When
        bookingTrackingServiceBean.getActivatedMembershipBlocking(bookingDetailWrapper);
    }

    @Test(expectedExceptions = RetryableException.class)
    public void testGetActivatedMembershipBlockingMemberPending()
            throws DataNotFoundException, MissingElementException, SQLException, RetryableException {
        // Given
        when(membershipMock.getStatus()).thenReturn(MemberStatus.PENDING_TO_ACTIVATE);
        when(membershipStore.fetchMembersByIdBlocking(eq(dataSource), eq(MEMBERSHIP_ID))).thenReturn(membershipMock);

        // When
        bookingTrackingServiceBean.getActivatedMembershipBlocking(bookingDetailWrapper);
    }

    @Test
    public void testGetActivatedMembershipBlocking()
            throws DataNotFoundException, MissingElementException, SQLException, RetryableException {
        // Given
        when(membershipMock.getId()).thenReturn(MEMBERSHIP_ID);
        when(membershipMock.getStatus()).thenReturn(MemberStatus.ACTIVATED);
        when(membershipStore.fetchMembersByIdBlocking(eq(dataSource), eq(MEMBERSHIP_ID))).thenReturn(membershipMock);

        // When
        Membership membership = bookingTrackingServiceBean.getActivatedMembershipBlocking(bookingDetailWrapper);

        // Then
        assertNotNull(membership);
        assertEquals(membership.getId(), Long.valueOf(MEMBERSHIP_ID));
    }

    @Test
    public void testBasicFreeMembershipBookingUpdate() throws MissingElementException, RetryableException, DataAccessException, SQLException, BookingApiException {
        mockBookingApiCallAndFetchMember(bookingDetail, membershipMock);
        when(membershipMock.getMembershipType()).thenReturn(MembershipType.BASIC_FREE);
        when(membershipMock.getAutoRenewal()).thenReturn(MembershipRenewal.DISABLED);
        when(bookingApiDao.searchBookingsByMembershipId(eq(membershipMock.getId()))).thenReturn(Collections.emptyList());
        when(bookingApiManager.updateBooking(eq(BOOKING_ID), any(UpdateBookingRequest.class))).thenReturn(bookingDetail);
        bookingTrackingServiceBean.processBooking(BOOKING_ID);
        verifyZeroInteractions(bookingTrackingStore);
        verify(bookingApiManager).updateBooking(eq(BOOKING_ID), any(UpdateBookingRequest.class));
    }

    @Test
    public void testBasicFreeMembershipBookingAlreadyUpdated() throws MissingElementException, RetryableException, DataAccessException, SQLException, BookingApiException {
        mockBookingApiCallAndFetchMember(bookingDetail, membershipMock);
        when(membershipMock.getMembershipType()).thenReturn(MembershipType.BASIC_FREE);
        when(membershipMock.getAutoRenewal()).thenReturn(MembershipRenewal.DISABLED);
        when(bookingApiDao.searchPrimeBookingsByMembershipId(eq(membershipMock.getId()))).thenReturn(Collections.singletonList(new BookingSummary()));
        bookingTrackingServiceBean.processBooking(BOOKING_ID);
        verify(bookingTrackingStore).isBookingTracked(eq(dataSource), eq(BOOKING_ID));
        verify(bookingApiManager, never()).updateBooking(eq(BOOKING_ID), any(UpdateBookingRequest.class));
    }

    @Test
    public void testBasicFreeMembershipBookingNotContract() throws MissingElementException, RetryableException, DataAccessException, SQLException, BookingApiException {
        mockBookingApiCallAndFetchMember(bookingDetail, membershipMock);
        bookingDetail.setBookingStatus("INIT");
        when(membershipMock.getMembershipType()).thenReturn(MembershipType.BASIC_FREE);
        when(membershipMock.getAutoRenewal()).thenReturn(MembershipRenewal.DISABLED);
        when(bookingApiDao.searchPrimeBookingsByMembershipId(eq(membershipMock.getId()))).thenReturn(Collections.emptyList());
        bookingTrackingServiceBean.processBooking(BOOKING_ID);
        verify(bookingTrackingStore).isBookingTracked(eq(dataSource), eq(BOOKING_ID));
        verify(bookingApiManager, never()).updateBooking(eq(BOOKING_ID), any(UpdateBookingRequest.class));
    }

    @Test
    public void testBasicFreeMembershipAlreadyRenewalEnabled() throws MissingElementException, RetryableException, DataAccessException, SQLException, BookingApiException {
        mockBookingApiCallAndFetchMember(bookingDetail, membershipMock);
        when(membershipMock.getMembershipType()).thenReturn(MembershipType.BASIC_FREE);
        when(membershipMock.getAutoRenewal()).thenReturn(MembershipRenewal.ENABLED);
        when(bookingApiDao.searchPrimeBookingsByMembershipId(eq(membershipMock.getId()))).thenReturn(Collections.emptyList());
        bookingTrackingServiceBean.processBooking(BOOKING_ID);
        verify(bookingTrackingStore).isBookingTracked(eq(dataSource), eq(BOOKING_ID));
        verify(bookingApiManager, never()).updateBooking(eq(BOOKING_ID), any(UpdateBookingRequest.class));
    }


    @Test
    public void testBasicFreeMembershipBookingAlreadyAdded() throws MissingElementException, RetryableException, DataAccessException, SQLException, BookingApiException, BuilderException {
        mockBookingApiCallAndFetchMember(bookingDetail, membershipMock);
        bookingDetail.setAllFeeContainers(Collections.singletonList(new FeeContainerBuilder().feeContainerType("MEMBERSHIP_SUBSCRIPTION").build(new Random())));
        when(membershipMock.getMembershipType()).thenReturn(MembershipType.BASIC_FREE);
        when(membershipMock.getAutoRenewal()).thenReturn(MembershipRenewal.DISABLED);
        bookingTrackingServiceBean.processBooking(BOOKING_ID);
        verify(bookingTrackingStore).isBookingTracked(eq(dataSource), eq(BOOKING_ID));
        verify(bookingApiManager, never()).updateBooking(eq(BOOKING_ID), any(UpdateBookingRequest.class));
    }

    @Test(expectedExceptions = RetryableException.class)
    public void testBasicFreeMembershipBookingUpdateFails() throws MissingElementException, RetryableException, DataAccessException, SQLException, BookingApiException {
        mockBookingApiCallAndFetchMember(bookingDetail, membershipMock);
        when(membershipMock.getMembershipType()).thenReturn(MembershipType.BASIC_FREE);
        when(membershipMock.getAutoRenewal()).thenReturn(MembershipRenewal.DISABLED);
        when(bookingApiDao.searchBookingsByMembershipId(eq(membershipMock.getId()))).thenReturn(Collections.emptyList());
        when(bookingApiManager.updateBooking(eq(BOOKING_ID), any(UpdateBookingRequest.class))).thenReturn(null);
        bookingTrackingServiceBean.processBooking(BOOKING_ID);
    }

    @Test(expectedExceptions = RetryableException.class)
    public void testBasicFreeMembershipBookingUpdateError() throws MissingElementException, RetryableException, DataAccessException, SQLException, BookingApiException, BuilderException {
        mockBookingApiCallAndFetchMember(bookingDetail, membershipMock);
        when(membershipMock.getMembershipType()).thenReturn(MembershipType.BASIC_FREE);
        when(membershipMock.getAutoRenewal()).thenReturn(MembershipRenewal.DISABLED);
        when(bookingApiDao.searchBookingsByMembershipId(eq(membershipMock.getId()))).thenReturn(Collections.emptyList());
        BookingDetail bookingDetailError = buildBookingDetail();
        bookingDetailError.setErrorMessage("error");
        when(bookingApiManager.updateBooking(eq(BOOKING_ID), any(UpdateBookingRequest.class))).thenReturn(bookingDetailError);
        bookingTrackingServiceBean.processBooking(BOOKING_ID);
    }

    @Test
    public void testNoTrackNoPrimeBooking() throws DataAccessException, SQLException, BookingApiException, MissingElementException, RetryableException {
        mockBookingApiCallAndFetchMember(bookingDetail, membershipMock);
        bookingDetail.setMembershipPerks(null);
        bookingTrackingServiceBean.processBooking(BOOKING_ID);
        verifyZeroInteractions(bookingTrackingStore);
        verify(bookingApiManager, never()).updateBooking(eq(BOOKING_ID), any(UpdateBookingRequest.class));
    }

    @Test
    public void testGetMembershipBookingTracking() throws SQLException, DataAccessException {
        BookingTracking bookingTracking = createBookingTracking(BigDecimal.ONE, BigDecimal.ZERO);
        when(bookingTrackingStore.getBookingTracked(eq(dataSource), eq(BOOKING_ID))).thenReturn(bookingTracking);
        BookingTracking membershipBookingTracking = bookingTrackingServiceBean.getMembershipBookingTracking(BOOKING_ID);
        verify(bookingTrackingStore).getBookingTracked(eq(dataSource), eq(BOOKING_ID));
        assertEquals(membershipBookingTracking, bookingTracking);
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testGetMembershipBookingTrackingException() throws SQLException, DataAccessException {
        when(bookingTrackingStore.getBookingTracked(eq(dataSource), eq(BOOKING_ID))).thenThrow(SQL_EXCEPTION);
        bookingTrackingServiceBean.getMembershipBookingTracking(BOOKING_ID);
    }

    @Test
    public void testInsertMembershipBookingTracking() throws SQLException, DataAccessException {
        mockFetchMembership(membershipMock);
        BookingTracking bookingTracking = createBookingTracking(BigDecimal.ONE, BigDecimal.ZERO);
        when(bookingTrackingStore.trackBooking(eq(dataSource), eq(bookingTracking))).thenReturn(Boolean.TRUE);
        when(bookingTrackingStore.getBookingTracked(eq(dataSource), eq(BOOKING_ID))).thenReturn(bookingTracking);
        BookingTracking bookingTrackingSaved = bookingTrackingServiceBean.insertMembershipBookingTracking(bookingTracking);
        assertEquals(bookingTrackingSaved, bookingTracking);
        verify(bookingTrackingStore).trackBooking(eq(dataSource), eq(bookingTracking));
        verify(bookingTrackingStore).getBookingTracked(eq(dataSource), eq(BOOKING_ID));
        verify(membershipStore).updateMembershipBalance(eq(dataSource), eq(bookingTracking.getMembershipId()), eq(BigDecimal.valueOf(11L)));
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testInsertMembershipBookingTrackingMembershipNotFound() throws SQLException, DataAccessException {
        BookingTracking bookingTracking = createBookingTracking(BigDecimal.ONE, BigDecimal.ZERO);
        when(bookingTrackingStore.trackBooking(eq(dataSource), eq(bookingTracking))).thenReturn(Boolean.TRUE);
        when(membershipStore.fetchMembershipById(eq(dataSource), eq(bookingTracking.getMembershipId()))).thenThrow(new DataNotFoundException(StringUtils.EMPTY));
        bookingTrackingServiceBean.insertMembershipBookingTracking(bookingTracking);
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testInsertMembershipBookingTrackingSQLException() throws SQLException, DataAccessException {
        BookingTracking bookingTracking = createBookingTracking(BigDecimal.ONE, BigDecimal.ZERO);
        when(bookingTrackingStore.trackBooking(eq(dataSource), eq(bookingTracking))).thenThrow(SQL_EXCEPTION);
        bookingTrackingServiceBean.insertMembershipBookingTracking(bookingTracking);
    }

    @Test
    public void testDeleteMembershipBookingTracking() throws SQLException, DataAccessException {
        mockFetchMembership(membershipMock);
        BookingTracking bookingTracking = createBookingTracking(BigDecimal.ONE, BigDecimal.ZERO);
        when(bookingTrackingStore.getBookingTracked(eq(dataSource), eq(BOOKING_ID))).thenReturn(bookingTracking);
        when(bookingTrackingStore.removeTrackedBooking(eq(dataSource), eq(bookingTracking.getBookingId()))).thenReturn(Boolean.TRUE);
        bookingTrackingServiceBean.deleteMembershipBookingTracking(bookingTracking);
        BigDecimal expectedBalance = membershipMock.getBalance().subtract(bookingTracking.getAvoidFeeAmount());
        verify(membershipStore).updateMembershipBalance(eq(dataSource), eq(bookingTracking.getMembershipId()), eq(expectedBalance));
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testDeleteMembershipBookingTrackingSQLException() throws SQLException, DataAccessException {
        when(bookingTrackingStore.removeTrackedBooking(eq(dataSource), eq(BOOKING_ID))).thenThrow(SQL_EXCEPTION);
        bookingTrackingServiceBean.deleteMembershipBookingTracking(createBookingTracking(BigDecimal.ONE, BigDecimal.ZERO));
    }

    private BookingDetail buildBookingDetail() throws BuilderException {
        Calendar bookingCalendar = Calendar.getInstance();
        bookingCalendar.setTime(new Date());
        return new BookingDetailBuilder()
                .bookingBasicInfoBuilder(new BookingBasicInfoBuilder()
                        .id(BOOKING_ID).creationDate(bookingCalendar)
                        .website(new WebsiteBuilder().brand(BRAND_ED).code(SITE_ES)))
                .bookingStatus(CONTRACT)
                .membershipPerks(new MembershipPerksBuilder().memberId(MEMBERSHIP_ID).feeContainerId(FEE_CONTAINER_ID))
                .build(RANDOM);
    }
}
