package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.odigeo.membership.MembershipFee;
import com.odigeo.membership.product.MembershipSubscriptionFeeStore;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Collections;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;

public class MembershipProductServiceBeanTest {

    @Mock
    private DataSource dataSource;
    @Mock
    private MembershipSubscriptionFeeStore membershipSubscriptionFeeStore;

    @InjectMocks
    private MembershipProductServiceBean membershipProductServiceBean = new MembershipProductServiceBean();

    private static final long MEMBER_ID1 = 10L;
    private static final String EUR = "EUR";
    private static final String RENEWAL = "RENEWAL";
    private static final MembershipFee MEMBERSHIP_FEE = new MembershipFee("1", BigDecimal.TEN, EUR, RENEWAL);

    @BeforeMethod
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void testGetMembershipFee() throws Exception {
        when(membershipSubscriptionFeeStore.getMembershipFee(eq(dataSource), anyString())).thenReturn(Collections.singletonList(MEMBERSHIP_FEE));
        assertEquals(membershipProductServiceBean.getMembershipFees(MEMBER_ID1), Collections.singletonList(MEMBERSHIP_FEE));
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testGetMembershipFeeKO() throws Exception {
        when(membershipSubscriptionFeeStore.getMembershipFee(eq(dataSource), anyString())).thenThrow(new SQLException());
        membershipProductServiceBean.getMembershipFees(MEMBER_ID1);
    }
}