package com.odigeo.membership.robots.activator;

import com.odigeo.membership.robots.BookingUpdatesReaderServiceBean;
import com.odigeo.membership.robots.tracker.MembershipTrackerConsumerController;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.verification.VerificationMode;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.verify;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

@Test
public class BookingUpdatesReaderServiceBeanTest {

    private static final VerificationMode ONE_TIME = Mockito.times(1);
    @Mock
    private MembershipTrackerConsumerController membershipTrackerConsumerController;
    @InjectMocks
    private BookingUpdatesReaderServiceBean bookingUpdatesReaderServiceBean;

    @BeforeMethod
    public void setUp() {
        bookingUpdatesReaderServiceBean = new BookingUpdatesReaderServiceBean();
        MockitoAnnotations.initMocks(this);
    }

    public void testStart() throws Exception {
        assertTrue(bookingUpdatesReaderServiceBean.startTrackerConsumerController());
        verify(membershipTrackerConsumerController, ONE_TIME).startMultiThreadConsumer();
        verify(membershipTrackerConsumerController, ONE_TIME).join();
    }

    public void testStartWithException() throws Exception {
        Mockito.doThrow(new InterruptedException()).when(membershipTrackerConsumerController).join();
        assertFalse(bookingUpdatesReaderServiceBean.startTrackerConsumerController());
    }

    public void testStartWithNoBookings() {
        assertTrue(bookingUpdatesReaderServiceBean.startTrackerConsumerController());
    }

    public void testStop() {
        bookingUpdatesReaderServiceBean.stopTrackerConsumerController();
        verify(membershipTrackerConsumerController).stopMultiThreadConsumer();
    }
}