package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.bookingapi.mock.v12.response.BookingDetailBuilder;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.UpdateMembership;
import com.odigeo.membership.UpdateMembershipAction;
import com.odigeo.membership.exception.DataNotFoundException;
import com.odigeo.membership.exception.ExistingRecurringException;
import com.odigeo.membership.exception.bookingapi.BookingApiException;
import com.odigeo.membership.member.userarea.MemberUserAreaService;
import com.odigeo.membership.recurring.MembershipRecurringStore;
import com.odigeo.membership.v4.messages.SubscriptionStatus;
import com.odigeo.messaging.MembershipMessageSendingService;
import com.odigeo.membership.v4.messages.MembershipSubscriptionMessage;
import com.odigeo.messaging.SubscriptionMessagePublisher;
import org.apache.commons.lang.StringUtils;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.security.InvalidParameterException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Random;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertTrue;

public class UpdateMembershipServiceBeanTest {

    private static final long MEMBER_ID1 = 10L;
    private static final long MEMBER_ID2 = 11L;
    private static final long MEMBER_ID3 = 12L;
    private static final long MEMBER_ID4 = 13L;
    private static final long MEMBER_ID5 = 14L;
    private static final long MEMBER_ACCOUNT_ID1 = 1L;
    private static final long MEMBER_ACCOUNT_ID2 = 2L;
    private static final long MEMBER_ACCOUNT_ID3 = 3L;
    private static final long MEMBER_ACCOUNT_ID4 = 4L;
    private static final String SITE_ES = "ES";
    private static final String SITE_FR = "FR";
    private static final String SITE_DE = "DE";
    private static final String SITE_GB = "GB";
    private static final int DEFAULT_DURATION = 12;
    private static final String RECURRING_ID = "12345";

    @Mock
    private DataSource dataSource;
    @Mock
    private MemberManager memberManager;
    @Mock
    private MembershipStore membershipStore;
    @Mock
    private MemberService memberService;
    @Mock
    private MemberUserAreaService memberUserAreaService;
    @Mock
    private MemberStatusActionStore memberStatusActionStore;
    @Mock
    private SubscriptionMessagePublisher subscriptionMessagePublisher;
    @Mock
    private MembershipMessageSendingService membershipMessageSendingServiceMock;
    @Mock
    private MembershipRecurringStore membershipRecurringStore;

    @InjectMocks
    private UpdateMembershipServiceBean updateMembershipServiceBean = new UpdateMembershipServiceBean();

    private Membership pendingToCollectMembership = getMembership(MEMBER_ID1, SITE_ES, MemberStatus.PENDING_TO_COLLECT, MembershipRenewal.ENABLED, MEMBER_ACCOUNT_ID1);
    private Membership activeMembership1 = getMembership(MEMBER_ID1, SITE_ES, MemberStatus.ACTIVATED, MembershipRenewal.ENABLED, MEMBER_ACCOUNT_ID1);
    private Membership activeMembership2 = getMembership(MEMBER_ID4, SITE_DE, MemberStatus.ACTIVATED, MembershipRenewal.ENABLED, MEMBER_ACCOUNT_ID3);
    private Membership pendingMembership = getMembership(MEMBER_ID3, SITE_DE, MemberStatus.PENDING_TO_ACTIVATE, MembershipRenewal.ENABLED, MEMBER_ACCOUNT_ID3);
    private Membership inactiveMembership = getMembership(MEMBER_ID2, SITE_FR, MemberStatus.DEACTIVATED, MembershipRenewal.ENABLED, MEMBER_ACCOUNT_ID2);
    private Membership expiredMembership = getMembership(MEMBER_ID2, SITE_FR, MemberStatus.EXPIRED, MembershipRenewal.ENABLED, MEMBER_ACCOUNT_ID2);
    private Membership internalMembership = getInternalMembership(MEMBER_ID5, MemberStatus.ACTIVATED, MEMBER_ACCOUNT_ID4);
    private Membership enabledAutoRenewalMember = getMembership(MembershipRenewal.ENABLED);
    private Membership disabledAutoRenewalMember = getMembership(MembershipRenewal.DISABLED);

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        ConfigurationEngine.init(this::configure);
    }

    private void configure(Binder binder) {
        binder.bind(MemberManager.class).toInstance(memberManager);
        binder.bind(MembershipStore.class).toInstance(membershipStore);
        binder.bind(MemberUserAreaService.class).toInstance(memberUserAreaService);
        binder.bind(MemberStatusActionStore.class).toInstance(memberStatusActionStore);
        binder.bind(MembershipMessageSendingService.class).toInstance(membershipMessageSendingServiceMock);
        binder.bind(SubscriptionMessagePublisher.class).toInstance(subscriptionMessagePublisher);
        binder.bind(MembershipRecurringStore.class).toInstance(membershipRecurringStore);
    }

    @Test
    public void testUpdateMembershipExpireMembershipOperation() throws MissingElementException, DataAccessException, BookingApiException, ExistingRecurringException {
        UpdateMembership updateMembership = getUpdateMembership(UpdateMembershipAction.EXPIRE_MEMBERSHIP);
        mockGetMembershipByIdWithMemberAccount(activeMembership1);
        updateMembershipServiceBean.updateMembership(updateMembership);
        verify(membershipStore).updateStatus(dataSource, MEMBER_ID1, MemberStatus.EXPIRED);
    }

    @Test
    public void testUpdateMembershipDiscardMembershipOperation() throws MissingElementException, DataAccessException, BookingApiException, ExistingRecurringException {
        UpdateMembership updateMembership = getUpdateMembership(UpdateMembershipAction.DISCARD_MEMBERSHIP);
        mockGetMembershipByIdWithMemberAccount(pendingToCollectMembership);
        updateMembershipServiceBean.updateMembership(updateMembership);
        verify(membershipStore).updateStatus(dataSource, MEMBER_ID1, MemberStatus.DISCARDED);
    }

    private UpdateMembership getUpdateMembership(UpdateMembershipAction operation) {
        UpdateMembership updateMembership = new UpdateMembership();
        updateMembership.setMembershipId(Long.toString(MEMBER_ID1));
        updateMembership.setOperation(operation.name());
        return updateMembership;
    }

    @Test
    public void testUpdateMembershipReactivateMembershipOperation() throws MissingElementException, DataAccessException, BookingApiException, ExistingRecurringException {
        UpdateMembership updateMembership = getUpdateMembership(UpdateMembershipAction.REACTIVATE_MEMBERSHIP);
        Membership deactivatedMembership = getMembership(MemberStatus.DEACTIVATED);
        mockGetMembershipByIdWithMemberAccount(deactivatedMembership);
        updateMembershipServiceBean.updateMembership(updateMembership);
        verify(membershipStore).updateStatus(dataSource, MEMBER_ID1, MemberStatus.ACTIVATED);
    }

    @Test
    public void testUpdateMembershipDeactivateMembershipOperation() throws MissingElementException, DataAccessException, BookingApiException, ExistingRecurringException {
        UpdateMembership updateMembership = getUpdateMembership(UpdateMembershipAction.DEACTIVATE_MEMBERSHIP);
        mockGetMembershipByIdWithMemberAccount(activeMembership1);
        updateMembershipServiceBean.updateMembership(updateMembership);
        verify(membershipStore).updateStatus(dataSource, MEMBER_ID1, MemberStatus.DEACTIVATED);
    }

    @Test
    public void testUpdateMembershipAddingRecurringIdOperation() throws MissingElementException, DataAccessException, BookingApiException, SQLException, ExistingRecurringException {
        UpdateMembership updateMembership = getUpdateMembership(UpdateMembershipAction.INSERT_RECURRING_ID).setRecurringId(RECURRING_ID);
        when(membershipStore.fetchMembershipById(dataSource, Long.parseLong(updateMembership.getMembershipId()))).thenReturn(activeMembership1);
        assertTrue(updateMembershipServiceBean.updateMembership(updateMembership));
    }

    @Test(expectedExceptions = InvalidParameterException.class)
    public void testUpdateMembershipAddingRecurringIdOperationWithoutRecurring() throws MissingElementException, DataAccessException, BookingApiException, ExistingRecurringException, SQLException {
        UpdateMembership updateMembership = getUpdateMembership(UpdateMembershipAction.INSERT_RECURRING_ID);
        when(membershipStore.fetchMembershipById(dataSource, Long.parseLong(updateMembership.getMembershipId()))).thenReturn(activeMembership1);
        updateMembershipServiceBean.updateMembership(updateMembership);
    }

    @Test (expectedExceptions = MissingElementException.class)
    public void testUpdateMembershipAddingRecurringIdOperationWithNonExistingMembershipId() throws SQLException, DataAccessException, MissingElementException, BookingApiException, ExistingRecurringException {
        UpdateMembership updateMembership = getUpdateMembership(UpdateMembershipAction.INSERT_RECURRING_ID);
        when(membershipStore.fetchMembershipById(dataSource, Long.parseLong(updateMembership.getMembershipId())))
                .thenThrow(new DataNotFoundException("Membership ID not found."));
        updateMembershipServiceBean.updateMembership(updateMembership);
    }

    @Test
    public void testUpdateMembershipEnableAutoRenewalOperation() throws MissingElementException, DataAccessException, BookingApiException, ExistingRecurringException {
        UpdateMembership updateMembership = getUpdateMembership(UpdateMembershipAction.ENABLE_AUTO_RENEWAL);
        when(memberManager.getMembershipById(dataSource, MEMBER_ID1)).thenReturn(disabledAutoRenewalMember);
        updateMembershipServiceBean.updateMembership(updateMembership);
        verify(memberManager).enableAutoRenewal(dataSource, MEMBER_ID1);
    }

    @Test
    public void testUpdateMembershipDisableAutoRenewalOperation() throws MissingElementException, DataAccessException, BookingApiException, ExistingRecurringException {
        UpdateMembership updateMembership = getUpdateMembership(UpdateMembershipAction.DISABLE_AUTO_RENEWAL);
        when(memberManager.getMembershipById(dataSource, MEMBER_ID1)).thenReturn(activeMembership1);
        updateMembershipServiceBean.updateMembership(updateMembership);
        verify(memberManager).disableAutoRenewal(dataSource, MEMBER_ID1);
    }

    @Test
    public void testUpdateMembershipResetBalanceOperation() throws MissingElementException, DataAccessException, BookingApiException, SQLException, ExistingRecurringException {
        UpdateMembership updateMembership = getUpdateMembership(UpdateMembershipAction.CONSUME_MEMBERSHIP_REMNANT_BALANCE);
        updateMembershipServiceBean.updateMembership(updateMembership);
        verify(membershipStore).updateMembershipBalance(dataSource, MEMBER_ID1, BigDecimal.ZERO);
    }

    @Test
    public void testUpdateMembershipWrongOperation() throws MissingElementException, DataAccessException, BookingApiException, ExistingRecurringException {
        UpdateMembership updateMembership = getUpdateMembership(UpdateMembershipAction.EXPIRE_MEMBERSHIP);
        mockGetMembershipByIdWithMemberAccount(activeMembership1);
        updateMembershipServiceBean.updateMembership(updateMembership);
        verify(membershipStore).updateStatus(dataSource, MEMBER_ID1, MemberStatus.EXPIRED);
    }

    @Test
    public void testExpireActiveMembershipOk() throws Exception {
        mockManagerAndStoreForExpiration(activeMembership1, true);
        when(memberUserAreaService.getBookingDetailSubscription(activeMembership1.getId())).thenReturn(new BookingDetailBuilder().build(new Random()));
        assertEquals(updateMembershipServiceBean.expireMembership(new UpdateMembership()
                .setMembershipId(String.valueOf(activeMembership1.getId()))), MemberStatus.EXPIRED.toString());
        verifyStepsOfExpireMembership(activeMembership1);
        verify(subscriptionMessagePublisher).sendSubscriptionMessageToCRMTopic(activeMembership1, SubscriptionStatus.PENDING);
    }

    @Test
    public void testExpireInactiveMembershipOk() throws Exception {
        mockManagerAndStoreForExpiration(inactiveMembership, true);
        assertEquals(updateMembershipServiceBean.expireMembership(new UpdateMembership().
                setMembershipId(String.valueOf(inactiveMembership.getId()))), MemberStatus.EXPIRED.toString());
        verifyStepsOfExpireMembership(inactiveMembership);
        verify(subscriptionMessagePublisher).sendSubscriptionMessageToCRMTopic(inactiveMembership, SubscriptionStatus.UNSUBSCRIBED);
    }

    @Test
    public void testExpirePendingMembershipMktMessageNotSent() throws Exception {
        mockManagerAndStoreForExpiration(pendingMembership, true);
        assertEquals(updateMembershipServiceBean.expireMembership(new UpdateMembership()
                .setMembershipId(String.valueOf(pendingMembership.getId()))), MemberStatus.EXPIRED.toString());
        verifyStepsOfExpireMembership(pendingMembership);
        verify(membershipMessageSendingServiceMock, never()).sendMembershipSubscriptionMessage(any(MembershipSubscriptionMessage.class));
    }

    @Test
    public void testExpireInternalMembershipMktMessageNotSent() throws Exception {
        mockManagerAndStoreForExpiration(internalMembership, true);
        assertEquals(updateMembershipServiceBean.expireMembership(new UpdateMembership()
                .setMembershipId(String.valueOf(internalMembership.getId()))), MemberStatus.EXPIRED.toString());
        verifyStepsOfExpireMembership(internalMembership);
        verify(membershipMessageSendingServiceMock, never()).sendMembershipSubscriptionMessage(any(MembershipSubscriptionMessage.class));
    }


    private void verifyStepsOfExpireMembership(Membership membership) throws DataAccessException, SQLException {
        verify(membershipStore).updateStatus(eq(dataSource), eq(membership.getId()), eq(MemberStatus.EXPIRED));
        verify(memberStatusActionStore)
            .createMemberStatusAction(eq(dataSource), eq(membership.getId()), eq(StatusAction.EXPIRATION));
        verify(membershipMessageSendingServiceMock).sendMembershipIdToMembershipReporter(membership.getId());
    }

    @Test
    public void testExpireActiveMembershipStoreErrorKO() throws Exception {
        mockManagerAndStoreForExpiration(activeMembership1, false);
        assertEquals(updateMembershipServiceBean.expireMembership(new UpdateMembership().
                setMembershipId(String.valueOf(activeMembership1.getId()))), activeMembership1.getStatus().toString());
        verify(membershipStore).updateStatus(eq(dataSource), eq(activeMembership1.getId()), eq(MemberStatus.EXPIRED));
        verify(memberStatusActionStore, never()).createMemberStatusAction(eq(dataSource), anyLong(), any(StatusAction.class));
    }

    @Test
    public void testExpirePendingMembershipStateNotAllowedKO() throws Exception {
        mockManagerAndStoreForExpiration(pendingMembership, false);
        assertEquals(updateMembershipServiceBean.expireMembership(new UpdateMembership()
                .setMembershipId(String.valueOf(pendingMembership.getId()))), pendingMembership.getStatus().toString());
        verify(membershipStore, never()).updateStatus(eq(dataSource), eq(activeMembership1.getId()), eq(MemberStatus.EXPIRED));
    }

    private void mockManagerAndStoreForExpiration(Membership membership, boolean isExpireMembership) throws Exception {
        mockGetMembershipByIdWithMemberAccount(membership);
        when(membershipStore.updateStatus(dataSource, membership.getId(), MemberStatus.EXPIRED)).thenReturn(isExpireMembership);
    }

    @Test
    public void testDisableDeactivatedMember() throws Exception {
        mockGetMembershipByIdWithMemberAccount(inactiveMembership);
        assertEquals(updateMembershipServiceBean.deactivateMembership(new UpdateMembership()
                .setMembershipId(String.valueOf(inactiveMembership.getId()))), MemberStatus.DEACTIVATED.toString());
        verify(membershipStore, never()).updateStatus(dataSource, MEMBER_ID1, MemberStatus.DEACTIVATED);
    }

    @Test
    public void testDisableMemberActivated() throws Exception {
        testDisableMemberActivation(Boolean.TRUE);
        assertEquals(updateMembershipServiceBean.deactivateMembership(new UpdateMembership()
                .setMembershipId(String.valueOf(activeMembership2.getId()))), MemberStatus.DEACTIVATED.toString());
        verify(memberStatusActionStore).createMemberStatusAction(eq(dataSource), anyLong(), any(StatusAction.class));
    }

    @Test
    public void testDisableMemberKO() throws Exception {
        testDisableMemberActivation(Boolean.FALSE);
        assertEquals(updateMembershipServiceBean.deactivateMembership(new UpdateMembership()
                .setMembershipId(String.valueOf(activeMembership2.getId()))), activeMembership2.getStatus().toString());
        verify(memberStatusActionStore, never()).createMemberStatusAction(eq(dataSource), anyLong(), any(StatusAction.class));
    }

    private void testDisableMemberActivation(Boolean isDisabledMember) throws Exception {
        mockGetMembershipByIdWithMemberAccount(activeMembership2);
        when(membershipStore.updateStatus(dataSource, activeMembership2.getId(), MemberStatus.DEACTIVATED)).thenReturn(isDisabledMember);
    }

    @Test
    public void testDisablePendingMember() throws Exception {
        mockGetMembershipByIdWithMemberAccount(pendingMembership);
        assertEquals(updateMembershipServiceBean.deactivateMembership(new UpdateMembership()
                .setMembershipId(String.valueOf(pendingMembership.getId()))), MemberStatus.PENDING_TO_ACTIVATE.toString());
        verify(membershipStore, never()).updateStatus(dataSource, MEMBER_ID1, MemberStatus.DEACTIVATED);
    }

    @Test
    public void testReactivateActiveMember() throws Exception {
        mockGetMembershipByIdWithMemberAccount(activeMembership1);
        assertEquals(MemberStatus.ACTIVATED.toString(), updateMembershipServiceBean.reactivateMembership(new UpdateMembership()
                .setMembershipId(String.valueOf(activeMembership1.getId()))));
    }

    @Test
    public void testReactivateDeactivatedMember() throws Exception {
        setReactiveDeactivatedMember();
        assertEquals(MemberStatus.ACTIVATED.toString(), updateMembershipServiceBean.reactivateMembership(new UpdateMembership()
                .setMembershipId(String.valueOf(inactiveMembership.getId()))));
    }

    @Test
    public void testReactivatePendingToActiveMember() throws Exception {
        mockGetMembershipByIdWithMemberAccount(pendingMembership);
        assertEquals(MemberStatus.PENDING_TO_ACTIVATE.toString(), updateMembershipServiceBean.reactivateMembership(new UpdateMembership()
                .setMembershipId(String.valueOf(pendingMembership.getId()))));
    }

    private void setReactiveDeactivatedMember() throws MissingElementException, DataAccessException {
        mockGetMembershipByIdWithMemberAccount(inactiveMembership);
        when(membershipStore.updateStatus(dataSource, inactiveMembership.getId(), MemberStatus.ACTIVATED)).thenReturn(Boolean.TRUE);
    }

    @Test
    public void testDisableMemberWithAutoRenewalEnabled() throws Exception {
        when(memberManager.getMembershipById(dataSource, MEMBER_ID1)).thenReturn(enabledAutoRenewalMember);
        when(memberManager.disableAutoRenewal(dataSource, MEMBER_ID1)).thenReturn(true);
        assertEquals(updateMembershipServiceBean.disableAutoRenewal(new UpdateMembership()
                .setMembershipId(String.valueOf(MEMBER_ID1))), MembershipRenewal.DISABLED.toString());
        verify(memberManager).disableAutoRenewal(eq(dataSource), eq(MEMBER_ID1));
    }

    @Test
    public void testEnableMemberWithAutoRenewalDisabled() throws Exception {
        when(memberManager.getMembershipById(dataSource, MEMBER_ID1)).thenReturn(disabledAutoRenewalMember);
        when(memberManager.enableAutoRenewal(dataSource, MEMBER_ID1)).thenReturn(true);
        assertEquals(updateMembershipServiceBean.enableAutoRenewal(new UpdateMembership()
                .setMembershipId(String.valueOf(MEMBER_ID1))), MembershipRenewal.ENABLED.toString());
        verify(memberManager).enableAutoRenewal(eq(dataSource), eq(MEMBER_ID1));
    }

    @Test
    public void testEnableMemberWithAutoRenewalFalse() throws Exception {
        when(memberManager.getMembershipById(dataSource, MEMBER_ID1)).thenReturn(disabledAutoRenewalMember);
        when(memberManager.enableAutoRenewal(dataSource, MEMBER_ID1)).thenReturn(false);
        assertNotEquals(updateMembershipServiceBean.enableAutoRenewal(new UpdateMembership()
                .setMembershipId(String.valueOf(MEMBER_ID1))), MembershipRenewal.ENABLED.toString());
        verify(memberManager).enableAutoRenewal(eq(dataSource), eq(MEMBER_ID1));
    }

    @Test
    public void testEnableMemberWithAutoRenewalEnabled() throws Exception {
        when(memberManager.getMembershipById(dataSource, MEMBER_ID1)).thenReturn(enabledAutoRenewalMember);
        assertEquals(updateMembershipServiceBean.enableAutoRenewal(new UpdateMembership()
                .setMembershipId(String.valueOf(MEMBER_ID1))), MembershipRenewal.ENABLED.toString());
        verify(memberManager, never()).enableAutoRenewal(eq(dataSource), eq(MEMBER_ID1));
    }

    @Test
    public void testDisableMemberWithAutoRenewalDisabled() throws Exception {
        when(memberManager.getMembershipById(dataSource, MEMBER_ID1)).thenReturn(disabledAutoRenewalMember);
        assertEquals(updateMembershipServiceBean.disableAutoRenewal(new UpdateMembership()
                .setMembershipId(String.valueOf(MEMBER_ID1))), MembershipRenewal.DISABLED.toString());
        verify(memberManager, never()).disableAutoRenewal(eq(dataSource), eq(MEMBER_ID1));
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testReactivateDeactivateMemberDataAccessException() throws SQLException,
            DataAccessException, MissingElementException {
        setReactiveDeactivatedMember();
        doThrow(new SQLException(StringUtils.EMPTY)).when(memberStatusActionStore).createMemberStatusAction(any(DataSource.class), anyLong(), any(StatusAction.class));
        assertEquals(MemberStatus.ACTIVATED.toString(), updateMembershipServiceBean.reactivateMembership(new UpdateMembership()
                .setMembershipId(String.valueOf(inactiveMembership.getId()))));
    }

    @Test
    public void testReactivateDeactivatedMemberBookingApiException() throws Exception {
        setReactiveDeactivatedMember();
        doThrow(new BookingApiException(StringUtils.EMPTY)).when(memberUserAreaService).getBookingDetailSubscription(inactiveMembership.getId());
        assertEquals(MemberStatus.ACTIVATED.toString(), updateMembershipServiceBean.reactivateMembership(new UpdateMembership()
                .setMembershipId(String.valueOf(inactiveMembership.getId()))));
    }

    @Test
    public void testConsumeMembershipRemnantBalance() throws Exception {
        when(membershipStore.updateMembershipBalance(dataSource, activeMembership1.getId(), BigDecimal.ZERO)).thenReturn(true);
        assertTrue(updateMembershipServiceBean.consumeMembershipRemnantBalance(new UpdateMembership()
                .setMembershipId(String.valueOf(activeMembership1.getId()))));
    }

    @Test
    public void testConsumeMembershipRemnantBalance_MemberNotFound() throws Exception {
        Membership nonExistingMembership = getMembership(MemberStatus.EXPIRED);
        when(membershipStore.updateMembershipBalance(dataSource, nonExistingMembership.getId(), BigDecimal.ZERO)).thenReturn(false);
        assertFalse(updateMembershipServiceBean.consumeMembershipRemnantBalance(new UpdateMembership()
                .setMembershipId(String.valueOf(nonExistingMembership.getId()))));
    }

    @Test(expectedExceptions = DataAccessException.class, expectedExceptionsMessageRegExp = "Error trying to reset membership balance to zero for membershipId " + MEMBER_ID1)
    public void testConsumeMembershipRemnantBalance_ExceptionThrown() throws Exception {
        doThrow(new SQLException(StringUtils.EMPTY)).when(membershipStore).updateMembershipBalance(dataSource, activeMembership1.getId(), BigDecimal.ZERO);
        updateMembershipServiceBean.consumeMembershipRemnantBalance(new UpdateMembership()
                .setMembershipId(String.valueOf(activeMembership1.getId())));
        verify(membershipStore).updateMembershipBalance(dataSource, activeMembership1.getId(), BigDecimal.ZERO);
    }

    @Test
    public void testUpdateMembership_SendCorrectIdToReporter() throws MissingElementException, DataAccessException, BookingApiException, ExistingRecurringException {
        UpdateMembership updateMembership = getUpdateMembership(UpdateMembershipAction.DEACTIVATE_MEMBERSHIP);
        mockGetMembershipByIdWithMemberAccount(activeMembership1);
        when(membershipStore.updateStatus(dataSource, MEMBER_ID1, inactiveMembership.getStatus())).thenReturn(true);
        updateMembershipServiceBean.updateMembership(updateMembership);
        verify(membershipMessageSendingServiceMock).sendMembershipIdToMembershipReporter(MEMBER_ID1);
    }

    @Test
    public void testReactivateMembership_SendCorrectIdToReporter() throws MissingElementException, DataAccessException {
        mockGetMembershipByIdWithMemberAccount(inactiveMembership);
        when(membershipStore.updateStatus(dataSource, MEMBER_ID2, activeMembership1.getStatus())).thenReturn(true);
        updateMembershipServiceBean.reactivateMembership(new UpdateMembership()
                .setMembershipId(String.valueOf(MEMBER_ID2)));
        verify(membershipMessageSendingServiceMock).sendMembershipIdToMembershipReporter(MEMBER_ID2);
    }

    @Test
    public void testDeactivateMembership_SendCorrectIdToReporter() throws MissingElementException, DataAccessException {
        mockGetMembershipByIdWithMemberAccount(activeMembership1);
        when(membershipStore.updateStatus(dataSource, MEMBER_ID1, inactiveMembership.getStatus())).thenReturn(true);
        updateMembershipServiceBean.deactivateMembership((new UpdateMembership()
                .setMembershipId(String.valueOf(MEMBER_ID1))));
        verify(membershipMessageSendingServiceMock).sendMembershipIdToMembershipReporter(MEMBER_ID1);
    }

    @Test
    public void testExpireMembership_SendCorrectIdToReporter() throws MissingElementException, DataAccessException {
        mockGetMembershipByIdWithMemberAccount(activeMembership1);
        when(membershipStore.updateStatus(dataSource, MEMBER_ID1, expiredMembership.getStatus())).thenReturn(true);
        updateMembershipServiceBean.expireMembership(new UpdateMembership()
                .setMembershipId(String.valueOf(MEMBER_ID1)));
        verify(membershipMessageSendingServiceMock).sendMembershipIdToMembershipReporter(MEMBER_ID1);
    }

    @Test
    public void testDisableAutoRenewal_SendCorrectIdToReporter() throws Exception {
        when(memberManager.getMembershipById(dataSource, MEMBER_ID1)).thenReturn(enabledAutoRenewalMember);
        when(memberManager.disableAutoRenewal(dataSource, MEMBER_ID1)).thenReturn(true);
        updateMembershipServiceBean.disableAutoRenewal(new UpdateMembership()
                .setMembershipId(String.valueOf(MEMBER_ID1)));
        verify(membershipMessageSendingServiceMock).sendMembershipIdToMembershipReporter(MEMBER_ID1);
    }

    @Test
    public void testEnableAutoRenewal_SendCorrectIdToReporter() throws Exception {
        when(memberManager.getMembershipById(dataSource, MEMBER_ID1)).thenReturn(disabledAutoRenewalMember);
        when(memberManager.enableAutoRenewal(dataSource, MEMBER_ID1)).thenReturn(true);
        updateMembershipServiceBean.enableAutoRenewal(new UpdateMembership()
                .setMembershipId(String.valueOf(MEMBER_ID1)));
        verify(membershipMessageSendingServiceMock).sendMembershipIdToMembershipReporter(MEMBER_ID1);
    }

    @Test
    public void testActivatePendingToCollect_NotActivated() throws DataAccessException, SQLException {
        when(membershipStore.activatePendingToCollect(dataSource, pendingToCollectMembership)).thenReturn(false);
        assertFalse(updateMembershipServiceBean.activatePendingToCollect(pendingToCollectMembership));
        verify(memberStatusActionStore, never()).createMemberStatusAction(any(DataSource.class), anyLong(), any(StatusAction.class));
        verify(membershipMessageSendingServiceMock, never()).sendMembershipSubscriptionMessage(any(MembershipSubscriptionMessage.class));
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testActivatePendingToCollect_ActivationException() throws SQLException, DataAccessException {
        when(membershipStore.activatePendingToCollect(dataSource, pendingToCollectMembership)).thenThrow(new SQLException());
        updateMembershipServiceBean.activatePendingToCollect(pendingToCollectMembership);
    }

    @Test
    public void testActivatePendingToCollect_HappyPath() throws DataAccessException, SQLException, MissingElementException {
        initMocksForActivatePTC();
        assertTrue(updateMembershipServiceBean.activatePendingToCollect(pendingToCollectMembership));
        verify(memberStatusActionStore).createMemberStatusAction(dataSource, pendingToCollectMembership.getId(), StatusAction.ACTIVATION);
        verify(subscriptionMessagePublisher).sendSubscriptionMessageToCRMTopic(pendingToCollectMembership, SubscriptionStatus.SUBSCRIBED);
        verify(membershipMessageSendingServiceMock).sendMembershipIdToMembershipReporter(pendingToCollectMembership.getId());
    }

    @Test
    public void testActivatePendingToCollect_StatusActionStoreException() throws DataAccessException, SQLException, MissingElementException {
        initMocksForActivatePTC();
        doThrow(new SQLException()).when(memberStatusActionStore).createMemberStatusAction(any(DataSource.class), anyLong(), any(StatusAction.class));
        assertTrue(updateMembershipServiceBean.activatePendingToCollect(pendingToCollectMembership));
        verify(subscriptionMessagePublisher).sendSubscriptionMessageToCRMTopic(pendingToCollectMembership, SubscriptionStatus.SUBSCRIBED);
        verify(membershipMessageSendingServiceMock).sendMembershipIdToMembershipReporter(pendingToCollectMembership.getId());
    }

    @Test
    public void testActivatePendingToCollect_FetchMembershipException() throws DataAccessException, SQLException, MissingElementException {
        initMocksForActivatePTC();
        doThrow(new DataAccessException(""))
                .when(memberService).getMembershipByIdWithMemberAccount(pendingToCollectMembership.getId());
        assertTrue(updateMembershipServiceBean.activatePendingToCollect(pendingToCollectMembership));
        verify(memberStatusActionStore).createMemberStatusAction(dataSource, pendingToCollectMembership.getId(), StatusAction.ACTIVATION);
        verify(subscriptionMessagePublisher, never()).sendSubscriptionMessageToCRMTopic(pendingToCollectMembership, SubscriptionStatus.SUBSCRIBED);
        verify(membershipMessageSendingServiceMock).sendMembershipIdToMembershipReporter(pendingToCollectMembership.getId());
    }

    private void initMocksForActivatePTC() throws SQLException, DataAccessException, MissingElementException {
        when(membershipStore.activatePendingToCollect(dataSource, pendingToCollectMembership)).thenReturn(true);
        when(memberService.getMembershipByIdWithMemberAccount(pendingToCollectMembership.getId())).thenReturn(activeMembership1);
    }

    private void mockGetMembershipByIdWithMemberAccount(Membership expectedMembership) throws MissingElementException, DataAccessException {
        when(memberService.getMembershipByIdWithMemberAccount(expectedMembership.getId())).thenReturn(expectedMembership);
    }

    private static Membership getMembership(MemberStatus status) {
        return getMembership(MEMBER_ID1, SITE_ES, status, MembershipRenewal.ENABLED, MEMBER_ACCOUNT_ID1);
    }

    private static Membership getMembership(MembershipRenewal renewal) {
        return getMembership(MEMBER_ID1, SITE_ES, MemberStatus.ACTIVATED, renewal, MEMBER_ACCOUNT_ID1);
    }

    private static Membership getMembership(long memberId, String website, MemberStatus status, MembershipRenewal renewal, long memberAccountId) {
        MembershipBuilder builder = new MembershipBuilder().setId(memberId).setWebsite(website).setStatus(status)
                .setMembershipRenewal(renewal).setActivationDate(LocalDateTime.now()).setMemberAccountId(memberAccountId);
        if (Arrays.asList(MemberStatus.ACTIVATED, MemberStatus.DEACTIVATED).contains(status)) {
            builder.setExpirationDate(LocalDateTime.now().plusMonths(DEFAULT_DURATION));
        }
        return builder.build();
    }

    private static Membership getInternalMembership(long memberId, MemberStatus status, long memberAccountId) {
        return new MembershipBuilder().setId(memberId).setWebsite(SITE_GB).setStatus(status).setMembershipRenewal(MembershipRenewal.ENABLED)
                .setMemberAccountId(memberAccountId).build();
    }
}
