Feature: Test retrieve user area information

  Scenario: retrieve total amount saved by the user with one subscription

    Given the next memberAccount stored in db:
      | memberAccountId | userId | firstName | lastNames |
      | 123             | 4321   | JOSE      | GARCIA    |

    And the next membership stored in db:
      | memberId | website | status    | autoRenewal | memberAccountId | activationDate | expirationDate | balance |
      | 666      | ES      | ACTIVATED | ENABLED     | 123             | 2017-12-12     | 2018-12-12     | 54.99   |

    And that booking-api returns the following bookings when performs the search by membershipId
      | membershipId | bookingId | isSubscriptionBooking |
      | 666          | 2567      | true                  |
      | 666          | 2568      | false                 |
      | 666          | 2569      | false                 |

    And each prime booking has the following details
      | bookingId | containerId | feeTotalAmount | currencyCode | ccNumber         | ccExpirationDate | productCategoryBooking |
      | 2567      | 213         | 35             | EUR          | 1234123412346969 | 12/22            | subscription           |
      | 2568      | 112         | 24             | EUR          | 6768678099999999 | 12/19            | itinerary              |
      | 2569      | 112         | 21             | EUR          | 6768678099994390 | 10/21            | itinerary              |

    When the client request subscription details to membership Api with memberAccountId 123

    Then the subscription details has 3 prime bookings info and the total amount saved is 80

    And for each subscription the subscription payment method is the following
      | membershipId | ccNumber         | ccExpirationDate |
      | 666          | ************6969 | 12/22            |

  Scenario: retrieve total amount saved by the user with more subscriptions

    Given the next memberAccount stored in db:
      | memberAccountId | userId | firstName | lastNames |
      | 178             | 4321   | JOSE      | GARCIA    |

    And the next membership stored in db:
      | memberId | website | status    | autoRenewal | memberAccountId | activationDate | expirationDate | balance |
      | 665      | ES      | ACTIVATED | ENABLED     | 178             | 2017-12-12     | 2018-12-12     | 54.99   |
      | 777      | ES      | ACTIVATED | ENABLED     | 178             | 2018-12-12     | 2019-12-12     | 54.99   |

    And that booking-api returns the following bookings when performs the search by membershipId
      | membershipId | bookingId | isSubscriptionBooking |
      | 665          | 1234      | false                 |
      | 665          | 1235      | true                  |
      | 665          | 1236      | false                 |
      | 777          | 1500      | false                 |
      | 777          | 1510      | true                  |

    And each prime booking has the following details
      | bookingId | containerId | feeTotalAmount | currencyCode | ccNumber         | ccExpirationDate | productCategoryBooking |
      | 1235      | 112         | 24             | EUR          | 1234123412341234 | 12/22            | subscription           |
      | 1234      | 213         | 35             | EUR          | 2131415435414112 | 05/22            | itinerary              |
      | 1236      | 113         | 21             | EUR          | 2342545435321423 | 12/25            | itinerary              |
      | 1500      | 121         | 40             | EUR          | 2342545435321423 | 12/25            | itinerary              |
      | 1510      | 343         | 10             | EUR          | 5678567856785678 | 05/21            | subscription           |


    When the client request subscription details to membership Api with memberAccountId 178

    Then the subscription details has 5 prime bookings info and the total amount saved is 130

    And for each subscription the subscription payment method is the following
      | membershipId | ccNumber         | ccExpirationDate |
      | 665          | ************1234 | 12/22            |
      | 777          | ************5678 | 05/21            |

  Scenario Outline: User has any active memberships in one brand or not
    Given the next memberAccount stored in db:
      | memberAccountId | userId | firstName | lastNames |
      | 1178            | 123    | JOSE      | GARCIA    |
      | 1179            | 123    | JOSE      | GARCIA    |
      | 1180            | 123    | JOSE      | GARCIA    |
      | 1181            | 123    | JOSE      | GARCIA    |
      | 1182            | 123    | JOSE      | GARCIA    |
      | 1183            | 123    | JOSE      | GARCIA    |
      | 1184            | 123    | JOSE      | GARCIA    |
      | 1185            | 123    | JOSE      | GARCIA    |

    And the next membership stored in db:
      | memberId | website | status      | autoRenewal | memberAccountId | activationDate | expirationDate | balance |
      | 1567     | ES      | ACTIVATED   | ENABLED     | 1178            | 2017-12-12     | 2018-12-12     | 54.99   |
      | 1568     | IT      | ACTIVATED   | ENABLED     | 1179            | 2017-12-12     | 2018-12-12     | 54.99   |
      | 1569     | FR      | DEACTIVATED | ENABLED     | 1180            | 2017-12-12     | 2018-12-12     | 54.99   |
      | 1570     | OPFR    | EXPIRED     | ENABLED     | 1181            | 2017-12-12     | 2018-12-12     | 54.99   |
      | 1571     | OPDE    | DEACTIVATED | ENABLED     | 1182            | 2017-12-12     | 2018-12-12     | 54.99   |
      | 1573     | GOFR    | ACTIVATED   | ENABLED     | 1184            | 2017-12-12     | 2018-12-12     | 54.99   |
      | 1574     | GOES    | ACTIVATED   | ENABLED     | 1185            | 2017-12-12     | 2018-12-12     | 54.99   |

    When the client asks if the user 123 has any active membership for the brand <brand>
    Then user has any active membership for the brand is <expectedResult>

    Examples:
      | brand   | expectedResult |
      | ED      | true           |
      | Ed      | true           |
      | OP      | false          |
      | oP      | false          |
      | GO      | true           |
      | go      | true           |
      | xx      | false          |
      | unknown | false          |

  Scenario Outline: Retrieve membership account ID by membership ID
    Given the next membership stored in db:
      | memberId       | website | status    | autoRenewal | memberAccountId       | activationDate | expirationDate | balance |
      | <membershipId> | ES      | ACTIVATED | ENABLED     | <membershipAccountId> | 2017-12-12     | 2018-12-12     | 54.99   |
    When the client asks for membership account ID with the membership ID <membershipId>
    Then membership account ID is <membershipAccountId>
    Examples:
      | membershipId | membershipAccountId |
      | 839          | 994                 |
      | 130          | 824                 |