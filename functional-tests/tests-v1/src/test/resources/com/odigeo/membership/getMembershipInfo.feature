Feature: Test getMembershipInfo service

  Scenario Outline: Check MembershipInfo
    Given the next memberAccount stored in db:
      | memberAccountId | userId | firstName | lastNames |
      | 91              | 1010   | SQUALL    | LEONHART  |
      | 894             | 1894   | JON       | SMITH     |
    And the next membership stored in db:
      | memberId | website | status    | autoRenewal | memberAccountId | activationDate | expirationDate | balance | membershipType | sourceType     | currencyCode | totalPrice | timestamp           |
      | 1991     | ES      | ACTIVATED | ENABLED     | 91              | 2017-12-12     | 2018-12-12     | 54.99   | BASIC          | POST_BOOKING   | EUR          | 45.56      | 2019-11-11T16:15:14 |
      | 1887     | ES      | ACTIVATED | ENABLED     | 894             | 2018-01-01     | 2019-01-01     | 54.99   | BUSINESS       | FUNNEL_BOOKING | EUR          | 923.35     | 2015-10-11T01:06:09 |
    And that booking-api returns the following bookings when performs the search by membershipId
      | membershipId | bookingId | isSubscriptionBooking |
      | 1991         | 2731      | false                 |
      | 1991         | 1205      | false                 |
      | 1991         | 8291      | true                  |
      | 1887         | 1432      | false                 |
      | 1887         | 2432      | true                  |
    And each booking related to member has the following details
      | bookingId | productCategoryBooking |
      | 2731      | itinerary              |
      | 1205      | itinerary              |
      | 8291      | subscription           |
      | 1432      | itinerary              |
      | 2432      | subscription           |
    And the next member status action stored in db:
      | id   | memberId | actionType | actionDate |
      | 1236 | 1887     | CREATION   | 2018-06-06 |
      | 1235 | 1887     | ACTIVATION | 2018-10-10 |
    When the client calls getMembershipInfo to membership Api with membershipId <membershipId> and skip booking is <skipBooking>
    Then the membershipInfo response is:
      | name   | lastNames  | membershipId   | membershipAccountId   | membershipType   | expirationDate   | activationDate   | sourceType   | currencyCode   | totalPrice   | website   | status   | autoRenewal   | bookingIdSubscription   | lastStatusModificationDate   | timestamp   |
      | <name> | <lastName> | <membershipId> | <membershipAccountId> | <membershipType> | <expirationDate> | <activationDate> | <sourceType> | <currencyCode> | <totalPrice> | <website> | <status> | <autoRenewal> | <bookingIdSubscription> | <lastStatusModificationDate> | <timestamp> |
    Examples:
      | membershipId | membershipAccountId | membershipType | expirationDate | activationDate | sourceType     | currencyCode | totalPrice | name   | lastName | website | status    | autoRenewal | bookingIdSubscription | skipBooking | lastStatusModificationDate | timestamp           |
      | 1991         | 91                  | BASIC          | 2018-12-12     | 2017-12-12     | POST_BOOKING   | EUR          | 45.56      | SQUALL | LEONHART | ES      | ACTIVATED | ENABLED     | 8291                  | false       |                            | 2019-11-11T16:15:14 |
      | 1887         | 894                 | BUSINESS       | 2019-01-01     | 2018-01-01     | FUNNEL_BOOKING | EUR          | 923.35     | JON    | SMITH    | ES      | ACTIVATED | ENABLED     | 2432                  | false       | 2018-10-10                 | 2015-10-11T01:06:09 |
      | 1991         | 91                  | BASIC          | 2018-12-12     | 2017-12-12     | POST_BOOKING   | EUR          | 45.56      | SQUALL | LEONHART | ES      | ACTIVATED | ENABLED     | 0                     | true        |                            | 2019-11-11T16:15:14 |
      | 1887         | 894                 | BUSINESS       | 2019-01-01     | 2018-01-01     | FUNNEL_BOOKING | EUR          | 923.35     | JON    | SMITH    | ES      | ACTIVATED | ENABLED     | 0                     | true        | 2018-10-10                 | 2015-10-11T01:06:09 |