Feature: Expire membership

  Background:
    Given the next memberAccount stored in db:
      | memberAccountId | userId | firstName | lastNames |
      | 9938            | 4321   | JOSE      | GARCIA    |
      | 5903            | 7295   | Rosa      | Martin    |

  Scenario Outline: Expire renewable and non renewable membership
    Given the user-api has the following user info
      | userId | status | email   | brand | token | tokenType        |
      | 4321   | ACTIVE | <email> | ED    | null  | REQUEST_PASSWORD |
    And the next membership stored in db:
      | memberId       | website | status             | autoRenewal   | memberAccountId | activationDate | expirationDate | balance |
      | <membershipId> | ES      | <membershipStatus> | <autoRenewal> | 9938            | 2019-10-06     | 2020-10-06     | 0       |
    And property to send IDs to Kafka has value true in db:
    When requested to update membership
      | membershipId   | operation   |
      | <membershipId> | <operation> |
    Then the result of the membership update operation is true
    And membershipSubscriptionMessage is correctly sent to kafka queue as <expectedSubscriptionStatus> and email will be sent to <email>
    And membershipUpdateMessage is correctly sent to kafka queue with the membershipId <membershipId>
    Examples:
      | membershipId | membershipStatus | autoRenewal | operation         | expectedSubscriptionStatus | email            |
      | 8380         | ACTIVATED        | ENABLED     | EXPIRE_MEMBERSHIP | PENDING                    | test1@odigeo.com |
      | 8381         | ACTIVATED        | DISABLED    | EXPIRE_MEMBERSHIP | UNSUBSCRIBED               | test2@odigeo.com |
      | 8382         | DEACTIVATED      | ENABLED     | EXPIRE_MEMBERSHIP | UNSUBSCRIBED               | test3@odigeo.com |

  Scenario Outline: Expire internal membership
    Given the user-api has the following user info
      | userId | status | email   | brand | token | tokenType        |
      | 8193   | ACTIVE | <email> | ED    | null  | REQUEST_PASSWORD |
    And the next membership stored in db:
      | memberId       | website | status             | autoRenewal   | memberAccountId | activationDate | expirationDate | balance |
      | <membershipId> | GB      | <membershipStatus> | <autoRenewal> | 5903            | 2020-11-26     | null           | 0       |
    And property to send IDs to Kafka has value true in db:
    When requested to update membership
      | membershipId   | operation   |
      | <membershipId> | <operation> |
    Then the result of the membership update operation is true
    Examples:
      | membershipId | membershipStatus | autoRenewal | operation         | email                         |
      | 4194         | ACTIVATED        | ENABLED     | EXPIRE_MEMBERSHIP | rosa.martin@edreamsodigeo.com |