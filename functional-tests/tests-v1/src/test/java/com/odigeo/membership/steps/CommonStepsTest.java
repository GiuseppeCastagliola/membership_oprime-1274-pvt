package com.odigeo.membership.steps;


import bean.test.BeanTest;
import com.odigeo.membership.ServerConfiguration;

public class CommonStepsTest extends BeanTest<CommonSteps> {

    @Override
    protected CommonSteps getBean() {
        return new CommonSteps(new ServerConfiguration());
    }
}