package com.odigeo.membership.steps;

import com.google.inject.Inject;
import com.odigeo.membership.ServerConfiguration;
import com.odigeo.membership.exception.InvalidParametersException;
import com.odigeo.membership.response.MembershipInfo;
import com.odigeo.membership.world.MembershipManagementWorld;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.java.guice.ScenarioScoped;
import org.apache.commons.lang.StringUtils;

import java.time.LocalDate;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@ScenarioScoped
public class PostBookingSteps extends CommonSteps {

    private static final int DEFAULT_MONTHS_TO_RENEWAL = 6;

    private final MembershipManagementWorld world;

    @Inject
    public PostBookingSteps(MembershipManagementWorld world, ServerConfiguration serverConfiguration) {
        super(serverConfiguration);
        this.world = world;
    }

    @When("^the client request createPostBookingMemberAccount to PostBooking API$")
    public void requestSentToPostBookingService() throws InvalidParametersException {
        world.setCreatedMemberId(postBookingService.createPostBookingMembership(world.getMembershipCreationRequest()));
    }

    @When("^the client request createPostBookingPendingMembership to PostBooking API$")
    public void requestSentToPostBookingPendingMembershipService() throws InvalidParametersException {
        world.setCreatedMemberId(postBookingService.createPostBookingPendingMembership(world.getMembershipCreationRequest()));
    }

    @Then("^the post booking membership is created with this information and status (\\w+)$")
    public void accountCreatedWithPostBookingActionStatus(String status) throws InvalidParametersException {
        MembershipInfo membershipInfo = userAreaService.getMembershipInfo(world.getCreatedMemberId(), false);
        assertEquals(membershipInfo.getMembershipStatus(), status);
        String monthsToRenewal = world.getMembershipCreationRequest().getMonthsToRenewal();
        long monthsToAdd = StringUtils.isBlank(monthsToRenewal) ? DEFAULT_MONTHS_TO_RENEWAL
                : Long.parseLong(world.getMembershipCreationRequest().getMonthsToRenewal());
        if ("ACTIVATED".equals(status)) {
            assertEquals(LocalDate.now().plusMonths(monthsToAdd).toString(), membershipInfo.getExpirationDate());
        } else {
            assertTrue(StringUtils.isEmpty(membershipInfo.getExpirationDate()));
        }
    }

    @Then("^a post booking membership is created with the proper membership type$")
    public void accountCreatedWithPostBookingMembershipType() throws InvalidParametersException {
        String membershipTypeRequest = world.getMembershipCreationRequest().getMembershipType();
        String membershipType = StringUtils.isBlank(membershipTypeRequest) ? "BASIC" : membershipTypeRequest;
        MembershipInfo createdMembershipInfo = userAreaService.getMembershipInfo(world.getCreatedMemberId(), false);
        assertEquals(createdMembershipInfo.getMembershipType(), membershipType);
    }

    @Then("^a membership is created with source type (\\w+)$")
    public void accountCreatedWithSourceType(String sourceType) throws InvalidParametersException {
        MembershipInfo membershipInfo = userAreaService.getMembershipInfo(world.getCreatedMemberId(), false);
        assertEquals(membershipInfo.getSourceType(), sourceType);
    }
}
