package com.odigeo.membership.steps;

import com.google.inject.Inject;
import com.odigeo.membership.functionals.database.DatabaseWorld;
import com.odigeo.membership.functionals.membership.MembershipBuilder;
import com.odigeo.membership.message.MembershipMailerMessage;
import com.odigeo.membership.message.enums.MessageType;
import com.odigeo.membership.mocks.kafka.BookingUpdatesV1KafkaProducer;
import com.odigeo.membership.mocks.kafka.MembershipSubscriptionMessageKafkaConsumer;
import com.odigeo.membership.mocks.kafka.MembershipUpdateMessageKafkaConsumer;
import com.odigeo.membership.mocks.kafka.MessageControllerWorld;
import com.odigeo.membership.mocks.kafka.processors.WelcomeToPrimeMessageProcessor;
import com.odigeo.membership.v1.messages.MembershipUpdateMessage;
import com.odigeo.membership.v4.messages.MembershipSubscriptionMessage;
import com.odigeo.membership.v4.messages.SubscriptionStatus;
import com.odigeo.messaging.utils.MessageDataAccessException;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.runtime.java.guice.ScenarioScoped;
import org.apache.commons.collections.CollectionUtils;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

@ScenarioScoped
public class KafkaSteps {

    private static final int MESSAGE_WAIT_TIME = 200;
    private static final int MESSAGE_WAIT_MAX_ATTEMPTS = 20;

    private final DatabaseWorld databaseWorld;
    private final MembershipUpdateMessageKafkaConsumer membershipUpdateMessageKafkaConsumer;
    private final BookingUpdatesV1KafkaProducer kafkaProducer;
    private final MembershipSubscriptionMessageKafkaConsumer subscriptionMessageKafkaConsumer;
    private final MessageControllerWorld messageControllerWorld;

    @Inject
    public KafkaSteps(DatabaseWorld databaseWorld, MessageControllerWorld messageControllerWorld, MembershipSubscriptionMessageKafkaConsumer subscriptionMessageKafkaConsumer,
                      MembershipUpdateMessageKafkaConsumer membershipUpdateMessageKafkaConsumer, BookingUpdatesV1KafkaProducer kafkaProducer) {
        this.databaseWorld = databaseWorld;
        this.messageControllerWorld = messageControllerWorld;
        this.subscriptionMessageKafkaConsumer = subscriptionMessageKafkaConsumer;
        this.membershipUpdateMessageKafkaConsumer = membershipUpdateMessageKafkaConsumer;
        this.kafkaProducer = kafkaProducer;
    }

    @And("^the booking (\\d+) is updated$")
    public void sendUpdateBookingMessageToKafka(long bookingId) throws MessageDataAccessException, InterruptedException {
        kafkaProducer.sendMessage(bookingId);
    }

    @And("^membershipSubscriptionMessage is correctly sent to kafka queue and email will be sent to (.*)$")
    public void readMembershipSubscriptionMessageFromKafka(String email) {
        assertEmailSentToKafka(email, SubscriptionStatus.SUBSCRIBED);
    }

    @And("^membershipUpdateMessage is correctly sent to kafka queue with the membershipId (\\d+)$")
    public void checkMembershipUpdatedMessageThen(final Long membershipId) throws InterruptedException {
        checkMembershipUpdatedMessage(membershipId, true);
    }

    @Then("^the message with member info is sent to kafka for (.*) and (.*) with status (.*)$")
    public void checkSubscriptionMessageData(Long membershipId, String email, String status)
        throws InterruptedException, SQLException, ClassNotFoundException {
        MembershipBuilder membership = databaseWorld.getMembershipById(membershipId);
        MembershipSubscriptionMessage membershipSubscriptionMessage = checkAndGetMembershipSubscriptionMessage();
        validateMessageValues(email, membership, status, membershipSubscriptionMessage);
    }

    @And("^the message with shouldSetPassword (true|false) and passwordToken (null|\\w+) is sent to kafka$")
    public void messageIsSentToKafkaWithPasswordTokenFields(boolean shouldSetPassword, String passwordToken) {
        assertShouldSetPasswordAndPasswordToken(shouldSetPassword, passwordToken);
    }

    @And("^the message with member info is sent to kafka with shouldSetPassword (true|false) and passwordToken (null|\\w+) if apply (true|false)$")
    public void messageIsSentToKafkaWithPasswordTokenFieldsIfApplies(boolean shouldSetPassword, String passwordToken, boolean shouldSendMembershipIdsToKafka) {
        if (shouldSendMembershipIdsToKafka) {
            assertShouldSetPasswordAndPasswordToken(shouldSetPassword, passwordToken);
        }
    }

    @And("^if apply to (true|false) membershipSubscriptionMessage is correctly sent to kafka queue and email will be sent to (.*)$")
    public void ifApplyToShouldSendMembershipIdsToKafkaMembershipSubscriptionMessageIsCorrectlySentToKafkaQueueAndEmailWillBeSentToEmail(boolean shouldSendMembershipIdsToKafka, String email) {
        if (shouldSendMembershipIdsToKafka) {
            assertEmailSentToKafka(email, SubscriptionStatus.SUBSCRIBED);
        }
    }

    @And("^membershipSubscriptionMessage is correctly sent to kafka queue as (\\w+) and email will be sent to (.*)$")
    public void membershipSubscriptionMessageIsSentToKafkaWithSubscriptionStatusAndEmail(String subscriptionStatus, String email) {
        assertEmailSentToKafka(email, SubscriptionStatus.valueOf(subscriptionStatus));
    }

    @And("^the number of membership subscription messages sent is (\\d+)$")
    public void noMembershipSubscriptionMessageHasBeenSent(int numOfMessagesSent) {
        assertEquals(subscriptionMessageKafkaConsumer.getMessagesReceived().size(), numOfMessagesSent);
    }

    @And("^no message to trigger a WelcomeToPrime transactional email is sent$")
    public void noTransactionalEmailTriggerMessageSentToKafka() throws InterruptedException {
        transactionalEmailTriggerMessagesSentToKafka(0);
    }

    @And("^(\\d)+ message(?:s?) to trigger a WelcomeToPrime transactional email is sent$")
    public void transactionalEmailTriggerMessagesSentToKafka(int expectedListSize) throws InterruptedException {
        Thread.sleep(500);
        List<MembershipMailerMessage> messageList = getWelcomeToPrimeMessageProcessor().getReceivedMessages();
        assertEquals(messageList.size(), expectedListSize);
    }


    @And("^a message to trigger a WelcomeToPrime transactional email is sent to kafka with these properties:$")
    public void transactionalEmailTriggerMessageReceived(DataTable datatable) throws InterruptedException {
        Map<String, Long> messageProperties = new HashMap<>(datatable.asMap(String.class, Long.class));
        Thread.sleep(500);

        long membershipId = messageProperties.get("membershipId");
        long userId = messageProperties.get("userId");
        long bookingId = messageProperties.get("bookingId");

        List<MembershipMailerMessage> filteredMessages = getWelcomeToPrimeMessageProcessor().getReceivedMessages().stream()
                .filter(message -> message.getMembershipId() == membershipId)
                .collect(Collectors.toList());
        MembershipMailerMessage expectedMessage = filteredMessages.get(0);

        assertEquals(filteredMessages.size(), 1);
        assertEquals(expectedMessage.getMembershipId().longValue(), membershipId);
        assertEquals(expectedMessage.getBookingId().longValue(), bookingId);
        assertEquals(expectedMessage.getUserId().longValue(), userId);
        assertEquals(expectedMessage.getMessageType(), MessageType.WELCOME_TO_PRIME);
    }

    private WelcomeToPrimeMessageProcessor getWelcomeToPrimeMessageProcessor() {
        return messageControllerWorld.getWelcomeToPrimeMessageConsumer().getMessageProcessor();
    }

    private void assertEmailSentToKafka(String email, SubscriptionStatus status) {
        Optional<MembershipSubscriptionMessage> membershipSubscriptionMessage = subscriptionMessageKafkaConsumer.getMessagesReceived().stream()
                .filter(subscriptionMessage -> subscriptionMessage.getEmail().equals(email) && subscriptionMessage.getSubscriptionStatus().equals(status))
                .findFirst();
        assertTrue(membershipSubscriptionMessage.isPresent());
        assertNotNull(membershipSubscriptionMessage.get());
        assertEquals(membershipSubscriptionMessage.get().getSubscriptionStatus(), status);
        assertEquals(membershipSubscriptionMessage.get().getEmail(), email);
    }

    private void validateMessageValues(String email, MembershipBuilder membership,
        String status, MembershipSubscriptionMessage membershipSubscriptionMessage) {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");
        SubscriptionStatus subscriptionStatus = SubscriptionStatus.valueOf(status);
        assertEquals(membershipSubscriptionMessage.getSubscriptionStatus(), subscriptionStatus);
        assertEquals(membershipSubscriptionMessage.getEmail(), email);
        assertEquals(membershipSubscriptionMessage.getActivationDate(),
            LocalDate.parse(membership.getActivationDate(), format));
        assertEquals(membershipSubscriptionMessage.getExpirationDate(),
            LocalDate.parse(membership.getExpirationDate(), format));
        assertEquals(membershipSubscriptionMessage.getWebsite(), membership.getWebsite());
    }

    private MembershipSubscriptionMessage checkAndGetMembershipSubscriptionMessage() {
        List<MembershipSubscriptionMessage> subscriptionMessages = subscriptionMessageKafkaConsumer.getMessagesReceived();
        assertTrue(CollectionUtils.isNotEmpty(subscriptionMessages));
        MembershipSubscriptionMessage membershipSubscriptionMessage = subscriptionMessages.get(0);
        assertNotNull(membershipSubscriptionMessage);
        return membershipSubscriptionMessage;
    }

    private MembershipUpdateMessage checkAndGetMembershipUpdateMessage() throws InterruptedException {
        List<MembershipUpdateMessage> messagesReceived = waitAndGetMembershipUpdateMessage(MESSAGE_WAIT_MAX_ATTEMPTS, membershipUpdateMessageKafkaConsumer.getMessagesReceived());
        assertTrue(CollectionUtils.isNotEmpty(messagesReceived));
        MembershipUpdateMessage membershipUpdateMessage = messagesReceived.get(0);
        assertNotNull(membershipUpdateMessage);
        return membershipUpdateMessage;
    }

    private List<MembershipUpdateMessage> waitAndGetMembershipUpdateMessage(int attempts, List<MembershipUpdateMessage> messagesReceived) throws InterruptedException {
        if (CollectionUtils.isNotEmpty(messagesReceived) || attempts <= 0) {
            return messagesReceived;
        }
        Thread.sleep(MESSAGE_WAIT_TIME);
        return waitAndGetMembershipUpdateMessage(attempts - 1, membershipUpdateMessageKafkaConsumer.getMessagesReceived());
    }

    private void checkEmptyMembershipUpdateMessage() {
        List<MembershipUpdateMessage> messagesReceived = membershipUpdateMessageKafkaConsumer.getMessagesReceived();
        assertTrue(CollectionUtils.isEmpty(messagesReceived));
    }

    private void checkMembershipUpdatedMessage(final Long membershipId, final boolean shouldSendUpdates) throws InterruptedException {
        if (shouldSendUpdates) {
            MembershipUpdateMessage membershipUpdateMessage = checkAndGetMembershipUpdateMessage();
            assertEquals(Long.valueOf(membershipUpdateMessage.getMembershipId()), membershipId);
        } else {
            checkEmptyMembershipUpdateMessage();
        }
    }

    private void assertShouldSetPasswordAndPasswordToken(Boolean shouldSetPassword, String passwordToken) {
        MembershipSubscriptionMessage membershipSubscriptionMessage = checkAndGetMembershipSubscriptionMessage();
        assertEquals(membershipSubscriptionMessage.getShouldSetPassword(), shouldSetPassword);
        if ("null".equalsIgnoreCase(passwordToken)) {
            assertNull(membershipSubscriptionMessage.getForgetPasswordToken());
        } else {
            assertEquals(membershipSubscriptionMessage.getForgetPasswordToken(), passwordToken);
        }
    }
}
