package com.odigeo.membership.steps;

import com.google.inject.Inject;
import com.odigeo.membership.ServerConfiguration;
import com.odigeo.membership.exception.InvalidParametersException;
import cucumber.api.java.en.When;
import cucumber.runtime.java.guice.ScenarioScoped;

@ScenarioScoped
public class BackOfficeSteps extends CommonSteps {

    @Inject
    public BackOfficeSteps(ServerConfiguration serverConfiguration) {
        super(serverConfiguration);
    }

    @When("^requested update member info in marketing for (.*) and (.*)$")
    public void callServiceWithMemberInfo(Long membershipId, String email)
        throws InvalidParametersException {
        membershipBackOfficeService.updateMembershipMarketingInfo(membershipId, email);
    }
}
