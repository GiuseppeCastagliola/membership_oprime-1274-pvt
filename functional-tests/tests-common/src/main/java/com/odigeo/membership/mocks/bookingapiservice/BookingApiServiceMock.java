package com.odigeo.membership.mocks.bookingapiservice;

import com.odigeo.bookingapi.client.v12.BookingApiServiceMockImpl;
import com.odigeo.bookingapi.v12.responses.BookingDetail;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

public class BookingApiServiceMock extends BookingApiServiceMockImpl {

    private static final long ERRONEOUS_BOOKING_ID = 1L;

    private List<BookingDetail> bookingDetails;

    public BookingApiServiceMock() {
        bookingDetails = new ArrayList<>();
    }

    @Override
    public BookingDetail getBooking(String user, String password, Locale locale, long bookingId) {
        if (bookingId == ERRONEOUS_BOOKING_ID) {
            BookingDetail bookingWithError = new BookingDetail();
            bookingWithError.setErrorMessage("Mock was configured to return this booking with error");
            return bookingWithError;
        }
        Optional<BookingDetail> bookingDetailOpt = bookingDetails.stream().filter(bookingDetail -> bookingDetail.getBookingBasicInfo().getId() == bookingId).findFirst();
        return bookingDetailOpt.orElse(new BookingDetail());
    }

    public void addBookingDetail(BookingDetail bookingDetail) {
        if (bookingDetails == null) {
            bookingDetails = new ArrayList<>();
        }
        bookingDetails.add(bookingDetail);
    }

    public void resetMocks() {
        bookingDetails.clear();
    }
}
