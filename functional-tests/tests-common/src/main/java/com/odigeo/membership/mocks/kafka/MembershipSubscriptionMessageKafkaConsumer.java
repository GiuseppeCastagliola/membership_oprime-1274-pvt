package com.odigeo.membership.mocks.kafka;

import com.google.inject.Singleton;
import com.odigeo.membership.functionals.config.KafkaConfig;
import com.odigeo.membership.v4.messages.MembershipSubscriptionMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Singleton
public class MembershipSubscriptionMessageKafkaConsumer extends AbstractKafkaConsumer<MembershipSubscriptionMessage> {

    private static final Logger LOGGER = LoggerFactory.getLogger(MembershipSubscriptionMessageKafkaConsumer.class);

    public MembershipSubscriptionMessageKafkaConsumer() {
        super(MembershipSubscriptionMessage.class);
    }

    @Override
    protected String getMessagesGroupId(final KafkaConfig kafkaConfig) {
        return kafkaConfig.getSubscriptionMessagesGroupId();
    }

    @Override
    protected String getMessagesTopicName(final KafkaConfig kafkaConfig) {
        return kafkaConfig.getSubscriptionMessagesTopicName();
    }

    @Override
    protected Logger logger() {
        return LOGGER;
    }
}
