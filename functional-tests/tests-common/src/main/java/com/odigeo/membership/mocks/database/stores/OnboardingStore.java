package com.odigeo.membership.mocks.database.stores;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class OnboardingStore {

    private static final String GET_ONBOARDINGS_SQL = "SELECT ID, MEMBER_ACCOUNT_ID, EVENT, DEVICE, TIMESTAMP FROM MEMBERSHIP_OWN.GE_ONBOARDING";

    public List<Onboarding> getOnboardings(Connection conn) throws SQLException {
        List<Onboarding> results = new ArrayList<>();
        try (PreparedStatement preparedStatement = conn.prepareStatement(GET_ONBOARDINGS_SQL)) {
            try (ResultSet rs = preparedStatement.executeQuery()) {
                while (rs.next()) {
                    results.add(buildOnboarding(rs));
                }
            }
        }
        return results;
    }

    private Onboarding buildOnboarding(ResultSet rs) throws SQLException {
        return new Onboarding(rs.getLong("ID"), rs.getLong("MEMBER_ACCOUNT_ID"), rs.getString("EVENT"), rs.getString("DEVICE"), rs.getTimestamp("TIMESTAMP").toLocalDateTime());
    }

    public static class Onboarding {

        private final long id;
        private final long memberAccountId;
        private final String event;
        private final String device;
        private final LocalDateTime timestamp;

        public Onboarding(long id, long memberAccountId, String event, String device, LocalDateTime timestamp) {
            this.id = id;
            this.memberAccountId = memberAccountId;
            this.event = event;
            this.device = device;
            this.timestamp = timestamp;
        }

        public long getId() {
            return id;
        }

        public long getMemberAccountId() {
            return memberAccountId;
        }

        public String getEvent() {
            return event;
        }

        public String getDevice() {
            return device;
        }

        public LocalDateTime getTimestamp() {
            return timestamp;
        }
    }
}
