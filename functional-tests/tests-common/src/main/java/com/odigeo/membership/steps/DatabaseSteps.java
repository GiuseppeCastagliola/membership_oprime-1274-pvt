package com.odigeo.membership.steps;

import com.google.inject.Inject;
import com.odigeo.converter.BooleanConverter;
import com.odigeo.membership.functionals.database.DatabaseWorld;
import com.odigeo.membership.functionals.membership.BlacklistPaymentMethodBuilder;
import com.odigeo.membership.functionals.membership.MemberAccountBuilder;
import com.odigeo.membership.functionals.membership.MemberStatusActionBuilder;
import com.odigeo.membership.functionals.membership.MembershipBuilder;
import cucumber.api.Transform;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.testng.Assert;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static java.math.BigDecimal.ROUND_CEILING;
import static java.time.temporal.ChronoUnit.YEARS;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;


public class DatabaseSteps {
    private static final String MEMBERSHIP_TYPE_BASIC = "BASIC";
    private static final String PROPERTY_KEY_SEND_IDS_KAFKA = "SEND_IDS_TO_KAFKA";
    private final DatabaseWorld databaseWorld;

    @Inject
    public DatabaseSteps(DatabaseWorld databaseWorld) {
        this.databaseWorld = databaseWorld;
    }

    @Given("^the next memberAccount stored in db:$")
    public void theNextMembershipStoredInDb(List<MemberAccountBuilder> memberAccounts) throws InterruptedException, SQLException, ClassNotFoundException {
        memberAccounts.forEach(memberAccount -> {
            if (memberAccount.getTimestamp() == null) {
                memberAccount.setTimestamp(LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME));
            }
        });
        databaseWorld.addMemberAccounts(memberAccounts);
    }

    @Given("^membership rule of max (\\d+) bookings on (\\d+) days and rule activation value (\\d+)$")
    public void withMembershipRulesOfMaxBookingsOnDays(int numBookings, int numDays, int ruleActivated) throws Throwable {
        databaseWorld.addMembershipsRules(numBookings, numDays, ruleActivated);
    }

    @And("^member (\\d+) has (\\d+) bookings in the last (\\d+) days$")
    public void memberHasBookingsInTheLastDays(long memberId, int numBookings, int numDays) throws Throwable {
        databaseWorld.addMemberBookings(memberId, numBookings, numDays);
    }

    @And("^the next membership stored in db:$")
    public void createMembershipInDB(List<MembershipBuilder> memberships) throws InterruptedException, SQLException, ClassNotFoundException {
        databaseWorld.addMemberships(addMembershipsMissingData(memberships));
    }

    @And("^the next membership product stored in db:$")
    public void createMembershipProductInDB(List<MembershipBuilder> memberships) throws InterruptedException, SQLException, ClassNotFoundException {
        databaseWorld.addMemberships(addMembershipsMissingData(memberships));
    }

    private List<MembershipBuilder> addMembershipsMissingData(List<MembershipBuilder> memberships) {
        memberships.forEach(membership -> {
            if (membership.getMembershipType() == null) {
                membership.setMembershipType(MEMBERSHIP_TYPE_BASIC);
            }
            if (membership.getTimestamp() == null) {
                membership.setTimestamp(LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME));
            }
        });
        return memberships;
    }

    @And("^the next membership activated today is stored in db:$")
    public void createMembershipActivatedTodayInDB(final List<MembershipBuilder> memberships) throws InterruptedException, SQLException, ClassNotFoundException {
        final LocalDate now = LocalDate.now();
        final String today = DateTimeFormatter.ISO_LOCAL_DATE.format(now);
        final String inAYear = DateTimeFormatter.ISO_LOCAL_DATE.format(now.plus(1, YEARS));
        memberships.forEach(membership -> {
            membership.setActivationDate(today);
            membership.setExpirationDate(inAYear);
            if (membership.getMembershipType() == null) {
                membership.setMembershipType(MEMBERSHIP_TYPE_BASIC);
            }
            if (membership.getTimestamp() == null) {
                membership.setTimestamp(LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME));
            }
        });
        databaseWorld.addMemberships(memberships);
    }

    @And("^the stored membership with ID (\\d+) now has (-?\\d.*) as balance$")
    public void storedMembershipBalance(final Long membershipId, final BigDecimal expectedBalance) throws InterruptedException, SQLException, ClassNotFoundException {
        Optional<BigDecimal> membershipBalance = databaseWorld.findMembershipBalance(membershipId);
        if (membershipBalance.isPresent()) {
            assertEquals(membershipBalance.get().setScale(2, ROUND_CEILING), expectedBalance.setScale(2, ROUND_CEILING));
        } else {
            Assert.fail("Membership balance not found with the ID ".concat(String.valueOf(membershipId)));
        }
    }

    @Then("^the MemberAccount stored in db has the following information:$")
    public void theMemberIsUpdatedCorrectlyWithThisInformation(List<MemberAccountBuilder> memberAccountList) {
        Objects.requireNonNull(memberAccountList)
                .forEach(memberAccount -> {
                    MemberAccountBuilder dBMemberAccount = databaseWorld.getMemberAccountByMemberAccountId(memberAccount.getMemberAccountId());
                    assertEquals(memberAccount, dBMemberAccount);
                });
    }

    @And("^the next member status action stored in db:$")
    public void theNextMemberStatusActionStoredInDb(final List<MemberStatusActionBuilder> statusActions) throws InterruptedException, SQLException, ClassNotFoundException {
        databaseWorld.addMemberStatusActions(statusActions);
    }

    @And("^property to send IDs to Kafka has value (true|false) in db:$")
    public void sendIDsToKafkaHasValueSendMembershipIdsToKafkaInDb(@Transform(BooleanConverter.class) Boolean sendIds) throws InterruptedException, SQLException, ClassNotFoundException {
        databaseWorld.addPropertiesConfigurationValue(PROPERTY_KEY_SEND_IDS_KAFKA, sendIds);
    }

    @Given("^the following blacklisted payment methods are stored in the db:$")
    public void blacklistedPaymentMethodsAreStored(List<BlacklistPaymentMethodBuilder> paymentMethods) throws InterruptedException, SQLException, ClassNotFoundException {
        databaseWorld.addBlacklistedPaymentMethods(paymentMethods);
    }

    @Then("^the (.*) auto-renewal operation tracking was stored in db for membership with ID (.*)$")
    public void checkAutorenewalTracking(final String autoRenewalOperation, final String membershipId) throws InterruptedException, SQLException, ClassNotFoundException {
        assertTrue(databaseWorld.existAutorenewalTracking(membershipId, autoRenewalOperation));
    }
}
