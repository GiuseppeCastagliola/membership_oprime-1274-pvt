package com.odigeo.membership.functionals;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Inject;
import com.odigeo.membership.functionals.config.KafkaConfig;
import com.odigeo.membership.functionals.database.DatabaseInstallerWorld;
import com.odigeo.membership.functionals.jboss.JbossServerWorld;
import com.odigeo.membership.mocks.bookingapiservice.BookingApiServiceWorld;
import com.odigeo.membership.mocks.kafka.MembershipSubscriptionMessageKafkaConsumer;
import com.odigeo.membership.mocks.kafka.MembershipUpdateMessageKafkaConsumer;
import com.odigeo.membership.mocks.kafka.MessageControllerWorld;
import com.odigeo.membership.mocks.userapi.UserApiWorld;
import com.odigeo.membership.mocks.visitengine.VisitEngineServiceWorld;
import com.odigeo.membership.server.ServerStopException;
import com.odigeo.messaging.utils.BasicMessage;
import com.odigeo.messaging.utils.Message;
import com.odigeo.messaging.utils.MessageDataAccessException;
import com.odigeo.messaging.utils.Publisher;
import com.odigeo.messaging.utils.kafka.KafkaJsonPublisher;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.runtime.java.guice.ScenarioScoped;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Collections;
import java.util.concurrent.CompletableFuture;

@ScenarioScoped
public class InstallerScenarioEnvironment {

    private static final int KAFKA_CONFIG_WAIT_TIME = 1000;
    private static final Logger LOGGER = Logger.getLogger(InstallerScenarioEnvironment.class);

    @Inject
    private JbossServerWorld jbossServerWorld;
    @Inject
    private DatabaseInstallerWorld databaseInstallerWorld;
    @Inject
    private BookingApiServiceWorld bookingApiServiceWorld;
    @Inject
    private MessageControllerWorld messageControllerWorld;
    @Inject
    private MembershipSubscriptionMessageKafkaConsumer membershipSubscriptionMessageKafkaConsumer;
    @Inject
    private MembershipUpdateMessageKafkaConsumer membershipUpdateMessageKafkaConsumer;
    @Inject
    private VisitEngineServiceWorld visitEngineServiceWorld;
    @Inject
    private UserApiWorld userApiWorld;

    @Before
    public void install() throws InterruptedException, SQLException, ClassNotFoundException, IOException, ServerStopException, MessageDataAccessException {
        LOGGER.info("Start install environment for a scenario");
        CompletableFuture.runAsync(membershipSubscriptionMessageKafkaConsumer);
        CompletableFuture.runAsync(membershipUpdateMessageKafkaConsumer);
        databaseInstallerWorld.clearData();
        jbossServerWorld.installScenario();
        bookingApiServiceWorld.install();
        visitEngineServiceWorld.install();
        userApiWorld.install();
        bootstrapKafkaConsumer();
        LOGGER.info("End install environment for a scenario");
    }

    @After
    public void uninstall() throws InterruptedException {
        LOGGER.info("Start uninstall environment for a scenario");
        bookingApiServiceWorld.uninstall();
        visitEngineServiceWorld.uninstall();
        userApiWorld.uninstall();
        messageControllerWorld.uninstall();
        closeKafkaConsumers();
        LOGGER.info("End uninstall environment for a scenario");
    }

    private void bootstrapKafkaConsumer() throws MessageDataAccessException, InterruptedException {
        KafkaConfig kafkaConfig = ConfigurationEngine.getInstance(KafkaConfig.class);
        Publisher<Message> publisher = new KafkaJsonPublisher.Builder<>(kafkaConfig.getCompleteHost()).build();
        BasicMessage message = new BasicMessage();
        message.setTopicName(kafkaConfig.getSubscriptionMessagesTopicName());
        publisher.publishMessages(Collections.singletonList(message));

        BasicMessage messageBookingUpdateTopic = new BasicMessage();
        messageBookingUpdateTopic.setTopicName("BOOKING_UPDATES_v1");
        messageBookingUpdateTopic.setKey("123");
        publisher.publishMessages(Collections.singletonList(messageBookingUpdateTopic));

        Thread.sleep(KAFKA_CONFIG_WAIT_TIME);
    }

    private void closeKafkaConsumers() {
        membershipSubscriptionMessageKafkaConsumer.clearMessages();
        membershipSubscriptionMessageKafkaConsumer.close();
        membershipUpdateMessageKafkaConsumer.clearMessages();
        membershipUpdateMessageKafkaConsumer.close();
    }
}
