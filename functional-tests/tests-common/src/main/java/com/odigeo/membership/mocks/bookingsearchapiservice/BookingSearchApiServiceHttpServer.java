package com.odigeo.membership.mocks.bookingsearchapiservice;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.membership.server.JaxRsServiceHttpServer;

@Singleton
public class BookingSearchApiServiceHttpServer extends JaxRsServiceHttpServer {

    private static final String BOOKING_SEARCH_API_PATH = "/engine";
    private static final int BOOKING_SEARCH_API_PORT = 45685;

    @Inject
    public BookingSearchApiServiceHttpServer() {
        super(BOOKING_SEARCH_API_PATH, BOOKING_SEARCH_API_PORT);
    }
}
