package com.odigeo.membership.mocks.database.stores;

import com.odigeo.commons.uuid.UUIDSerializer;
import com.odigeo.membership.functionals.membership.AutorenewalTrackingBuilder;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AutorenewalTrackingStore {
    private static final String FIND_AUTORENEWAL_TRACKING = "SELECT t.ID, t.MEMBERSHIP_ID, t.TIMESTAMP, t.OPERATION, t.REQUESTED_METHOD, t.REQUESTER from GE_AUTORENEWAL_TRACKING t "
            + "WHERE t.MEMBERSHIP_ID = ? and t.OPERATION = ? ORDER BY t.TIMESTAMP";
    private static final String ID = "ID";
    private static final String MEMBERSHIP_ID = "MEMBERSHIP_ID";
    private static final String TIMESTAMP = "TIMESTAMP";
    private static final String OPERATION = "OPERATION";
    private static final String REQUESTED_METHOD = "REQUESTED_METHOD";
    private static final String REQUESTER = "REQUESTER";

    public List<AutorenewalTrackingBuilder> findAutorenewalTracking(final Connection conn, final String membershipId, String autoRenewalOperation) throws SQLException {
        List<AutorenewalTrackingBuilder> trackingBuilders = new ArrayList<>();
        try (PreparedStatement preparedStatement = conn.prepareStatement(FIND_AUTORENEWAL_TRACKING)) {
            preparedStatement.setString(1, membershipId);
            preparedStatement.setString(2, autoRenewalOperation);
            try (ResultSet rs = preparedStatement.executeQuery()) {
                while (rs.next()) {
                    trackingBuilders.add(buildAutorenewalTrackingBuilder(rs));
                }
            }
        }
        return trackingBuilders;
    }

    private AutorenewalTrackingBuilder buildAutorenewalTrackingBuilder(final ResultSet rs) throws SQLException {
        return new AutorenewalTrackingBuilder()
                .setId(UUIDSerializer.fromBytes(rs.getBytes(ID)))
                .setMembershipId(rs.getString(MEMBERSHIP_ID))
                .setTimestamp(rs.getTimestamp(TIMESTAMP).toInstant())
                .setAutoRenewalOperation(rs.getString(OPERATION))
                .setRequestedMethod(rs.getString(REQUESTED_METHOD))
                .setRequester(rs.getString(REQUESTER));
    }
}
