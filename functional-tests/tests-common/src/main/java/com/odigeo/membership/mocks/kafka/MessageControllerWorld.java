package com.odigeo.membership.mocks.kafka;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.mocks.kafka.consumers.WelcomeToPrimeMessageConsumer;
import cucumber.runtime.java.guice.ScenarioScoped;
import org.apache.log4j.Logger;

@ScenarioScoped
public class MessageControllerWorld {
    private static final Logger LOGGER = Logger.getLogger(MessageControllerWorld.class);

    private final WelcomeToPrimeMessageConsumer welcomeToPrimeMessageConsumer;

    public MessageControllerWorld() {
        welcomeToPrimeMessageConsumer = ConfigurationEngine.getInstance(WelcomeToPrimeMessageConsumer.class);
    }

    public WelcomeToPrimeMessageConsumer getWelcomeToPrimeMessageConsumer() {
        return welcomeToPrimeMessageConsumer;
    }

    public void uninstall() {
        LOGGER.info("resetMessageList() for mock kafka consumer");
        welcomeToPrimeMessageConsumer.getMessageProcessor().resetMessageList();
    }
}
