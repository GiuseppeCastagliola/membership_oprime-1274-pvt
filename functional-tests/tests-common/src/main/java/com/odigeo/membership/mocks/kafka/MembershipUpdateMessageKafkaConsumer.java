package com.odigeo.membership.mocks.kafka;

import com.google.inject.Singleton;
import com.odigeo.membership.functionals.config.KafkaConfig;
import com.odigeo.membership.v1.messages.MembershipUpdateMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Singleton
public class MembershipUpdateMessageKafkaConsumer extends AbstractKafkaConsumer<MembershipUpdateMessage> {

    private static final Logger LOGGER = LoggerFactory.getLogger(MembershipUpdateMessageKafkaConsumer.class);

    public MembershipUpdateMessageKafkaConsumer() {
        super(MembershipUpdateMessage.class);
    }

    @Override
    protected String getMessagesGroupId(final KafkaConfig kafkaConfig) {
        return kafkaConfig.getMembershipUpdateMessagesGroupId();
    }

    @Override
    protected String getMessagesTopicName(final KafkaConfig kafkaConfig) {
        return kafkaConfig.getMembershipUpdateMessagesTopicName();
    }

    @Override
    protected Logger logger() {
        return LOGGER;
    }
}
