package com.odigeo.membership;

import com.google.inject.Singleton;
import org.apache.commons.lang.StringUtils;

@Singleton
public class ServerConfiguration {
    private static final String DEFAULT_HTTP_PORT = "8080";
    private static final String HTTP_PORT_PROPERTY = "httpPort";
    private final String serviceServer;

    public ServerConfiguration() {
        serviceServer = "localhost:" + StringUtils.defaultIfEmpty(System.getProperty(HTTP_PORT_PROPERTY), DEFAULT_HTTP_PORT);
    }

    public String getMembershipServer() {
        return serviceServer;
    }
}
