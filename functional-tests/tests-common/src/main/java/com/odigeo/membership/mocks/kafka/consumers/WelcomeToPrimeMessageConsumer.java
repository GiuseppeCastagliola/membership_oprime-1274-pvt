package com.odigeo.membership.mocks.kafka.consumers;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.commons.messaging.Consumer;
import com.odigeo.commons.messaging.JsonDeserializer;
import com.odigeo.commons.messaging.KafkaConsumer;
import com.odigeo.membership.message.MembershipMailerMessage;
import com.odigeo.membership.mocks.kafka.processors.WelcomeToPrimeMessageProcessor;

@Singleton
public class WelcomeToPrimeMessageConsumer {
    private static final String TOPIC_NAME = "MEMBERSHIP_TRANSACTIONAL_ACTIVATIONS_v1";
    private static final int NUMBER_OF_THREADS = 1;
    private final WelcomeToPrimeMessageProcessor messageProcessor;

    @Inject
    public WelcomeToPrimeMessageConsumer(WelcomeToPrimeMessageProcessor welcomeToPrimeMessageProcessor) throws InterruptedException {
        this.messageProcessor = welcomeToPrimeMessageProcessor;
        Consumer<MembershipMailerMessage> consumer = new KafkaConsumer(new JsonDeserializer<>(MembershipMailerMessage.class), TOPIC_NAME, "welcome-to-prime-group");
        consumer.start(messageProcessor, NUMBER_OF_THREADS);
    }

    public WelcomeToPrimeMessageProcessor getMessageProcessor() {
        return messageProcessor;
    }
}
