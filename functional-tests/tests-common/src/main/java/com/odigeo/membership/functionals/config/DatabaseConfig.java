package com.odigeo.membership.functionals.config;

import com.edreams.configuration.ConfiguredInPropertiesFile;
import com.google.inject.Singleton;

@Singleton
@ConfiguredInPropertiesFile
public class DatabaseConfig {

    private String url;
    private String user;
    private String password;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
