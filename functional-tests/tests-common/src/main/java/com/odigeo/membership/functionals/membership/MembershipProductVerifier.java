package com.odigeo.membership.functionals.membership;

import com.odigeo.product.v2.model.Product;

import java.math.BigDecimal;

import static org.testng.Assert.assertEquals;

public class MembershipProductVerifier {
    private final String productId;
    private final BigDecimal amount;

    public MembershipProductVerifier(String productId, BigDecimal amount) {
        this.productId = productId;
        this.amount = amount;
    }

    public void verifyMembershipProduct(Product product) {
        assertEquals(this.productId, product.getId());
        assertEquals(this.amount, product.getFees().stream().findFirst().get().getPrice().getAmount());
    }
}
